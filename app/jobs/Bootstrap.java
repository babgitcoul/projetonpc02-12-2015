package jobs;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import models.*;
import models.enums.UserRoleType;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import controllers.Agents;
import controllers.AppController;


@OnApplicationStart
public class Bootstrap extends Job {
    @Override
    public void doJob() {
     
     Agent  agt = new Agent();
      Agent  agt1 = new Agent();
      agt = Agent.find("byContact ","002257878787").first();
      agt1 = Agent.find("byContact ","0022547767646").first();
     
     if(agt == null)
      {
          Agent  agent = new Agent();
        agent.setAdresse("BP 3150 Abj 13");

        agent.setContact("002257878787");
        
        agent.setEmail("onpc@gmail.com");
        agent.setFonctionaire("Non");
 
        agent.setLibelle_classe("Classe 1");
        agent.setLibelle_contrat("CDI");
        agent.setLibelle_csu_courant("DALOA");
        agent.setLibelle_echelon("Echelon 1");
        agent.setLibelle_grade("Colonel");
        agent.setLibelle_poste("Administrateur");
        agent.setLibelle_sous_dir_courant("DES FINANCES");
        agent.setLibelle_type_agent("Permanant");
        agent.setLieu_naissance("Abidjan");
        agent.setLieuHbtion("Abidjan");
        agent.setMatricule("M12457878");
        agent.setMecano("M12457878");
      
        agent.setNom("BABOUET");
        agent.setNombreEnf((long) 2);
        agent.setNomMere("BABOUET");
        agent.setNomPere("YOBOUET");
        agent.setNum_note_svce("MRSDE124545");
        agent.setNumCni("CI1102121212");
   
        agent.setPrenom("ROBERT");
        agent.setSexe("Masculin");
        agent.setSit_matr("Celibataire");
        
        Date today = Calendar.getInstance().getTime();
       
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
         // (3) conversion de la date formatee en String
   
         
        agent.setUpdated(today);
        agent.setCreated(today);

        
        Agent monAgent = Agent.find("contact is ?", "002257878787").first();

        if (monAgent == null ) {
         
         if(agent.validateAndSave())
         {
          Agent monAgt = Agent.find("contact is ?", "002257878787").first();
          Compte compte = new Compte();
             compte.setAgent(monAgt);
             compte.setLogin("ebenyx@gmail.com");
             compte.setMot_passe("ebenyx01");
             compte.setTypecpte("1");
            
             if(compte.validateAndSave()){
              System.out.println("ok");
             }
         }
           System.out.println("Verifier ");
              
              Logger.info("Super admin user loaded");
        }else
        {
           System.out.println("Agent nul ");
        }

        }

        if(agt1 == null)
      {
          Agent  agent = new Agent();
        agent.setAdresse("BP 3150 Abj 13");

        agent.setContact("0022547767646");
        
        agent.setEmail("onpc@gmail.com");
        agent.setFonctionaire("Non");
 
        agent.setLibelle_classe("Classe 1");
        agent.setLibelle_contrat("CDI");
        agent.setLibelle_csu_courant("DALOA");
        agent.setLibelle_echelon("Echelon 1");
        agent.setLibelle_grade("Colonel");
        agent.setLibelle_poste("Administrateur");
        agent.setLibelle_sous_dir_courant("DES FINANCES");
        agent.setLibelle_type_agent("Permanant");
        agent.setLieu_naissance("Abidjan");
        agent.setLieuHbtion("Abidjan");
        agent.setMatricule("T121245878");
        agent.setMecano("M12457878");
      
        agent.setNom("BABOUET");
        agent.setNombreEnf((long) 2);
        agent.setNomMere("BABOUET");
        agent.setNomPere("YOBOUET");
        agent.setNum_note_svce("MRSDE124545");
        agent.setNumCni("CI1102121212");
   
        agent.setPrenom("ROBERT");
        agent.setSexe("Masculin");
        agent.setSit_matr("Celibataire");
        
        Date today = Calendar.getInstance().getTime();
       
       SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
          // (3) conversion de la date formatee en String
    
          
        agent.setUpdated(today);
        agent.setCreated(today);
      //  agent.setExpire_le(new Date());
       // agent.setDate_naissance(new Date());
       // agent.setDate_prise_svce_poste_courant( new Date() );
      //  agent.setDelivre_le(new Date());
        
        Agent monAgent = Agent.find("contact is ?", "0022547767646").first();

        if (monAgent == null ) {
          
          if(agent.validateAndSave()){
           Agent monAgt = Agent.find("contact is ?", "0022547767646").first();
           Compte compte = new Compte();
             compte.setAgent(monAgt);
             compte.setLogin("admin@gmail.com");
             compte.setMot_passe("ebenyx01");
             compte.setTypecpte("2");
             if(compte.validateAndSave()){
              System.out.println("ok");
             }
          }
            System.out.println("Verifier ");
              
              Logger.info("Super admin user loaded");
        }

        }
      }

    }