package jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Device;
import models.Alert;
import models.Agent;
import models._Agent_conge;

import com.google.gson.Gson;

import play.Logger;
import play.jobs.Job;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

public class SendAlertJob extends Job {

    private long alertId;
    
    public SendAlertJob(long alertId) {
        this.alertId = alertId;
    }

    @Override
    public void doJob() {

        Logger.info("Start SendAlertJob & Sms");
        _Agent_conge agt_cge = _Agent_conge.findById(alertId);
        agt_cge.setsId(alertId);
        System.out.println("la valeur de sid est:"+agt_cge.getsId());
        String message = agt_cge.getLibelle_conge();
        List<Device> devices = Device.find("type is ?", "android").fetch();
        List <String> regIds = new ArrayList<String>();
        for(Device device : devices){
            regIds.add(device.getRegistrationId());
            Logger.info("Found one");
            Logger.info("*****************************************");
            System.out.println("la valeur de sid est:"+agt_cge.getsId());
            Logger.info("*****************************************");
        }
        final Map<String, String> data = new HashMap<String, String>();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "key=AIzaSyC_oPJNPjTYfiCZPKBm0wZ6Ig_38WdQV3o");
        headers.put("Content-Type", "application/json");

        final Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("registration_ids", regIds);
        params.put("collapse_key", "CONGES");
        
        Gson gson = new Gson();

        data.put("conge", gson.toJson(agt_cge));
        params.put("data", data);
        
        final WSRequest request = WS
                .url("https://android.googleapis.com/gcm/send")
                .headers(headers).body(gson.toJson(params));

        Logger.info("Header %s", request.headers.toString());
        Logger.info("Body %s", request.body.toString());
        final HttpResponse response = request.post();   
        
        Logger.info("Status of GCM response is %s", response.getStatus());
        Logger.info("Body of GCM response is %s", response.getString());
        Logger.info("*****************************************");
        System.out.println("la valeur de sid est:"+agt_cge.getsId());
        Logger.info("*****************************************");
      
      
        }

   

    }

