package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List; 
import java.util.Date;

import models.Agent;
import models.Classe;
import models.Contrat;
import models.Csu;
import play.data.binding.As;
import models.AndroidJson;
import models.BaseModel;
import models.Compte;
import models.Decoration;
import models.Departement;
import models.Echelon;
import models.Emploi;   
import models.Grade;
import models.Pie;
import models.Situation;  
import models.Mode_recrutement;
import models.Sous_direction;
import models.Type_agent;
import models.Type_recrutement;
import models._Agent_classe;
import models._Agent_contrat;
import models._Agent_decoration;
import models._Agent_echelon;
import models._Agent_grade;
import models._Agent_statu;
import models._Piece_agent;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class Agents extends AppController {
	
	public static void menuRecrutement(){
		render();
	}
	public static void enregistrement(){
		render();
	}


	
	public static void saveAgent(Agent agent,_Agent_contrat agentcontrat,String eDate,String vDate,String nDate,String dDate,String fDate, String sexe,String fonctionaire){
		 
		System.out.println("oki");
    
		// (1) obtenir la date du jour
   	    Date today = Calendar.getInstance().getTime();
   	 
   	    // (2) formatage de la date obtenue
   	     SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
   	    // (3) conversion de la date formatee en String
   	    String daynow = formatter.format(today);
   	   /*
   	    * recuperation des libellés en fonction des select
   	    * */ 
   	  Grade mongrade = Grade.findById(agent.getGrade().id);
	  Emploi monemploi = Emploi.findById(agent.getEmplois().id); 
	  Echelon monechelon = Echelon.findById(agent.getEchelon().id);
	  Classe maclasse = Classe.findById(agent.getClasse().id);
	  Situation masituation = Situation.findById(agent.getSituation().id);
	  Type_agent montypeagent = Type_agent.findById(agent.getType_agent().id);
   	  Missions mission = new Missions();
		
	     checkAuthenticity();
	     // renderJSON(agent);
	          if (agent != null) {
	        	  /*
	        	   * verification de la validité des date :date de naissance,de validité et detablissement de la CNI
	        	   * */
	        	  if (
	        			  mission.comparedate(mission.convertirdate(eDate), mission.convertirdate(vDate))
	        					  && mission.comparedate(mission.convertirdate(nDate), mission.convertirdate(daynow))
	        					  && mission.comparedate(mission.convertirdate(nDate), mission.convertirdate(eDate))
	        					  ){
	        		  /*
	        		   * creation d'une nouvelle instance de agent_grade 
	        		   * */
	        		  _Agent_grade agent_grade = new _Agent_grade();
		        	  agent_grade.setAgent(agent);
		        	  agent_grade.setGrade(mongrade);
		        	  agent_grade.setLibelle_grade(mongrade.getLibelle_grade());
		        	  agent_grade.setMatricule_agent(agent.getMatricule());
		        	  agent_grade.setNom_agent(agent.getNom());
		        	  agent_grade.setPrenom_agent(agent.getPrenom());
		        	  /*
	        		   * creation d'une nouvelle instance de agent_echelon 
	        		   * */
		        	  _Agent_echelon agent_echelon = new _Agent_echelon();
		        	  agent_echelon.setAgent(agent);
		        	  agent_echelon.setEchelon(monechelon);
		        	  agent_echelon.setLibelle_echelon(monechelon.getLibelle_echelon());
		        	  agent_echelon.setMatricule_agent(agent.getMatricule());
		        	  agent_echelon.setNom_agent(agent.getNom());
		        	  agent_echelon.setPrenom_agent(agent.getPrenom());
		        	  /*
	        		   * creation d'une nouvelle instance de agent_classe 
	        		   * */
		        	  _Agent_classe agent_classe = new _Agent_classe();
		        	  agent_classe.setAgent(agent);
		        	  agent_classe.setClasse(maclasse);
		        	  agent_classe.setDate_classe(new Date());
		        	  agent_classe.setMatricule_agent(agent.getMatricule());
		        	  agent_classe.setNom_agent(agent.getNom());
		        	  agent_classe.setPrenom_agent(agent.getPrenom());;
		        	 
		        	  agent.setDate_naissance(mission.convertirdate(nDate));
	        		  agent.setDelivre_le(mission.convertirdate(eDate));
	        		  agent.setExpire_le(mission.convertirdate(vDate));
		        	  agent.setEmploi(monemploi.getLibelle_emploi()); 
		      		  agent.setLibelle_grade(mongrade.getLibelle_grade());
		      	      agent.setLibelle_classe(maclasse.getLibelle_classe());
		      	      agent.setLibelle_echelon(monechelon.getLibelle_echelon());
		              agent.setSexe(sexe);
		              agent.setSit_matr(masituation.getLibelle_situation());
		              agent.setFonctionaire(fonctionaire);
		              agent.setLibelle_type_agent(montypeagent.getLibelle_type_agent());
		              /*
		               * verification si l agent est fonctionnaire ou pas */
		              if( fonctionaire.equalsIgnoreCase("Non")){

		            	    if(mission.comparedate(mission.convertirdate(dDate), mission.convertirdate(fDate))&&
		            	    		mission.comparedate(mission.convertirdate(dDate), mission.convertirdate(daynow)))
                            {
		        		  Contrat moncontrat = Contrat.findById(agent.getContrat().id);
		        		  _Agent_contrat agent_contrat = new _Agent_contrat();
			        	  agent_contrat.setAgent(agent);
			        	  agent_contrat.setContrat(moncontrat);
			        	  agent_contrat.setLibelle_contrat(moncontrat.getLibelle_contrat());
			        	  agent_contrat.setDate_debut(mission.convertirdate(dDate));
			        	  agent_contrat.setDate_fin(mission.convertirdate(fDate));
			        	  agent.setContrat(moncontrat);
			        	  agent.setLibelle_contrat(moncontrat.getLibelle_contrat());
    			        	  if(agentcontrat.validateAndSave())
                              {
    			        		  flash.success("agent_contrat.saved.success");
    			        		  
            			        }
            		        }else{
        		        		  flash.error("Verifier que la date de debut du contrat est anterieur a sa date de fin");
        		        		  renderTemplate("Agents/enregistrement.html", agent);
        		        		  System.out.println("Verifier que la date de debut du contrat est anterieur a sa date de fin");
            		        		  
            		        	  }
		            	    }
		              
		            if (agent.validateAndSave()
		            		&& agent_grade.validateAndSave() 
		            		&& agent_echelon.validateAndSave()
		            		&& agent_classe.validateAndSave()
		            	){
		                
		            	 flash.success("ENREGISTREMENT EFFECTUE AVEC SUCCES");
		                  list(null, null, null, 1);
		               }
	        		  
	        	  }else{
	        		  System.out.println("Verifier que la date d'etablissement est anteriéure à celle de validitée");
	        		  flash.error("Verifier que la date d'etablissement est anteriéure à celle de validité");
	    	          renderTemplate("Agents/enregistrement.html", agent);
	        	  }
	             
	          }
	          flash.error("Verifier que la date de naissance est anterieur a la date du jour");
	          renderTemplate("Agents/enregistrement.html", agent);
	       }
	 @Before
	    public static void addViewParameters() {
		 /*
		  * Chargement des liste deroulante:les select
		  * */

	        List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
	        renderArgs.put("allemplois", allemplois); 

          List<Situation> allsituations = Situation.find("order by libelle_situation").fetch();
          renderArgs.put("allsituations", allsituations);

	       
	        List<Contrat> allContrats = Contrat.find("order by libelle_contrat").fetch();
	        renderArgs.put("allContrats", allContrats);
            System.out.println(allContrats);



	        
	        List<Csu> allCsus = Csu.find("order by libelle_csu").fetch();
	        renderArgs.put("allCsus", allCsus);
	        
	        List<Sous_direction> allSousDirections = Sous_direction.find("order by libelle_sous_direction").fetch();
	        renderArgs.put("allSousDirections", allSousDirections);
	        
	        
	        List<Type_agent> allType_agent = Type_agent.find("order by libelle_type_agent").fetch();
	        renderArgs.put("allType_agent", allType_agent);
	        
	        List<Grade> allgrades = Grade.find("order by libelle_grade").fetch();
	        renderArgs.put("allgrades", allgrades);
	        
	        List<Classe> allclasses = Classe.find("order by libelle_classe").fetch();
	        renderArgs.put("allclasses", allclasses);
	        
	        List<Echelon> allechelons = Echelon.find("order by libelle_echelon").fetch();
	        renderArgs.put("allechelons", allechelons);
	        
	        List<Mode_recrutement> allmoderecutements = Mode_recrutement.find("order by libelle_mode_recrutement").fetch();
	        renderArgs.put("allmoderecutements", allmoderecutements);
	        
	        List<Type_recrutement> allrecutements = Type_recrutement.find("order by libelle_type_recrutement").fetch();
	        renderArgs.put("allrecutements", allrecutements);
	        

	        }
	 
	 
	  public static void agentPhoto(long id) {
   	   final Agent agent = Agent.findById(id);
   	   notFoundIfNull(agent);
   	  // response.setContentTypeIfNotSet(agent.getPhotos().type());
   	//   renderBinary(agent.getPhotos().get());
   	}
	 
	public static void delete(long id) {
        checkAuthenticity();
        Agent agent = Agent.findById(id);
        if (agent != null) {
            try {
                agent.delete();
                flash.success("agent.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("agent_has_children.error"));
                view(id);
            }
        } else {
            flash.error("agent.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Agent agent = Agent.findById(id);
            if (agent != null) {
            	 /* creation d un format de date
            	  * */
          	     SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
          	     
          	       /*
          	     * formatage des date recuperées depuis la db
          	     *
          	     * */
          	     
//          	   String nDate = formatter.format(agent.getDate_naissance());
//          	   String eDate = formatter.format(agent.getDelivre_le());
//          	   String vDate = formatter.format(agent.getExpire_le());
                render(agent);
            } else {
                flash.error("agent.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }
    
    public static void liste(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Agent agent) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(agent));
        if (agent != null) {
            if (agent.validateAndSave()) {
                flash.success("agent.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("agent.updated.error");
        renderTemplate("Agents/edit.html", agent);
    }

          public static void edits(long id) {
        if (id > 0) {
            Agent agent = Agent.findById(id);
            Compte object =  new Compte(); 
            if (agent != null) { 
         render(agent,object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
          
          /*
           * enregistrement des droit d un utilisateur
           *  */
                 public static void agentdroitSave(Compte object, Agent agent,String type_droit,String login,String password ){
                     if (object != null) {
                   Missions mission = new Missions();
                     	object.setAgent(agent);
                     	object.setLogin(login);
                     	object.setMot_passe(password);
                     	if (object.validateAndSave()){
                     		flash.success("compte.saved.success");
                     		 list(null,null,null,1);
                     	}
                     	else
                     	{
                     		flash.error("compte.saved.error");
                 	          renderTemplate("Agents/edits.html");
                     		
                     	}
                     	}
                     else{
                     	flash.error("compte.saved.error");
           	          renderTemplate("Agents/edit.html");
                     }
                     	
                     	}

    public static void view(long id) {
        if (id > 0) {
        	Agent agent = Agent.findById(id);
            if (agent != null) {
                render(agent);
              }else
                {list(null, null, null, 1);}
        }
        else{	
            System.out.println(session.get("user"));
            String idagent = session.get("user");
         long  id_agent = Integer.parseInt(idagent);
         String type ="user";
         Agent agent = Agent.findById(id_agent);
         Compte compte = Compte.find("byAgent", agent).first();
         if (agent != null) {
        	 System.out.println(compte);
             render(agent,type);
           }else{
        	   list(null, null, null, 1);  
           }
         
          
          
            }
        }

    /**
     * fonction pour l apllication android
     */
  
    public static void totalagent(){
    	
    	List<AndroidJson> listAnd = new ArrayList<>() ;
    	
    	AndroidJson and = new AndroidJson();
    	long dispo;
    	
    	List<Agent>  agts_total = Agent.findAll();
    	List<Csu>    csu_total =   Csu.findAll();
    	List<Agent>  agts_fem   = Agent.find("bySexe","Feminin").fetch();
    	List<Agent>  agts_max   = Agent.find("bySexe","Masculin").fetch();
    	List<_Agent_statu>  agts_sta   = _Agent_statu.findAll();
    	List<Agent>  agts_aff  = Agent.find("byLibelle_poste","").fetch();
        
    	dispo= agts_total.size()-agts_aff.size()-agts_sta.size();
    	
    	and.setTotal_onpc(agts_total.size());
    	and.setTotal_fem(agts_fem.size());
    	and.setTotal_mas(agts_max.size());
    	and.setTotal_depart(agts_sta.size());
    	and.setTotal_dispo(dispo);
    	and.setCsu_total(csu_total.size());
    	listAnd.add(and);
     
    	renderJSON(listAnd);
    	
    }
    
    public static void repartitioncsu()
    {
    	List<Csu> listcsu = Csu.findAll();
    	List<AndroidJson> listandroid= AndroidJson.findAll();
    	
    	 for (AndroidJson  andr:listandroid){
    		 
    		 andr.delete();
    	 }
 
    	
    	AndroidJson and = null;
    	
    
    	if(listcsu.size() > 0)
    	{
    		List<Agent>  agts_csu_mas = new ArrayList<Agent>();
    		List<Agent>  agts_csu_fem  = new ArrayList<Agent>();
    		String lib_csu = null;
    		for (Csu csu: listcsu) {  
    			lib_csu = csu.getLibelle_csu();
    			System.out.println("LIBELLE :"+lib_csu);
    			if(!lib_csu.equals(""))
    			{
    				and = new AndroidJson();
    				//Agent.find("libelle_csu_courant is ? and sexe = M", lib_csu).fetch();
    				agts_csu_mas  = Agent.find("libelle_csu_courant is ? and sexe = 'Masculin'", lib_csu).fetch();
    			    agts_csu_fem  = Agent.find("libelle_csu_courant is ? and sexe = 'Feminin'", lib_csu).fetch();
    			    //JPA.em().createQuery("select * from Agent agt where sexe ='F' and libelle_csu_courant ='"+lib_csu+"'").getResultList();
    			
    			   // if(agts_csu_mas.size()+agts_csu_fem.size() > 0){

        			 and.setLibelle_csu(csu.getLibelle_csu());
        			 and.setTotal_csu(agts_csu_mas.size()+agts_csu_fem.size());
        			 and.setTotal_csu_mas(agts_csu_mas.size());
        			 and.setTotal_csu_fem(agts_csu_fem.size());
    				 and.save();
    					
    			//	}
    		
    				
    			}else{
    				System.out.println("libelle null");
    				
    			}
    			
             	
       	    }
    		
    		
    	}else{
    		 System.out.println("Tableau vide");
    	}
    	
    	
            final HashMap<String, Object> result = AndroidJson.search(null, null, null, 1);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
     
    	
    }
   

}