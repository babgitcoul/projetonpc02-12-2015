package controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

import models.Agent;
import models.Classe;
import models.Contrat;
import models.Csu;
import play.data.binding.As;
import models.Departement;
import models.Echelon;
import models.Emploi;
import models.Grade;
import models.Situation;
import models.Mode_recrutement;
import models.Sous_direction;
import models.Type_agent;
import models.Type_recrutement;
import models._Agent_classe;
import models._Agent_contrat;
import models.Poste;
import models._Agent_decoration;
import models._Agent_echelon;
import models._Agent_grade;
import play.i18n.Messages;
import play.mvc.Before;
import static play.modules.pdf.PDF.*;
import com.google.gson.Gson;

import models._Agent_poste;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class _Agent_postes extends AppController {

     @Before
        public static void addViewParameters() {
    	 /*
		  * Chargement des listes deroulante:les select
		  * */

            List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
            renderArgs.put("allemplois", allemplois); 

            List<Contrat> allcontrat = Contrat.find("order by libelle_contrat").fetch();
            renderArgs.put("allcontrat", allcontrat);
            
            List<Csu> allCsus = Csu.find("order by libelle_csu").fetch();
            renderArgs.put("allCsus", allCsus);
            
            List<Sous_direction> allSousDirections = Sous_direction.find("order by libelle_sous_direction").fetch();
            renderArgs.put("allSousDirections", allSousDirections);
            
            List<Poste> allPostes  =  Poste.find("order by libelle_poste").fetch();
            renderArgs.put("allPostes", allPostes);

            List<Grade> allgrades = Grade.find("order by libelle_grade").fetch();
            renderArgs.put("allgrades", allgrades);
    
            }

            public static void affecter(_Agent_poste object, Agent agent ,String matricule_agent,String nom_agent,String prenom_agent,long csuId,long posteId,long sousDirId,String note_svce,String type_affectation,String dPriseSvce){
            	if (object != null){ 
            		/*
            		 * recuperation des libellés en fonction des id des select
            		 * */
            		// (1) obtenir la date du jour
               	    Date today = Calendar.getInstance().getTime();
                    String po= "";
                    String sousd = "";
                    String poste_ant = "";
                    String sous_dir_ant = "";
                    if(agent !=null){
                     poste_ant= agent.getLibelle_poste();
                    sous_dir_ant= agent.getLibelle_sous_dir_courant();
               	 }
               	    // (2) formatage de la date obtenue
               	     SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
               	    // (3) conversion de la date formatee en String
               	    String daynow = formatter.format(today);
               	 Missions mission = new Missions();

	            	 
                     
            	System.out.println(csuId);
            	Poste monposte = Poste.findById(posteId);
            	System.out.println(monposte);
            	Csu moncsu   = Csu.findById(csuId);
            	System.out.println(moncsu);
            	Sous_direction masous_dir = Sous_direction.findById(sousDirId);
            	System.out.println(masous_dir);

                if(masous_dir !=null){
                     sousd = masous_dir.getLibelle_sous_direction();
                     System.out.println(sousd);
                     object.setLibelle_sous_dir(sousd);
                     agent.setLibelle_sous_dir_courant(sousd);
                }
                 
               
                
                if(moncsu !=null){
                   String mcsu = moncsu.getLibelle_csu();
                    System.out.println(mcsu);
                    object.setLibelle_csu(mcsu);
                    agent.setLibelle_csu_courant(mcsu);
                }
               

                
                if(monposte !=null){
                    po = monposte.getLibelle_poste();
                   System.out.println(po);
                   object.setLibelle_poste(po);
                   agent.setLibelle_poste(po);
                }
            	
            	object.setCsu(moncsu);
            	object.setDate_prise_svce(mission.convertirdate(dPriseSvce));
                
            	object.setSous_dir(masous_dir);
            	object.setPoste(monposte);
              	
            	object.setAgent(agent);
            	object.setMatricule_agent(agent.getMatricule());
            	object.setNom_agent(agent.getNom());
            	object.setPrenom_agent(agent.getPrenom());
            	object.setNote_svce(note_svce);
            	object.setType_affectation(type_affectation);
            	/*
            	 * avant l'enregistrement de on verifie si l object est vide ou pas
            	 * dans le cas ou il l est alors il s agit d une modifcation
            	 * sinon d 'un nouvel enregistrement
            	 * */
            	if (object.validateAndSave()){
                    if (agent !=null){
                        
                        
                        
                        if(agent.validateAndSave()){
                           flash.success("ENREGISTREE AVEC SUCCES"); 
                          // list(null, null, null, 1);
                           decision(object,agent,po,sousd,poste_ant,sous_dir_ant);
                              }
                      else{
                        flash.error("agent.saved.error");

                      }
                    }

            		flash.success("aposte.saved.success");
            	}

            	}
            	
            	flash.error("aposte.saved.error");
  	          renderTemplate("_Agent_postes/edit.html");
            	}


    public static void recherche(String k, String sF, String sD, int page) {
        HashMap<String, Object> result =Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);

    } 
    
	public static void delete(long id) {
        checkAuthenticity();
        _Agent_poste object = _Agent_poste.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	_Agent_poste object = _Agent_poste.findById(id);
            Agent agent  = object.getAgent();

            if (object != null) {
                render(object,agent);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

        public static void edits(long id) {
        if (id > 0) {
            Agent agent = Agent.findById(id);
            _Agent_poste object = new _Agent_poste();
            if (agent != null) {
            	
         renderTemplate("_Agent_postes/edit.html", agent,object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

    public static void list(String k, String sF, String sD, int page) {
  	  System.out.println(session.get("user"));
  	 if (session.contains("user")){
     	  String type ="user";
     	  System.out.println(session.get("user"));
           long idagent =Integer.parseInt( session.get("user"));
  
          Agent agent = Agent.findById(idagent);
          System.out.println(agent.getNom());
          System.out.println(agent.getPrenom());
           HashMap<String, Object> result = _Agent_poste.search(agent.getNom(), sF, sD, page);
           render(type,result, k, sF, sD, page);  
       }
  	  else{
      	  HashMap<String, Object> result = _Agent_poste.search(k, sF, sD, page);	
      	  render(result, k, sF, sD, page);
        }
       
    
  }

    public static void update(_Agent_poste object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("_Agent_poste/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	_Agent_poste object = _Agent_poste.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
    public static void decision(_Agent_poste object,Agent agent,String po,String sousd,String poste_ant,String sous_dir_ant){
        renderPDF(object,agent,po,sousd,poste_ant,sous_dir_ant);

    }

}
