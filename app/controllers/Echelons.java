package controllers;

import java.util.HashMap;

import models.Echelon;
import play.i18n.Messages;

import com.google.gson.Gson;

public class Echelons extends AppController {
	
	public static void delete(long id) {
        checkAuthenticity();
        Echelon object = Echelon.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Echelon object = Echelon.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Echelon.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Echelon object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Artcs/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Echelon object = Echelon.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
