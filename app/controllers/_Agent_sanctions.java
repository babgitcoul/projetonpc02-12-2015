package controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Contrat;
import models.Csu;
import models.Decoration;  
import models.Emploi;
import models.Grade;
import models.Poste;
import models.Sanction;
import models.Sous_direction;
import models._Agent_poste;
import models._Agent_sanction;
import models._Agent_sanction;  
import play.i18n.Messages;
import play.mvc.Before;
import models.Agent;

import com.google.gson.Gson;

public class _Agent_sanctions extends AppController {
	
    @Before
    public static void addViewParameters() {

        List<Sanction> allsanction = Sanction.find("order by libelle_sanction").fetch();
        renderArgs.put("allsanction", allsanction); 
        }
	
	
	
	public static void delete(long id) {
        checkAuthenticity();
        _Agent_sanction object = _Agent_sanction.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("ENREGISTREE AVEC SUCCES");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

public static void agentsanctionSave(_Agent_sanction object, Agent agent,String agent_matri,String agent_name,String agent_prenom,long sanctionId,String agent_dted_sanct,String agent_dtef_sanct,String agent_motif_sanct){
  
	
	if (object != null) {
		
		// (1) obtenir la date du jour
   	    Date today = Calendar.getInstance().getTime();
   	 
   	    // (2) formatage de la date obtenue
   	     SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
   	    // (3) conversion de la date formatee en String
   	    String daynow = formatter.format(today);
   	  
		
		Missions mission = new Missions();
			   if(mission.comparedate(mission.convertirdate(agent_dted_sanct), mission.convertirdate(agent_dtef_sanct)))
			   {
				   object.setDate_debut(mission.convertirdate(agent_dted_sanct));  
				   object.setDate_fin(mission.convertirdate(agent_dtef_sanct));
			   }
			   else
			   {  
				   flash.error("la date de début de la sanction ne peut être anterieur a celle de fin");
		          renderTemplate("_Agent_sanctions/edit.html",object,agent);
		      }
		
    	Sanction masanct = Sanction.findById(sanctionId);
    	object.setAgent(agent);
    	object.setLibelle_sanction(masanct.getLibelle_sanction());
    	object.setSanction(masanct);
    	object.setNom_agent(agent.getNom());
    	object.setPrenom_agent(agent.getPrenom());
    	object.setMotif(agent_motif_sanct);
    	object.setMatricule_agent(agent_matri);
    	if (object.validateAndSave()){
    		flash.success("ENREGISTREE AVEC SUCCES");
    		// renderTemplate("_Agent_sanctions/list.html");
            list(null,null,null,1);
    	}
    	else
    	{
    		flash.error("sanction.saved.error");
	          renderTemplate("_Agent_decorations/edit.html");
    		
    	}
    	}
    else{
    	flash.error("sanction.saved.error");
        renderTemplate("_Agent_decorations/edit.html");
    }
    	
    	}
    public static void edit(Long id) {
        if (id != null) {
        	_Agent_sanction object = _Agent_sanction.findById(id);
            Agent agent  = object.getAgent();
            if (object != null) {
                render(object,agent);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

            public static void edits(long id) {
        if (id > 0) {
            Agent agent = Agent.findById(id);
            _Agent_sanction object = new _Agent_sanction();
            if (agent != null) {
         renderTemplate("_Agent_sanctions/edit.html", agent,object);
            }
        }
        flash.error("agent.not_found");
        list(null, null, null, 1);
    }

    public static void recherche_agent_sanction(String k, String sF, String sD, int page) {
        HashMap<String, Object> result =Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);

    }


    public static void list(String k, String sF, String sD, int page) {
    	  System.out.println(session.get("user"));
    	  if (session.contains("user")){
        	  String type ="user";
        	  System.out.println(session.get("user"));
              long idagent =Integer.parseInt( session.get("user"));
     
             Agent agent = Agent.findById(idagent);
             System.out.println(agent.getNom());
             System.out.println(agent.getPrenom());
              HashMap<String, Object> result = _Agent_sanction.search(agent.getNom(), sF, sD, page);
              render(type,result, k, sF, sD, page);  
          }
    	  else{
        	  HashMap<String, Object> result = _Agent_sanction.search(k, sF, sD, page);	
        	  render(result, k, sF, sD, page);
          }
          
      
    }

    public static void update(_Agent_sanction object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("_Agent_decirations/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	_Agent_sanction object = _Agent_sanction.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
