package controllers.api;

import java.util.Map;
import play.data.validation.Required;
import controllers.AppController;
import models.BaseModel;
import java.util.HashMap;
import models.Compte;
import play.data.validation.Validation;

public class Comptes extends AppController {

    public static void list(String query, String paginateField, String paginateDirection, int page) {
        if (!Validation.hasErrors()) {
            final HashMap<String, Object> result = Compte.search(null, paginateField, paginateDirection, page);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
        }
    }
    public static void loginAndroid(@Required String matricule,  @Required String mot_passe){
    
    final Map<String, Boolean> data = new HashMap<String, Boolean>();
    
    Compte cpte = Compte.find("matricule is ? and mot_passe is ? ",matricule,mot_passe).first();
    
    if( cpte != null ){
    data.put("error", false);
    renderJSON(data);
    }else{
    data.put("error", true);
    renderJSON(data);
    }
   }

}
