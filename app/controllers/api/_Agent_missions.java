package controllers.api;

import java.util.HashMap;

import controllers.AppController;
import models.BaseModel;
import models._Agent_mission;
import play.data.validation.Validation;

public class _Agent_missions extends AppController {

    public static void list(String query, String paginateField, String paginateDirection, int page) {
        if (!Validation.hasErrors()) {
             final HashMap<String, Object> result = _Agent_mission.search(null, null, null, 1);
            renderJSON(result.get(BaseModel.PAGINATE_OBJECT_KEY));
        }
    }

}
