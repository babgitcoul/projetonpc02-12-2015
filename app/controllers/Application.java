package controllers;

import play.*;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.*;
import play.mvc.Scope.RenderArgs;

import java.text.SimpleDateFormat;
import java.util.*;

import models.*;
import static play.modules.pdf.PDF.*;

public class Application extends Controller {
	
	 public static void content() {
		 List<Agent> agents = Agent.findAll();
         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
         
         System.out.println(agentsF.size());
         System.out.println(agentsM.size());
         System.out.println(agents.size());
         renderArgs.put("agents",agents.size());
         renderArgs.put("agentsM",agentsM.size());
         renderArgs.put("agentsF",agentsF.size());
      /*les demandes de congés*/
         
         List<_Agent_conge> alldemde = _Agent_conge.findAll();
         List<_Agent_conge> alldemdeAccd = _Agent_conge.find("status_conge is ?","2").fetch();
         List<_Agent_conge> alldemdeRef = _Agent_conge.find("status_conge is ?","1").fetch();
         List<_Agent_conge> alldemdeRep = _Agent_conge.find("status_conge is ?","0").fetch();
         renderArgs.put("alldemde",alldemde.size());
         renderArgs.put("alldemdeAccd",alldemdeAccd.size());
         renderArgs.put("alldemdeRep",alldemdeRep.size());
         /*les mutations et promotion*/
         List<_Agent_poste> alldpost = _Agent_poste.findAll();
         List<_Agent_poste> alldpostMut = _Agent_poste.find("type_affectation is ?","Mutation").fetch();
         List<_Agent_poste> alldpostPro = _Agent_poste.find("type_affectation is ?","Promotion").fetch();
         
         renderArgs.put("alldpost",alldpost.size());
         renderArgs.put("alldpostMut",alldpostMut.size());
         renderArgs.put("alldpostPro",alldpostPro.size());
         
         /*les mutations et formation*/
         List<_Agent_formation> allform = _Agent_formation.findAll();
         List<_Agent_formation> allformvacp = _Agent_formation.find("status is ?","1").fetch();
         List<_Agent_formation> allformvali = _Agent_formation.find("status is ?","2").fetch();
         
         renderArgs.put("allform",allform.size());
         renderArgs.put("allformvacp",allformvacp.size());
         renderArgs.put("allformvali",allformvali.size());
		 
		 renderTemplate("stat.html");
	    }
	 
	 public static void param() {
		 /*les agents*/   
		 List<Agent> agents = Agent.findAll();
         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
         renderArgs.put("agents",agents.size());
         renderArgs.put("agentsM",agentsM.size());
         renderArgs.put("agentsF",agentsF.size());
         
         /*les demandes de congés*/
         
         List<_Agent_conge> alldemde = _Agent_conge.findAll();
         List<_Agent_conge> alldemdeAccd = _Agent_conge.find("status_conge is ?","2").fetch();
         List<_Agent_conge> alldemdeRef = _Agent_conge.find("status_conge is ?","1").fetch();
         List<_Agent_conge> alldemdeRep = _Agent_conge.find("status_conge is ?","0").fetch();
         renderArgs.put("alldemde",alldemde.size());
         renderArgs.put("alldemdeAccd",alldemdeAccd.size());
         renderArgs.put("alldemdeRep",alldemdeRep.size());
         /*les mutations et promotion*/
         List<_Agent_poste> alldpost = _Agent_poste.findAll();
         List<_Agent_poste> alldpostMut = _Agent_poste.find("type_affectation is ?","Mutation").fetch();
         List<_Agent_poste> alldpostPro = _Agent_poste.find("type_affectation is ?","Promotion").fetch();
         
         renderArgs.put("alldpost",alldpost.size());
         renderArgs.put("alldpostMut",alldpostMut.size());
         renderArgs.put("alldpostPro",alldpostPro.size());
         
         /*les mutations et formation*/
         List<_Agent_formation> allform = _Agent_formation.findAll();
         List<_Agent_formation> allformvacp = _Agent_formation.find("status is ?","1").fetch();
         List<_Agent_formation> allformvali = _Agent_formation.find("status is ?","2").fetch();
         
         renderArgs.put("allform",allform.size());
         renderArgs.put("allformvacp",allformvacp.size());
         renderArgs.put("allformvali",allformvali.size());
         
         
		 renderTemplate("param.html");
	    }
	 public static void login() {
		 
		 renderTemplate("Application/login.html");
	    }

	 public static void wizard() {
		 
		 List<Agent> agents = Agent.findAll();
    	 
         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
         
         System.out.println(agentsF.size());
         System.out.println(agentsM.size());
         System.out.println(agents.size());
         renderArgs.put("agents",agents);
         renderArgs.put("agentsM",agentsM);
         renderArgs.put("agents",agents);
		 renderTemplate("wizard.html");
		 
	    }

    public static void index() {
    	
    	
    	 List<Agent> agents = Agent.findAll();
    	 
         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
         System.out.println(agentsF.size());
         System.out.println(agentsM.size());
         System.out.println(agents.size());
         renderArgs.put("agents",agents);
         renderArgs.put("agentsM",agentsM);
         renderArgs.put("agentF",agents);
    	 Application.login();
    	 
    }
    
    public static void update() {
    	Application.login();
    }
    
  
    public static void client() {
 List<Agent> agents = Agent.findAll();
    	 
         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
         
         System.out.println(agentsF.size());
         System.out.println(agentsM.size());
         System.out.println(agents.size());
         renderArgs.put("agents",agents);
         renderArgs.put("agentsM",agentsM);
         renderArgs.put("agentsF",agents);
   	 renderTemplate("Client.html");
    }
    public static void maternite(){
        Date today = Calendar.getInstance().getTime();
         SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // (3) conversion de la date formatee en String
        String daynow = formatter.format(today);
System.out.println(daynow);
              renderPDF(daynow);
    } 
    /*
     * methode de generation du l'attestation de fin de stage*/
    public static void att_fin_stage(long id){
        Date today = Calendar.getInstance().getTime();
         SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // (3) conversion de la date formatee en String
        String daynow = formatter.format(today);
        Agent agent = Agent.findById(id);
        System.out.println(daynow); 
        System.out.println(agent);
        
        //verifion le typpe de contrat de 'agent
        String contrat = agent.getLibelle_contrat();
        if (contrat.equals("STAGE")){
        	 renderPDF(agent);	
        }
        else
        {
        	flash.error("Cet agent n'est pas un stagiaire");
        	Agents.liste(null, null, null, 1);
        	
        }
       
             
    }
    public static void attest_de_stage(long id){
        Date today = Calendar.getInstance().getTime();
         SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // (3) conversion de la date formatee en String
        String daynow = formatter.format(today);
        Agent agent = Agent.findById(id);
        
        //verifion le typpe de contrat de 'agent
        String contrat = agent.getLibelle_contrat();
        if (contrat.equals("STAGE"))
          {
             renderPDF(agent);  
          }
        else
        {
            flash.error("Cet agent n'est pas un stagiaire");
            Agents.liste(null, null, null, 1);
            
        }
    }
    public static void autorisation(long id){
        Date today = Calendar.getInstance().getTime();
         SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // (3) conversion de la date formatee en String
        String daynow = formatter.format(today);
        Agent agent = Agent.findById(id);
        
        //verifion le typpe de contrat de 'agent
        String contrat = agent.getLibelle_contrat();
        
        
        	 renderPDF(agent,daynow);	
        
        
    }
    public static void autoriston_absence(long id){
    	Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
       // (3) conversion de la date formatee en String
       String daynow = formatter.format(today);
       Agent agent = Agent.findById(id);
       
       //verifion le typpe de contrat de 'agent
      
       
       
       	 renderPDF(agent,daynow);
    	
    }
    
    public static void cert_de_prise_svce(long id){
    	 Agent agent = Agent.findById(id);
    	 renderPDF(agent);
    	
    }
    public static void cert_de_reprise_svce(long id){
    	long diffDays=0;
    	Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
       // (3) conversion de la date formatee en String
       String daynow = formatter.format(today);
    	_Agent_conge demande = _Agent_conge.findById(id);
 	  // _Agent_piece_conge pieces =_Agent_piece_conge.find("byAgent_demande", demande).first();
 	   System.out.println(demande);
 	   if( demande.getDate_fin().getTime() != 0 && demande.getDate_debut().getTime() !=0){
 		   long diff =  demande.getDate_fin().getTime() - demande.getDate_debut().getTime();
     	    diffDays = diff / (24 * 60 * 60 * 1000);
     	   System.out.println("nombre de jours "+diffDays);   
 	   }
 	
 	  // System.out.println(pieces.getLibelle_piece());
 	   if(demande != null){
 		   Agent agent = demande.getAgent();
 		  renderPDF(demande,diffDays,daynow);

 	   } else{
 		  flash.error("Une erreur lors de la génération du certificat");
          Agents.liste(null, null, null, 1);
 		   
 	   } 
   	
   }
    
    public static void congesValides(){
 	   
 	      List<_Agent_conge> objects = _Agent_conge.find("status_conge ='2'").fetch();
 	      renderArgs.put("objects", objects);
 		   render (objects);
 	     
    }
        public static void cert_de_cessation_de_svce(long id){
        	long diffDays=0;
        	_Agent_conge demande = _Agent_conge.findById(id);
     	  // _Agent_piece_conge pieces =_Agent_piece_conge.find("byAgent_demande", demande).first();
     	   System.out.println(demande);
     	   if( demande.getDate_fin().getTime() != 0 && demande.getDate_debut().getTime() !=0){
     		   long diff =  demande.getDate_fin().getTime() - demande.getDate_debut().getTime();
         	    diffDays = diff / (24 * 60 * 60 * 1000);
         	   System.out.println("nombre de jours "+diffDays);   
     	   }
     	
     	  // System.out.println(pieces.getLibelle_piece());
     	   if(demande != null){
     		   Agent agent = demande.getAgent();
     		  renderPDF(demande,diffDays);
 
     	   } else{
     		  flash.error("Une erreur lors de la génération du certificat");
              Agents.liste(null, null, null, 1);
     		   
     	   } 
        
    }
        //camember des agents par csu
        public static List<Pie> plotByType(){
        	HashMap<String, Integer> map = new HashMap<String, Integer>();
        	List<Csu> listcsu = Csu.findAll();
        	List<Pie> pies = new ArrayList<Pie>();
        	if(listcsu.size() > 0)
        	{
        		List<Agent>  agts_csu_mas = new ArrayList<Agent>();
        		List<Agent>  agts_csu_fem  = new ArrayList<Agent>();
        		List<Agent>  agts_csu_total  = new ArrayList<Agent>();
        		String lib_csu = null;
        		for (Csu csu: listcsu) {  
        			lib_csu = csu.getLibelle_csu();
        			System.out.println("LIBELLE :"+lib_csu);
        			if(!lib_csu.equals(""))
        			{
        				agts_csu_mas    = Agent.find("libelle_csu_courant is ? and sexe = 'Masculin'", lib_csu).fetch();
        			    agts_csu_fem    = Agent.find("libelle_csu_courant is ? and sexe = 'Feminin'", lib_csu).fetch();
        			    agts_csu_total  = Agent.find("libelle_csu_courant is ?", lib_csu).fetch();
        			    
        				if(map.containsKey(lib_csu)){
                			map.put(lib_csu,1 );
                		}else{
                			map.put(lib_csu, agts_csu_total.size());
                		}
        				//Agent.find("libelle_csu_courant is ? and sexe = M", lib_csu).fetch();
        				

        			}else{
        				System.out.println("libelle null");
        				
        			}
                 	
           	    }
        		Set<String> sets = map.keySet();
            	
            	for(String set : sets){
            		pies.add(new Pie(set, map.get(set)));
            	}
            	renderJSON(pies);
            
        	}
        	return pies;
        	}
        
        //camember des agents par Sexe
        public static List<Pie> agentSexe(){
        	HashMap<String, Integer> map = new HashMap<String, Integer>();
        	List<Pie> pies = new ArrayList<Pie>();
        	List<Agent>	agts_csu_mas    = Agent.find("sexe = 'Masculin'").fetch();
            List<Agent>  agts_csu_fem    = Agent.find("sexe = 'Feminin'").fetch();
            map.put("Masculin", agts_csu_mas.size());
            map.put("Feminin", agts_csu_fem.size());

        		Set<String> sets = map.keySet();
            	
            	for(String set : sets){
            		pies.add(new Pie(set, map.get(set)));
            	}
            	renderJSON(pies);
            return pies;
        	}
        
        //camember des agents pour les conges
        public static List<Pie> congesStat(){
        	HashMap<String, Integer> map = new HashMap<String, Integer>();
        	List<Pie> pies = new ArrayList<Pie>();
        	List<_Agent_conge>	dmde_accepte    = _Agent_conge.find("status_conge = '2'").fetch();
            List<_Agent_conge>  dmde_attente    = _Agent_conge.find("status_conge = '1'").fetch();
            List<_Agent_conge>  dmde_rejete    = _Agent_conge.find("status_conge = '0'").fetch();
            map.put("Accepté", dmde_accepte.size());
            map.put("en attente", dmde_attente.size());
            map.put("rejeté", dmde_rejete.size());

        		Set<String> sets = map.keySet();
            	
            	for(String set : sets){
            		pies.add(new Pie(set, map.get(set)));
            	}
            	renderJSON(pies);
            return pies;
        	}




        public static void donnees(){
        	List<Csu> listcsu = Csu.findAll();
        	List<ChartCsu>  chartCsuList  = new ArrayList<ChartCsu>();
        	//final Map<String, List<AndroidJson>> data = new HashMap<String, List<AndroidJson>>();
        	AndroidJson and =  new AndroidJson();
        	ChartCsu chart  = new ChartCsu();
        	if(listcsu.size() > 0)
        	{
        		List<Agent>  agts_csu_mas = new ArrayList<Agent>();
        		List<Agent>  agts_csu_fem  = new ArrayList<Agent>();
        		String lib_csu = null;
        		for (Csu csu: listcsu) {  
        			lib_csu = csu.getLibelle_csu();
        			System.out.println("LIBELLE :"+lib_csu);
        			if(!lib_csu.equals(""))
        			{
        				and = new AndroidJson();
        				//Agent.find("libelle_csu_courant is ? and sexe = M", lib_csu).fetch();
        				agts_csu_mas  = Agent.find("libelle_csu_courant is ? and sexe = 'Masculin'", lib_csu).fetch();
        			    agts_csu_fem  = Agent.find("libelle_csu_courant is ? and sexe = 'Feminin'", lib_csu).fetch();
            			 and.setLibelle_csu(csu.getLibelle_csu());
            			 and.setTotal_csu(agts_csu_mas.size()+agts_csu_fem.size());
            			 and.setTotal_csu_mas(agts_csu_mas.size());
            			 and.setTotal_csu_fem(agts_csu_fem.size());
            			 chart.values.add(and);
        					
        			//	}
        		
        				
        			}else{
        				System.out.println("libelle null");
        				
        			}
                 	
           	    }
        		
        	}else{
        		 System.out.println("Tableau vide");
        	}
        	
        	//data.put("values", values);
        	chart.setKey("Cumulative Return");
        	chartCsuList.add(chart);
			renderJSON(chartCsuList);
               
        }
    public static void charts(){
    	renderTemplate("charts.html");
    }
    public static void chartSexe(){
    	renderTemplate("chartSexe.html");
    }
    public static void congesStats(){
    	renderTemplate("congesStat.html");
    }

}