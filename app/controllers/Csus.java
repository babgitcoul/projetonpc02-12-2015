package controllers;

import java.util.HashMap;
import java.util.List;

import models.Arpc;
import models.Csu;
import models.Ville;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class Csus  extends AppController{
	
	/**
	 * La liste des villes et artc a charger au demarrage
	*/
    @Before
    public static void addViewParameters() {
    
        List<Ville> allvilles = Ville.find("order by libelle_ville").fetch();
        renderArgs.put("allvilles", allvilles);    
        
        List<Arpc> allarpcs = Arpc.find("order by libelle_arpc").fetch();
        renderArgs.put("allarpcs", allarpcs);  
    }
    
	public static void delete(long id) {
        checkAuthenticity();
        Csu object = Csu.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Csu object = Csu.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Csu.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Csu object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Csus/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Csu object = Csu.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
}
