package controllers;

import java.util.HashMap;

import models._Agent_decoration;
import play.i18n.Messages;
import models.Agent;  

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Decoration;
import models.Grade;
import models.Contrat;
import models.Csu;
import models.Emploi;
import models.Poste;
import models.Sous_direction;
import models._Agent_decoration;
import models._Agent_poste;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class _Agent_decorations extends AppController {

     @Before
        public static void addViewParameters() {
    	 /*
		  * Chargement des liste deroulante:les select
		  * */

            List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
            renderArgs.put("allemplois", allemplois); 

            List<Contrat> allcontrat = Contrat.find("order by libelle_contrat").fetch();
            renderArgs.put("allcontrat", allcontrat);
            
            List<Csu> allCsus = Csu.find("order by libelle_csu").fetch();
            renderArgs.put("allCsus", allCsus);
            
            List<Sous_direction> allSousDirections = Sous_direction.find("order by libelle_sous_direction").fetch();
            renderArgs.put("allSousDirections", allSousDirections);
            
            List<Poste> allPostes  =  Poste.find("order by libelle_poste").fetch();
            renderArgs.put("allPostes", allPostes);

            List<Grade> allgrades = Grade.find("order by libelle_grade").fetch();
            renderArgs.put("allgrades", allgrades);
    
            List<Decoration> alldecorations = Decoration.find("order by libelle_decoration").fetch();
            renderArgs.put("alldecorations", alldecorations);
            }

     /*
      * enregistrement de la decoration d un agent
      *  */
            public static void agentdecoSave( _Agent_decoration object, Agent agent,String agent_matri,String agent_nom,String agent_prenom,String agent_dte_dco,long decoId,String agent_lieuDeco,String agent_deco_par,String agent_motif_deco){
                if (object != null) {
                	
                	Decoration madeco = Decoration.findById(decoId);
                	String libDeco = madeco.getLibelle_decoration();
                	Missions mission = new Missions();
                	object.setAgent(agent);
                	object.setDecorateur(agent_deco_par);
                	object.setLieu_decoration(agent_lieuDeco);
                	object.setNom_agent(agent.getNom());
                	object.setPrenom_agent(agent.getPrenom());
                	object.setDecoration(madeco);
                	object.setLibelle_decoration(libDeco);
                	object.setMotif_decoration(agent_motif_deco);
                	object.setMatricule_agent(agent.getMatricule());
                	object.setDate_decoration(mission.convertirdate(agent_dte_dco));
                	
                	if (object.validateAndSave()){
                		flash.success("ENREGISTREE AVEC SUCCES");
                		recherche_agent_deco(null,null,null,1);
                	}
                	else
                	{
                		flash.error("decoration.saved.error");
            	          renderTemplate("_Agent_decorations/edit.html");
                		
                	}
                	}
                else{
                	
      	          renderTemplate("_Agent_decorations/edit.html");
                }
                	
                	}
              
	/*
	 * Suppression d'un agent*/
	public static void delete(long id) {
        checkAuthenticity();
        _Agent_decoration object = _Agent_decoration.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	_Agent_decoration object = _Agent_decoration.findById(id);
            Agent agent  = object.getAgent();
            System.out.println(agent);
            System.out.println(object);
            System.out.println(object.getMotif_decoration());
            if (object != null) {
            	/* creation d un format de date
           	  * */
         	     SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
         	    /*
         	     * formatage des date recuperées depuis la db*/
         	 
                render(object,agent);
                System.out.println("motif"+object.getMotif_decoration());
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }
            public static void edits(long id) {
        if (id > 0) {
            Agent agent = Agent.findById(id);
            _Agent_decoration object =  new _Agent_decoration();
            if (agent != null) {
            
         renderTemplate("_Agent_decorations/edit.html", agent,object);
         System.out.println("motif"+object.getMotif_decoration());
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
     /*
      * Recherche d'un agent en fonction du matricule
      * */
     public static void rechercher(String matricule){
        Agent agent = Agent.find("byMatricule", matricule).first();
                System.out.println(matricule);
                System.out.println("essai");
                if (agent != null){
                 System.out.println(agent); 
              
                 
                 renderTemplate("_Agent_decorations/edit.html", agent);
                }
                else
                {
                System.out.println("aucun agent avec ce matricule"); 
                renderTemplate("_Agent_decorations/edit.html",agent);
                }  
            }
     /*
      * fonction de recherche de l agent qui sera decoré par la suite 
      * */

            public static void recherche_agent_deco(String k, String sF, String sD, int page) {
        HashMap<String, Object> result =Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);

    }

    public static void list(String k, String sF, String sD, int page) {
    	  System.out.println(session.get("user"));
    	  if (session.contains("user")){
        	  String type ="user";
        	  System.out.println(session.get("user"));
              long idagent =Integer.parseInt( session.get("user"));
     
             Agent agent = Agent.findById(idagent);
             System.out.println(agent.getNom());
             System.out.println(agent.getPrenom());
              HashMap<String, Object> result = _Agent_decoration.search(agent.getNom(), sF, sD, page);
              render(type,result, k, sF, sD, page);  
          }
    	  
    	  else{
        	  HashMap<String, Object> result = _Agent_decoration.search(k, sF, sD, page);	
        	  render(result, k, sF, sD, page);
        	  
          }
         
      
    }

    public static void update(_Agent_decoration object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("_Agent_decorations/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	_Agent_decoration object = _Agent_decoration.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
    

}
