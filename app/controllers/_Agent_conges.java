/**
 * 
 */
package controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jobs.SendAlertJob;
import models.Agent;
import models.Alert;
import models.Conge;
import models._Agent_conge;
import models._Agent_piece_conge;
import models._Agent_poste;
import models._Piece_agent_conge;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import play.i18n.Messages;
import play.libs.Mail;
import play.mvc.Before;

import com.google.gson.Gson;

import static play.modules.pdf.PDF.*;

/**
 * @author TOSHIBA
 *
 */
public class _Agent_conges  extends AppController {
	

	public static void retourConge(){
		render();
	}
	
	public static void demande_de_conge(){
		render();
	}
	

    @Before
       public static void addViewParameters() {
   	 /*
		  * Chargement des listes deroulante:les select
		  * */

          List<Conge> allconges = Conge.find("order by libelle_conge").fetch();
           renderArgs.put("allconges", allconges); 


         
           }
    /*
     * methode d'enregistrement d'une demande de conge dans la table _Agent_conge
     * */

          public static void demandeCongeSave(_Agent_conge object,long congeId,String eDate,String vDate,String motif,String nombreJours){
           	if (object != null){ 
           		Date today = Calendar.getInstance().getTime();
              	 
           	    // (2) formatage de la date obtenue
           	   SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
           	    // (3) conversion de la date formatee en String
           	    String daynow = formatter.format(today);
           		/*
           		 * recuperation des libellés en fonction des id des select
           		 * */
           	Missions mission = new Missions();//pour formater les differentes dates
           	Conge monconge = Conge.findById(congeId);
           	System.out.println(monconge);
            object.setConge(monconge);
            object.setLibelle_conge(monconge.getLibelle_conge());
            
            System.out.println(session.get("user"));
            /*
             * verification de session ;si elle contient un element qui a pour clè user*/
      	  if (session.contains("user")){
          	   String type ="user";
               long idagent =Integer.parseInt( session.get("user"));
               Agent agent = Agent.findById(idagent);
               object.setAgent(agent);
               object.setNom_agent(agent.getNom()); 
               object.setPrenom_agent(agent.getPrenom());
               object.setLibelle_csu(agent.getLibelle_csu_courant());
               object.setLibelle_sous_direction(agent.getLibelle_sous_dir_courant());
               object.setLibelle_poste_agent(agent.getLibelle_poste());
               object.setStatus_conge("1");
               object.setNombreJours(nombreJours);
               
           }  
           if (
                  mission.comparedate(mission.convertirdate(daynow), mission.convertirdate(eDate))){
        	          if(mission.comparedate(mission.convertirdate(eDate), mission.convertirdate(vDate)))
        	            {
        	        	  object.setDate_fin(mission.convertirdate(vDate));
        	        	  object.setDate_debut(mission.convertirdate(eDate));
        	          }
        	   
        	     else{
        	    	 flash.error("la date de debut du congé doit etre anterieur à sa date de fin "); 
                          renderTemplate("_Agent_conges/edit.html",object);
                  	 }
           }
        	   else{
        		   flash.error("la date de debut du congé ne peut pas etre anterieur à la date du jour ");
                    renderTemplate("_Agent_conges/edit.html",object);
                  }
            
           	object.setMotif(motif);
         
          
           	/*
           	 * avant l'enregistrement de on verifie si l object est vide ou pas
           	 * dans le cas ou il l est alors il s agit d une modifcation
           	 * sinon d 'un nouvel enregistrement
           	 * */
           	if (object.validateAndSave()){
           		
           		/*envoi d'une notification
           		 * creation du message qui sera envoyé 
           		 * */
           		Alert alert = new Alert();
           		alert.setTitle("ONPC");
           		alert.setContent("nouvelle demande de congés effectuée");
           		if (alert.validateAndSave()){
           			Alert alerts = Alert.find("byTitle", "ONPC").first();
           			SendAlertJob sendalertjob = new SendAlertJob(object.getId());
           			sendalertjob.doJob();
           			System.out.println("alert envoyée");
           		}else{
           			System.out.println("alert non  envoyée");
           		}       
           		    System.out.println("demande enregistrée");  
           		     flash.success("demande.saved.success");  
                          list(null,null,null,1);
                             }
           	}
                       flash.error("erreur lors de l enregistrement");
                       renderTemplate("_Agent_conges/edit.html",object);

                     }
          
          public static void planningConges(String k, String sF, String sD, int page){
        	  HashMap<String, Object> result = _Agent_conge.search(k, sF, sD, page);
              render(result, k, sF, sD, page);
      	}
          /*
           * methode de recherche de congés en fonction des date de deut,de fin,du type et du status
           * */
          public static void congeSeach(String dDate,String fDate,String type, String status){
        	  Missions mission = new Missions();
        	  /*
        	   * conversion des dates
        	   * */
        	  
          }
                  
           


   public static void recherche(String k, String sF, String sD, int page) {
       HashMap<String, Object> result =Agent.search(k, sF, sD, page);
       render(result, k, sF, sD, page);

   }
   
   /*recuperation  de la liste des conges deja validé de l agent 
    * 
    * */
   public static void congeValide(){
	   if (session.contains("user")){
           long idagent =Integer.parseInt( session.get("user"));
           Agent agent = Agent.findById(idagent);
	      List<_Agent_conge> objects = _Agent_conge.find("agent is ? and status_conge ='2'", agent).fetch();
	      renderArgs.put("objects", objects);
		   render ();
	     
   }
	   
   }
	public static void delete(long id) {
       checkAuthenticity();
       _Agent_conge object = _Agent_conge.findById(id);
       if (object != null) {
           try {
               object.delete();
               flash.success("object.deleted.success");
               list(null, null, null, 1);
           } catch (Exception e) {
               flash.error(Messages.get("object_has_children.error"));
               view(id);
           }
       } else {
           flash.error("object.deleted.error");
           list(null, null, null, 1);
       }
   }
	public static void refuserconge(long idobject) throws EmailException{
		System.out.println(idobject);
		System.out.println("refuser");
		_Agent_conge congeAgent = _Agent_conge.findById(idobject);
		if (congeAgent !=null){
			congeAgent.setStatus_conge("0");
			if(congeAgent.validateAndSave()){
				SimpleEmail email = new SimpleEmail();
				email.setFrom("babcoul88@gmail.com");
				email.addTo(congeAgent.getAgent().getEmail());
				email.setSubject("subject");
				email.setMsg("Votre demande a été refusée pour des raisons que vous recevrez ulterieurement");
				Mail.send(email);
				System.out.println("congés refusé");
			 flash.success("DEMANDE DE CONGES  REFUSEE");
				
					planningConges(null,null,null,1);
			}
		}else{
			flash.error("object.updated.error");
			detailConges(idobject);
		}
	}
	
	public static void accepterconge(long idobject) throws EmailException{
		System.out.println(idobject);
		System.out.println("accepter");
		_Agent_conge congeAgent = _Agent_conge.findById(idobject);
		if (congeAgent !=null ){
			congeAgent.setStatus_conge("2");
			if(congeAgent.validateAndSave()){
				 flash.success("DEMANDE DE CONGES ACCEPTEE");
				SimpleEmail email = new SimpleEmail();
				email.setFrom("gestiononpc@gmail.com");
				email.addTo(congeAgent.getAgent().getEmail());
				System.out.println(congeAgent.getAgent().getEmail());
				email.setSubject("subject");
				email.setMsg("Votre demande a été acceptée");
				Mail.send(email);
				System.out.println("congés accepté");
				
				planningConges(null,null,null,1);
			}
		}else{
			flash.error("object.updated.error");	
			planningConges(null,null,null,1);
		}
	}
	
	public static void retourdeconge(long idobject) throws EmailException{
		System.out.println(idobject);
		System.out.println("refuser");
		_Agent_conge congeAgent = _Agent_conge.findById(idobject);
		if (congeAgent !=null){
			congeAgent.setStatus_conge("4");
			if(congeAgent.validateAndSave()){
			flash.success("DEMANDE DE CONGES REPORTEE");
				SimpleEmail email = new SimpleEmail();
				email.setFrom("babcoul88@gmail.com");

				email.addTo(congeAgent.getAgent().getEmail());
				email.setSubject("subject");
				email.setMsg("Votre demande a été reportée à une date ulterieur");
				Mail.send(email);
				System.out.println("congés reporté");
				
				planningConges(null,null,null,1);
				 renderJSON("true");
			}
		}else{
			flash.error("object.updated.error");
			planningConges(null,null,null,1);
		}
	}
	/*
	 * methode permettant d'afficher les detaills de la piece justificative selectionnée
	 *  sur la vue pieceview elle prend en parametre l'id de _Agent_piece_conge puisque celle ci 
	 *  a été denormalisér en inscrivant toutes les propriétés de la classe _Agent_piece dans la classe _Agent_piece_conge
	 *   
	 *  */
   public static void pieceview(long id){
	   System.out.println(id);
	   _Agent_piece_conge object = _Agent_piece_conge.findById(id);
	   if (object !=null){
		   System.out.println(id_demande);
		   renderArgs.put("id_demande", id_demande);
		   renderArgs.put("object", object);
	   render();
	   }else{
		   detailConges(id_demande);
	   }
   }
   
   public static void agentPiece(long id) {
	   final _Agent_piece_conge pieceAgentConge = _Agent_piece_conge.findById(id);
	   notFoundIfNull(pieceAgentConge);
	   response.setContentTypeIfNotSet(pieceAgentConge.getPiece().type());
	   renderBinary(pieceAgentConge.getPiece().get());
	}

   public static void edit(Long id) {
       if (id != null) {
       	_Agent_conge object = _Agent_conge.findById(id);
           Agent agent  = object.getAgent();

           if (object != null) {
               render(object,agent);
           } else {
               flash.error("object.not_found");
               list(null, null, null, 1);
           }
       }
       render();
   }

       public static void edits(long id) {
       if (id > 0) {
           Agent agent = Agent.findById(id);
           _Agent_poste object = new _Agent_poste();
           if (agent != null) {
           	
        renderTemplate("_Agent_conges/edit.html", agent,object);
           }
       }
       flash.error("object.not_found");
       list(null, null, null, 1);
   }

   public static void list(String k, String sF, String sD, int page) {
 	  System.out.println(session.get("user"));
 	  if (session.contains("user")){
     	  String type ="user";
     	  System.out.println(session.get("user"));
           long idagent =Integer.parseInt( session.get("user"));
  
          Agent agent = Agent.findById(idagent);
          System.out.println(agent.getNom());
          System.out.println(agent.getPrenom());
           HashMap<String, Object> result = _Agent_conge.search(agent.getNom(), sF, sD, page);
           render(type,result, k, sF, sD, page);  
       }
 	  
 	  else{
     	  HashMap<String, Object> result = _Agent_conge.search(k, sF, sD, page);	
     	  render(result, k, sF, sD, page);
     	  
       }
   }

   public static void update(_Agent_poste object) {
       checkAuthenticity();
       System.out.println(new Gson().toJson(object));
       if (object != null) {
           if (object.validateAndSave()) {
               flash.success("object.updated.success");
               list(null, null, null, 1);
           }
       }
       flash.error("object.updated.error");
       renderTemplate("_Agent_conges/edit.html", object);
   }
   
   /*
    * appelle de la vue detailconges en lui passant en paramettre
    * l'id de la demande qui sera utilsé pour rechercher la demande correspondante */
   
   /*cette variable sera utilisée dans la methode pieceview.elle permet de revenir a la vue qui precede pieceview en cas d echec
    * c est dire a la vue detailConges.html*/
   public static long id_demande;
   
   public static void detailConges(long id){
	   if(id>0){
		id_demande= id;
	   System.out.println(id);
	  _Agent_conge object = _Agent_conge.findById(id);
	   System.out.println(object);
	   Agent agent = object.getAgent();
	   List<_Agent_piece_conge> piecesAgent = _Agent_piece_conge.find("byAgent_demande",object).fetch();
	   System.out.println(piecesAgent);
	   renderArgs.put("piecesAgent", piecesAgent);
	   renderArgs.put("object", object);
	   renderArgs.put("agent", agent);
	   render ();
   }
	   else{
		   list(null, null, null, 1);
	   }
   }
   
   
   public static void view(long id) {
       if (id > 0) {
    	   if (session.contains("user")){
    		   String type="user";
    		   _Agent_conge object = _Agent_conge.findById(id);
               if (object != null) {
                   render(object,type);
               }else  {
            	   flash.error("object.not_found");
               list(null, null, null, 1);
               }
    	   }else{
    		   flash.error("object.not_found");
    	       list(null, null, null, 1);  
    	   }  
       	
       }
      
   }
   public static void etats(long sid,long choix) throws EmailException{
	   System.out.println(choix);System.out.println(sid);
	   final Map<String, Boolean> data = new HashMap<String, Boolean>(); 
	   if(choix==0){
		   System.out.println("rejeté");
		   System.out.println(sid);
			_Agent_conge congeAgent = _Agent_conge.findById(sid);
			if (congeAgent !=null)
			{
				congeAgent.setStatus_conge("0");
				if(congeAgent.validateAndSave())
				{
					SimpleEmail email = new SimpleEmail();
					email.setFrom("babcoul88@gmail.com");
					email.addTo(congeAgent.getAgent().getEmail());
					email.setSubject("subject");
					email.setMsg("Votre demande a été reportée à une date ulterieur");
					Mail.send(email);
					System.out.println("congés reporté");
					flash.success("demande.saved.success");
					data.put("error", false);
					renderJSON(data);
				 }
			   else
			     {
				   flash.error("object.updated.error");
				   data.put("error", true);
				   renderJSON(data);
			     }
		   
	   }
	}
	   if(choix==1){
		   System.out.println("reporté");
			_Agent_conge congeAgent = _Agent_conge.findById(sid);
			if (congeAgent !=null){
				 congeAgent.setStatus_conge("3");
				if(congeAgent.validateAndSave())
				  {
					SimpleEmail email = new SimpleEmail();
					email.setFrom("babcoul88@gmail.com");
					email.addTo(congeAgent.getAgent().getEmail());
					email.setSubject("subject");
					email.setMsg("Votre demande a été refusée pour des raisons que vous recevrez ulterieurement");
					Mail.send(email);
					System.out.println("congés refusé");
					 flash.success("demande.saved.success");
					 data.put("error", false);
					 renderJSON(data);
				  }
			   else
			      {
				   flash.error("object.updated.error");
				   data.put("error", true);
				   renderJSON(data);
			     }
	   }
	} 
	   if(choix==2){
		   System.out.println("accepter");
			_Agent_conge congeAgent = _Agent_conge.findById(sid);
			if (congeAgent !=null ){
				congeAgent.setStatus_conge("2");
				if(congeAgent.validateAndSave()){
					SimpleEmail email = new SimpleEmail();
					email.setFrom("gestiononpc@gmail.com");
          String adresse = congeAgent.getAgent().getEmail() ;
					email.addTo(adresse);
					System.out.println(adresse);
					email.setSubject("subject");
					email.setMsg("Votre demande a été acceptée");
					Mail.send(email);
					System.out.println("congés accepté");
					 flash.success("demande.saved.success");
					 data.put("error", false);
					 renderJSON(data);
				}
			else{
				flash.error("object.updated.error");
				data.put("error", true);
				 renderJSON(data);
			}
			}
	   } 
	   
	   
   }
   public static void maternite(_Agent_conge demande,_Agent_piece_conge pieces,long diffDays){
	   renderPDF(demande,pieces,diffDays);
   }
   public static void conge_de_paternite(_Agent_conge demande,_Agent_piece_conge pieces,long diffDays){
	   renderPDF(demande,pieces,diffDays);
   }
   public static void certificat(long id) {
	   _Agent_conge dmde = _Agent_conge.findById(id);
	   _Agent_piece_conge pieces =_Agent_piece_conge.find("byAgent_demande", dmde).first();
	   System.out.println(dmde);
	   long diff =  dmde.getDate_fin().getTime() - dmde.getDate_debut().getTime();
	   long diffDays = diff / (24 * 60 * 60 * 1000);
	   System.out.println("nombre de jours "+diffDays);
	   System.out.println(pieces.getLibelle_piece());
	   if(dmde != null){
		   Agent agent = dmde.getAgent();
		   String lib_conge= dmde.getLibelle_conge().toUpperCase();
		   switch(lib_conge){
		   
		   case "MATERNITE": 
			   maternite(dmde,pieces,diffDays);
			   break;
		   case "PATERNITE":
			   conge_de_paternite(dmde,pieces,diffDays);
			   break;
			   default:
				   congeValide();
				   
			   
		   }
		   
	   }
    

   }

}

