package controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Contrat;
import models.Csu;
import models.Decoration;
import models.Emploi;
import models.Formateur;
import models.Formateur_formation;
import models.Formation;
import models.Grade;   
import models.Poste;
import models.Sous_direction;
import models._Agent_conge;
import models._Agent_demande;
import models._Agent_formation;
import models._Agent_formation;
import models._Agent_sanction;
import models._Piece_agent;
import play.i18n.Messages;
import play.mvc.Before;

public class _Agent_formations extends AppController {

    @Before
       public static void addViewParameters() {
   	 /*
		  * Chargement des liste deroulante:les select
		  * */

           List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
           renderArgs.put("allemplois", allemplois); 

           List<Contrat> allcontrat = Contrat.find("order by libelle_contrat").fetch();
           renderArgs.put("allcontrat", allcontrat);
           
           List<Csu> allCsus = Csu.find("order by libelle_csu").fetch();
           renderArgs.put("allCsus", allCsus);
           
           List<Sous_direction> allSousDirections = Sous_direction.find("order by libelle_sous_direction").fetch();
           renderArgs.put("allSousDirections", allSousDirections);
           
           List<Poste> allPostes  =  Poste.find("order by libelle_poste").fetch();
           renderArgs.put("allPostes", allPostes);

           List<Grade> allgrades = Grade.find("order by libelle_grade").fetch();
           renderArgs.put("allgrades", allgrades);
   
           List<Decoration> alldecorations = Decoration.find("order by libelle_decoration").fetch();
           renderArgs.put("alldecorations", alldecorations);
           }

    /*
    
             
	/*
	 * Suppression d'un agent*/
	public static void delete(long id) {
       checkAuthenticity();
       _Agent_formation object = _Agent_formation.findById(id);
       if (object != null) {
           try {
               object.delete();
               flash.success("object.deleted.success");
               list(null, null, null, 1);
           } catch (Exception e) {
               flash.error(Messages.get("object_has_children.error"));
               view(id);
           }
       } else {
           flash.error("object.deleted.error");
           list(null, null, null, 1);
       }
   }
	   public static void edit(Long id) {
	       if (id != null) {
	    	   _Agent_formation object = _Agent_formation.findById(id);
	           Agent agent  = object.getAgent();

	           if (object != null) {
	               render(object,agent);
	           } else {
	               flash.error("object.not_found");
	               list(null, null, null, 1);
	           }
	       }
	       render();
	   }
	
	 public static void view(long id) {
	       if (id > 0) {
	    	   _Agent_formation object = _Agent_formation.findById(id);
	           if (object != null) {
	        	   if(session.contains("user")){
	        		   String type = "user";
	        		   render(object,type);
	        	   }else{
	               render(object);
	        	   }
	           }
	       }
	       flash.error("object.not_found");
	       list(null, null, null, 1);
	   }
	 /*methode permettant de voir les details d'une demande de formation*/
	 public  static long  id_dmd;
	 public static void detailDemande(long id){
		 if(id>0){
			 id_dmd=id;
		 _Agent_formation agt_form = _Agent_formation.findById(id);
		 //recuperer l agent qui a fait la demande
		 Agent agent = agt_form.getAgent();
		 //recuperation de la formation
		 Formation formation = agt_form.getFormation();
		 render(agent,formation);
		 }else
		 {
			 list(null,null,null,1);
		 }
		 
		 
		 
	 }
	 /*
	  * methode de validation de la demande de formation
	  * on recupere la demande de l agent pour en modifier la date debut,de fin et le lieu de la formation*/
	 
	 public static void validerDemande(_Agent_formation object,String eDate,String vDate,String lieu_formation){
		 _Agent_formation agt_form = _Agent_formation.findById(id_dmd);
		 if (agt_form != null){
			 //on verifie les dates
			     Date today = Calendar.getInstance().getTime();
        	    
               SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        	     String daynow = formatter.format(today);
        	     Missions mission = new Missions();//pour formater les differentes dates
        	 if (
                     mission.comparedate(mission.convertirdate(daynow), mission.convertirdate(eDate))){
           	          if(mission.comparedate(mission.convertirdate(eDate), mission.convertirdate(vDate)))
           	            {
           	        	agt_form.setDate_fin(mission.convertirdate(vDate));
           	        	agt_form.setDate_debut(mission.convertirdate(eDate));
           	          }
           	   
           	     else{
           	    	 flash.error("la date de debut de la formation doit etre anterieur à sa date de fin "); 
                             detailDemande(id_dmd);
                     	 }
              }
           	   else{
           		   flash.error("la date de debut de la formation ne peut pas etre anterieur à la date du jour ");
           		      detailDemande(id_dmd);
                     }
               
        	 agt_form.setLieu_formation(lieu_formation);
        	 agt_form.setStatus("2");
			 
		 }
if(agt_form.validateAndSave()){
	flash.success("demande.saved.success");  
    list(null,null,null,1);
}
		 
		 
		 
	 }
	 /*
	  * annulation  d une demade de formation*/
	    public static void annulerDmde(Long id) {
	        if (id != null) { 
	        	System.out.println(id);
	        	_Agent_formation object = _Agent_formation.findById(id);
	        	System.out.println(object);
	        	String statu_demande = object.getStatus();
	        	System.out.println(statu_demande);
	        	 if (statu_demande.equalsIgnoreCase("2")){
	        		 System.out.println("vous ne pouvez pas annuler car elle a ete deja validé");
	        		 flash.error("vous ne pouvez pas annuler car elle a été deja validée");
	        		 list(null, null, null, 1);
	        		 
	        	 }
	        	 else{
	        		 System.out.println("modification du status");
	        		 object.setStatus("0");
	        		 if (object.validateAndSave()){
	        		 Formations.list(null, null, null, 1);
	        		 }
	        	 }
	        }else{
	        	list(null, null, null, 1);
	        }
	    }
	
	   public static void list(String k, String sF, String sD, int page) {
		   
		   if (session.contains("user")){
	           long idagent =Integer.parseInt( session.get("user"));
	           Agent agent = Agent.findById(idagent);
	           String type= "user";
		      List<_Agent_formation> objects = _Agent_formation.find("agent is ?  and status='1'", agent).fetch();
		      renderArgs.put("objects", objects);
		      renderArgs.put("type", type);
			   render ();
		     
	      }
		else
		{
	       HashMap<String, Object> result = _Agent_formation.search(k, sF, sD, page);
	       render(result, k, sF, sD, page);
	   }
	 }
	   /*
	    * ramene la liste de toutes les demande de carte ou de diplome de l agent*/
	   public static void listeDemandeAgent() {
		   
		   if (session.contains("user")){
	           long idagent =Integer.parseInt( session.get("user"));
	           Agent agent = Agent.findById(idagent);
	           String type= "user";
		      List<_Agent_demande> objects = _Agent_demande.find("agent is ?  and status='1'", agent).fetch();
		      renderArgs.put("objects", objects);
		      renderArgs.put("type", type);
			   render ();
		     
	      }
		
	 }
	   
	public static void formationValidees(){
		 if (session.contains("user")){
	           long idagent =Integer.parseInt( session.get("user"));
	           Agent agent = Agent.findById(idagent);
	           String type= "user";
		      List<_Agent_formation> objects = _Agent_formation.find("agent is ?  and status='2'", agent).fetch();
		      int taille = objects.size();
		      if(taille == 0){
		      renderArgs.put("taille", taille);  
		      }
		      renderArgs.put("objects", objects);
		      renderArgs.put("type", type);
			   render ();
		 }
	}  
	
	public static void formationRealisees(){
		 if (session.contains("user")){
	           long idagent =Integer.parseInt( session.get("user"));
	           Agent agent = Agent.findById(idagent);
	           String type= "user";
		      List<_Agent_formation> objects = _Agent_formation.find("agent is ?  and status='3'", agent).fetch();
		      int taille = objects.size();
		      if(taille == 0){
		      renderArgs.put("taille", taille);  
		      }
		      renderArgs.put("objects", objects);
		      renderArgs.put("type", type);
			   render ();
		 }
	} 
	
public static void demande_carte(long id){
	_Agent_formation object = _Agent_formation.findById(id);
	_Agent_demande agt_dmd = _Agent_demande.find("formation is ? and agent is ?",object.getFormation(),object.getAgent()).first();
	if(agt_dmd != null){
		System.out.println("vous avez deja effectué une demande de carte pour cette formation");
		flash.error("vous avez deja effectué une demande de carte pour cette formation.la date de retrait vous sera communiqué");
		formationValidees();
	}
	else{
		_Agent_demande agt_dmde = new _Agent_demande(new Date(), "0", "CARTE", object.getFormation().getDiplome(),
				object.getFormation().getCarte(), object.getAgent().getNom(),
				object.getAgent().getPrenom(), object.getAgent().getMatricule(),
				object.getFormation(), object.getAgent());
		if (agt_dmde.validateAndSave()){
			flash.success("votre demande a été prise en compte.la date de retrait vous sera communiqué");
			formationValidees();
			
		}else{
			
		}
	}		
}


public static void demande_diplome(long id){
	_Agent_formation object = _Agent_formation.findById(id);
	_Agent_demande agt_dmd = _Agent_demande.find("formation is ? and agent is ?",object.getFormation(),object.getAgent()).first();
	if(agt_dmd != null){
		System.out.println("vous avez deja effectué une demande de carte pour cette formation");
		flash.error("vous avez deja effectué une demande de dipôme pour cette formation.la date de retrait vous sera communiqué");
		formationValidees();
	}
	else{
		_Agent_demande agt_dmde = new _Agent_demande(new Date(), "0", "DIPLOME", object.getFormation().getDiplome(),
				object.getFormation().getCarte(), object.getAgent().getNom(),
				object.getAgent().getPrenom(), object.getAgent().getMatricule(),
				object.getFormation(), object.getAgent());
		if (agt_dmde.validateAndSave()){
			flash.success("votre demande a été prise en compte.la date de retrait vous sera communiqué");
			formationValidees();
			
		}else{
			
		}
	}	
	
}
	
	public static void agent_formation_save(long id){
		
		Formateur_formation  object = Formateur_formation.findById(id);
		
		if (session.contains("user")){
			long idagent =Integer.parseInt( session.get("user"));
            Agent agent = Agent.findById(idagent);
            Formation formation = object.getFormation();
            Formateur formateur= object.getFormateur();
            System.out.println(agent);
            System.out.println(formation);
            System.out.println(formateur);
            _Agent_formation agt_form = _Agent_formation.find("formation is ? and agent is ? and status='1'", formation,agent).first();
            if (agt_form !=null){
            	System.out.println("vous avez deja postulé a cette formation");	
            	flash.error("vous avez deja postulé a cette formation");
            	Formations.list(null, null, null, 1);
            	
            }else{
            	_Agent_formation agt_format = new _Agent_formation();
            	agt_format.setAgent(agent);
            	agt_format.setFormation(formation);
            	agt_format.setNom_agent(agent.getNom());
            	agt_format.setPrenom_agent(agent.getPrenom());
            	agt_format.setFormateur_nom(formateur.getFormateur_nom());
            	agt_format.setDiplome(formation.getDiplome());
            	agt_format.setCarte(formation.getCarte());
            	agt_format.setLibelle_formation(formation.getLibelle_formation());
            	agt_format.setStatus("1");
             if (agt_format.validateAndSave()){
            	 System.out.println("ok");
            	 list(null,null,null,1);
            	 
             }
             else{
            	 System.out.println("pas ok");
            	 flash.error("votre demande n'a pas été prise en compte");
             	Formations.list(null, null, null, 1);
               }
            	
            }
          }
		}
	}