package controllers;

import java.util.HashMap;

import models.Status;
import play.i18n.Messages;

import com.google.gson.Gson;

public class Statuss  extends AppController{
	
	public static void delete(long id) {
        checkAuthenticity();
        Status object = Status.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Status object = Status.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Status.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Status object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Artcs/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Status object = Status.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
