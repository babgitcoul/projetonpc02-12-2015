package controllers;

import models.User;
import play.mvc.Before;
import play.mvc.Util;
import play.mvc.With;
import controllers.deadbolt.Deadbolt;

@With({ Deadbolt.class })
public class BaseController extends AppController {

    @Util
    public static User getCurrentUser() {
        String username = session.get(User.SESSION_KEY);
        if (username != null) {
            return User.findByUsername(username);
        } else {
            return null;
        }
    }

    @Before
    @Util
    static void setViewParameters() {
        User user = BaseController.getCurrentUser();
        renderArgs.put(User.VIEW_VARIABLE_NAME, user);
    }

}
