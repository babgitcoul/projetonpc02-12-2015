package controllers;

import java.text.ParseException;
import static play.modules.pdf.PDF.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Mission;
import models.Ressource;
import models._Agent_conge;
import models._Agent_mission;
import models._Agent_piece_conge;
import models._ressources_mission;
import play.db.jpa.JPA;
import play.i18n.Messages;

import com.google.gson.Gson;

public class _Agent_missions extends AppController {
	
		public static void delete(long id) {
        checkAuthenticity();
        _Agent_mission object = _Agent_mission.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }
	
	public static void edit(Long id) {
        if (id != null) {
        	
        	 Mission object = Mission.findById(id); 	 
        	
        	_Agent_mission objects = _Agent_mission.findById(id);
            if (object != null) {
                render(object);
                render(objects);
                System.out.println(new Gson().toJson(object));
              
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }
	/**
	 * La fonction permet d afficher les informations d'une mission sur la page missionDetail 
	 * la variable test permet de conserver la valeur de la mission
	 */
	public static long test;
	public static void edits(long id) {
        if (id > 0) {
        	Mission object = Mission.findById(id);
        	Agent agent = Agent.findById(object.getChef_mission());
        	test = object.getId();
        	
        	if (object != null) {
            	System.out.println(object); 	
            	System.out.println(agent); 
            	System.out.println(test);
    
            	renderTemplate("_Agent_missions/missionDetail.html",object,agent);
            
          
            }
        }
  
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
	

	/**
	 * la fonction permet de retour au details de la mission
	 */
	public static void retour() {
        if (test > 0) {
        	Mission object = Mission.findById(test);
        	Agent agent = Agent.findById(object.getChef_mission());
                	
        	if (object != null) {
            	
            renderTemplate("_Agent_missions/missionDetail.html", object,agent);
          
            }
        }
  
    }
	/**
	 * La fonction permet d'affecter une personne a une mission 
	 * @param id
	 */
    public static void affectation(long id ) {
        if (id > 0) {
        	Agent   agent   = Agent.findById(id); 
        	Mission mission = Mission.findById(test);
        	System.out.println(agent);
        	System.out.println( "Id de mission ="+test);
        	System.out.println( mission);
        
        	if (agent != null) {
        		if(mission.getChef_mission() == 0 )
        		{
        			String chef ="chef";
        			renderArgs.put("chef",chef);
        		}
        			
        		renderTemplate("_Agent_missions/edit.html",agent,mission);
                
             
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = _Agent_mission.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }
    
    /**
     * liste de mission pour la planification(affectation d'agent et de ressources)
     * @param k
     * @param sF
     * @param sD
     * @param page
     */
    public static void listmission(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Mission.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }
    /**
     * la liste des mission pour la planification des retour de mission
     * @param k
     * @param sF
     * @param sD
     * @param page
     */
    public static void liste(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Mission.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    /**
     * la fonctipn d'associer un agent a une mission
     * @param object
     * @param libelle_mission
     * @param lieu_mission
     * @param objet_mission
     * @param depart
     * @param retour
     * @param id
     * @param agendid l'identifiant de l'agent
     */

    
    public static void update(_Agent_mission object,String libelle_mission,String lieu_mission, String objet_mission,
    		String depart,String retour,long id,long agentid,String chefmission)
    {
    	
        checkAuthenticity(); 
        System.out.println(new Gson().toJson(object));
    	Mission mamission = Mission.findById(id);
    
    	Agent  agent = Agent.findById(agentid);
		System.out.println("Status mission="+mamission.getStatus()+""+mamission.getChef_mission()+" "+ chefmission);
     
    	if(mamission.getStatus().equalsIgnoreCase("Non"))
    	{
    		/**
    		 * requette pour verifier si l'agent participe deja a la mission donnee
    		 */
		    		List<_Agent_mission>  mis =  JPA.em().createQuery("select miss from _Agent_mission miss where mission_id ="+mamission.getId()+"and agent_id="+agent.getId()).getResultList();
		    		System.out.println("Resultat de la liste="+mis);
		    		
		    		if(mis.size()== 0)
		    		{
				    		if (object != null) 
						        {
				    		
						        	object.setMission(mamission);
						        	object.setAgent(agent);
						        	object.setLibelle_mission(libelle_mission);
						        	object.setLeiu(lieu_mission);
						        	object.setObjet(objet_mission);
						        	object.setDate_depart(mamission.getDate_de_depart());
						        	object.setDate_reour(mamission.getDate_de_retour());
						        	
						        	 if(mamission.getChef_mission() == 0){
						        		 
						        		 mamission.setChef_mission(Long.parseLong(chefmission)); 
						        		 if (mamission.validateAndSave()) {
						                    // flash.success("object.updated.success");
						                     System.out.println(new Gson().toJson(mamission));
						                     System.out.println("Enregistrer");
						                 }
						        	  
						        		 
								      }else{
								    	   // flash.error("Un chef de mission existe deja");
								    		System.out.println("Un chef de mission existe deja");
								      }    
						        	
						       
						            if (object.validateAndSave()) {            	
						             	
						               flash.success("AGENT AJOUTE AVEC SUCCES");
						               // renderTemplate("_Agent_missions/edit.html", object);
						                agent(null,null,null,1);
						               retour();
						            }
						        }
						      //  flash.error("object.updated.error");
				      //  agent(null, null, null, 1);
		    		}
		    		else{
		    			System.out.println("Agent deja inscrit pour la mission"); 
		    	        flash.error("AGENT DEJA AFFECTE A CETTE MISSION");
		    			 agent(null,null,null,1);
		    			}
		       }
		    	else
			    	{
    		     System.out.println("Verifier le status de la mission");
	    		  flash.error("Verifier le status de la mission");
	    	      listmission(null, null, null, 1);
	    	      
			    	} 
		        
    }
   
    public static long id_odre;
    public static void view(long id) {
        if (id > 0) {
        	id_odre=id;
        	Mission object = Mission.findById(id);
        	Agent agent = Agent.findById(object.getChef_mission());  
        	/**
        	 * Les differentes liste des missions et ressources lies a la mission
        	 */
            List<_Agent_mission> missionnaires   = _Agent_mission.find("byMission_id",object.getId()).fetch();
        	List<_ressources_mission> ressources = _ressources_mission.find("byMission_id",object.getId()).fetch(); 
        	
        	
        	/**
        	 * Les listes de type ressource et agent
        	 */
        	
        	List<Ressource> mesressources = new ArrayList<Ressource>();
        	List<Agent> agents = new ArrayList<Agent>();
        	
        	/**
        	 * Objet de type ressource et agent
        	 */
        	Ressource maressource;
        	Agent     lagent;
        	
        	for (_ressources_mission res: ressources) {  	
        		
       	     	maressource = Ressource.findById(res.getRessource().getId());
       	     	if(maressource!=null){
       	     		mesressources.add(maressource); 
       	     	}
       	    }
        	
        	System.out.println(missionnaires);
        	for (_Agent_mission miss: missionnaires) { 
        		
        		lagent = Agent.findById(miss.getAgent().getId());
        		if (lagent !=null){
                	
                	agents.add(lagent);
        		}
        		         	
       	        	           	    
       	    }
        
        	if (object != null) {
        		 renderArgs.put("agents", agents);
        		 renderArgs.put("mesressources", mesressources);
                 renderTemplate("_Agent_missions/view.html",object,agent);             
              
            }
        }
        flash.error("object.not_found");
       listmission(null, null, null, 1);
    }
  
    /**
     * Fonction de recherche d'un agent par matricule
     * @param matricule,nom,prenom
     */
    public static void agent(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Agent.search(k, sF, sD, page);        
        render(result, k, sF, sD, page);
       
    }
    /**
     * generation du pdf de l'ordre de mission
     * @param id_mission
     */
    public static void ordre_de_mission(Mission mission,Agent agent){
  	   renderPDF(mission,agent);
     }
     public static void ordreMission(){
     	Mission object = Mission.findById(id_odre);
     	Agent agent = Agent.findById(object.getChef_mission()); 
     	ordre_de_mission(object,agent);
     }
    /**
     * Fonction de recherche d'une ressources
     * @param libelle_ressource
     */
    public static void ressource(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Ressource.search(k, sF, sD, page);        
        render(result, k, sF, sD, page);
       
    }
    /**
     * la fonctin permet d'affecter une ressouce a une mission
     * @param id
     */
    public static void ressourceadd(long id ) {
        if (id > 0) {
        	Ressource   ressource   = Ressource.findById(id); 
        	Mission mission = Mission.findById(test);        
        	if (ressource != null) {
                renderTemplate("_Agent_missions/ressourceadd.html", ressource,mission);
                flash.success("object.not_found");
             retour();
            }
        }
        flash.error("object.not_found");
        ressource(null, null, null, 1);
    }
    /**
     * la fonction permet d'associer les ressources aux misssions
     * @param object 
     * @param mission
     * @param ressource
     */

    public static void updateresource(_ressources_mission object,long mission,long ressource)
    {
        checkAuthenticity(); 
        System.out.println(new Gson().toJson(object));
        if (object != null) 
        {
        	       	
        	Mission mamission = Mission.findById(mission);
        	Ressource  maressource = Ressource.findById(ressource);
        	
        	if(mamission.getStatus().equalsIgnoreCase("Non"))
        	{
        	System.out.println("Etat statut mission : "+mamission.getStatus());
        	System.out.println(maressource.getDisponibilite());
        	//test du status de la disponibilite de la ressource
		        	if (maressource.getDisponibilite().equalsIgnoreCase("Oui"))
		        	{
		        	        System.out.println("Etat ressource : "+maressource.getDisponibilite());
				        	  object.setMission(mamission);
				        	  object.setRessource(maressource);     
				        	
				            if (object.validateAndSave()) {    
				            	
				               maressource.setDisponibilite("Non");
				               maressource.validateAndSave();
				            	 
				               flash.success("RESSOURCE BIEN ENREGISTRE");
				               System.out.println("succes");
				               ressource(null, null, null, 1);
				               //retour();
				            }
				        	}else{
				        		System.out.println("Verifier la valeur de la disponibilite");
				        	      flash.error("RESSOURCE NON DISPONIBLE");
				        		ressource(null, null, null, 1);
				        	}
        	//fin de la verification et de l'insertion selon le cas
        	
		        	}
		        	else{
		        		System.out.println("Verifier le statut de la mission");
		        		 listmission(null, null, null, 1);
		        		
		        		}
        }
        else{
        
        flash.error("object.updated.error");
        ressource(null, null, null, 1);
        System.out.println("Oject vide");
        
        }
        
        
    }
    
   
    public static void mesMissions(){
    	//recuperation de l agent qui est dans la session
    	
    	if(session.contains("user")){
    		 long idagent =Integer.parseInt( session.get("user"));
             Agent agent = Agent.findById(idagent);
             List<_Agent_mission> objects = _Agent_mission.find("agent is ? ", agent).fetch();
             renderArgs.put("objects", objects);
             render();
    	}
    }
    public static void detailmission(long id){
    	_Agent_mission massion = _Agent_mission.findById(id);
    	Mission object = massion.getMission();
    	Agent agent = Agent.findById(object.getChef_mission());
    	render(object,agent);

    	
    }

}
