package controllers;

import java.util.HashMap;
import java.util.List;

import models.Formateur;
import models.Formateur_formation;
import models.Formation;
import models._Agent_conge;
import models._Piece_agent;
import play.db.jpa.JPA;
import play.i18n.Messages;

import com.google.gson.Gson;

public class Formateurs extends AppController {
	
	public static void delete(long id) {
        checkAuthenticity();
        Formateur object = Formateur.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }
	
	 public static void formateurLogo(long id) {
  	   final Formateur pieceAgentConge = Formateur.findById(id);
  	   notFoundIfNull(pieceAgentConge);
  	   response.setContentTypeIfNotSet(pieceAgentConge.getLogo().type());
  	   renderBinary(pieceAgentConge.getLogo().get());
  	}

    public static void edit(Long id) {
        if (id != null) {
        	Formateur object = Formateur.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Formateur.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }
    
    public static long id_form;
    public static void edits(long id,String k, String sF, String sD, int page) {
    if (id > 0) {
    	id_form = id;
    	System.out.println(id);
    	//Formateur_formation form = Formateur_formation.findById(id);
    	Formateur formateur = Formateur.findById(id);
    	List<Formateur_formation> objects = Formateur_formation.find("formateur is ?", formateur).fetch();
    	if (objects.size() != 0){
    		//Formation object = form.getFormation();
    		//HashMap<String, Object> result = Formateur_formation.search(k, sF, sD, page);
    		renderArgs.put("objects", objects);
    		renderArgs.put("formateur", formateur);
    		render();
    		
    	}else{
    		render();
    		
    	}     
    }
    
}
    public static void formationSave(Formation object){
    	checkAuthenticity();
    	if(object !=null){
    		if (object.validateAndSave()){
    			Formateur formateur = Formateur.findById(id_form);
    			if (formateur != null){
    				List<Formateur_formation>  form =  JPA.em().createQuery("select fftion from Formateur_formation fftion where formateur_id ="+formateur.getId()+"and formation_id="+object.getId()).getResultList();
    		       
    		        if(form.size()==0){
    				Formateur_formation format = new Formateur_formation();
    				format.setAdresse(formateur.getAdresse());
    				format.setCarte(object.getCarte());
    				format.setDescription(object.getDescription());
    				format.setDiplome(object.getDiplome());
    				format.setEmail(formateur.getEmail());
    				format.setFormateur(formateur);
    				format.setFormateur_nom(formateur.getFormateur_nom());
    				format.setFormation(object);
    				format.setLibelle_formation(object.getLibelle_formation());
    				format.setLocalite(formateur.getLocalite());
    				format.setSiege(formateur.getSiege());
    				format.setSite_web(formateur.getSite_web());
    				//format.setTelephone(formateur.getTelephone());
    				   if(format.validateAndSave()){
    					   flash.success("object.saved.success");
    					   System.out.println("nouvelle ligne enregistrée");
    		                list(null, null, null, 1);  
    					   }
    		        }else{
    		        	 System.out.println("Resultat de la liste="+form);
    		        	 for(Formateur_formation fftion: form){
    		        		 fftion.setCarte(object.getCarte());
    		        		 fftion.setDescription(object.getDescription());
    		        		 fftion.setDiplome(object.getDiplome());
    		        		 fftion.setLibelle_formation(object.getLibelle_formation());
    		        		 if(fftion.validateAndSave()){
    		        			 flash.success("object.saved.success"); 
    		        			 System.out.println("ligne mise a jour"); 
    		        		 }
    		        	 }
    		        	 list(null, null, null, 1);
    		        }
    			}
    		}
    		
    		
    	}else{
    		flash.error("object.not_found");
            list(null, null, null, 1);
    		
    	}
    }

    public static void update(Formateur object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Artcs/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Formateur object = Formateur.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
