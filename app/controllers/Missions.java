package controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import models.Agent;

import models.Mission;
import models.Ressource;
import models.Ville;
import models._Agent_mission;
import models._ressources_mission;
import play.data.binding.As;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class Missions extends AppController {

	
	public static void delete(long id) {
        checkAuthenticity();
        Mission object = Mission.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	
        	Mission object = Mission.findById(id);
        	Agent agent = Agent.findById(object.getChef_mission());
            if (object != null) {
                render(object,agent);
              
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }
    
    
    
    public static void edits(long id) {
        if (id > 0) {
        	
        	Agent agent =Agent.findById(id);        	
            if (agent != null) {
                renderTemplate("Missions/edit.html", agent);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Mission.search(k, sF, sD, page);
        
        render(result, k, sF, sD, page);
    }
    
    public static void agent(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Agent.search(k, sF, sD, page);        
        render(result, k, sF, sD, page);
    }
    
 
    public static void update(Mission object , String date_de_depart, String date_de_retour) throws ParseException {
        checkAuthenticity();  
        System.out.println(new Gson().toJson(object));	 
   	// (1) get today's date
   	    Date today = Calendar.getInstance().getTime();
   	    // (2) create a date "formatter" (the date format we want)
   	     SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
   	    // (3) create a new String using the date format we want
   	    String daynow = formatter.format(today);
        if (object != null) 
        {
        	 if(comparedate(convertirdate(daynow),convertirdate(date_de_depart))) 
        	    {
        		     if (comparedate(convertirdate(date_de_depart),convertirdate(date_de_retour)))
        		         {
        			       object.setDate_de_depart(convertirdate(date_de_depart));
			               object.setDate_de_retour(convertirdate(date_de_retour));
        			      }
        		     else{
        			     flash.error("la date de debut de la mission doit être inferieur a sa date de fin");
        		          renderTemplate("Missions/edit.html", object);
        	             }
        		 }
        	    else{
                       flash.error("la date de debut de la mission  ne peut être inferieur à la date du jour");
     		            renderTemplate("Missions/edit.html", object);
                      }   	 
			      object.setStatus("Non"); 
			      object.setChef_mission(0);
			    if (object.validateAndSave()) 
			    {  
			    	flash.success("ENREGISTREE AVEC SUCCES");
			       list(null,null,null,1);
                }
			    else{
                       flash.error("Une errreur s'est produite lors de l'enregistrement");
	        		   renderTemplate("Missions/edit.html", object);
			        }
        
        }
        flash.error("Une errreur s'est produite lors de l'enregistrement");
		   renderTemplate("Missions/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Mission object = Mission.findById(id);
        	Agent agent =Agent.findById(object.getChef_mission());
            if (object != null) {
                render(object,agent);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }
    /**
     * Permet de charge les information de la mission a mettre a jour
     * @param id
     */
    
	public static void missionUpdate(long id) {
        if (id > 0) {
        	Mission object = Mission.findById(id);
        	Agent agent = Agent.findById(object.getChef_mission());    
     
        	
        	if (object != null) {
            	System.out.println(object); 	
            	System.out.println(agent); 
                renderTemplate("Missions/missionUpdate.html", object,agent);          
            }
        }
  
        flash.error("object.not_found");
        _Agent_missions.liste(null, null, null, 1);
    }
	/**
	 * permet de changer le status de la mission pour les ressources de mission
	 * @param object la mission
	 * @param status la valeur pour dire que la mission est cloturer 
	 * Changement du status a oui 
	 */
	
	public static void miseJour(Mission object , String status) {
        checkAuthenticity();
        List<_ressources_mission> ressources = _ressources_mission.find("byMission_id",object.getId()).fetch();
        
        if (object != null) 
        {
            System.out.println(" Status : "+status); 
        	if(object.getStatus().equalsIgnoreCase("Non") ){
        	
        		object.setStatus("Oui");
        		
            if (object.validateAndSave() ) {
            	Ressource maressource;
            	for (_ressources_mission res : ressources) {  
            		
            	     System.out.println(" Ressource : "+res.getRessource().getId());             	     
            	     maressource = Ressource.findById(res.getRessource().getId());
            	     maressource.setDisponibilite("Oui");
            	     maressource.validateAndSave();            	 
            	           	    
            	    }
            	 
                flash.success("MISSION ACHEVEE");
         
               _Agent_missions.liste(null, null, null, 1);
            }
        	}else{
        		 flash.error("MISSION DEJA ACHEVEE");
            	  _Agent_missions.liste(null, null, null, 1);
            
            	
            }
        
        }
        flash.error("object.updated.error");
        
    }
	/**
	 * La fonction permet de comparer les dates
	 * @param debut la date de debut
	 * @param retour la date de fin
	 * @return un boolean
	 */
	
public static boolean comparedate(Date debut,Date retour){
	
	boolean verif = false;
	
	 	if(debut.compareTo(retour)>0){
	    		System.out.println("Date debut : "+debut+" est supperieur date retour : "+retour);
	    		verif = false;
	    		
	    	}else if(debut.compareTo(retour)<0){
	    		System.out.println("Date debut : "+debut+" est inferieur Date retour : "+retour);
	    		verif = true;
	    	}else if(debut.compareTo(retour)==0){
        		System.out.println("Date depart : "+debut+" est identique Date retour : "+retour);
        		verif = true;
	    	}else
	    	{
	    		System.out.println("How to get here?");
	    	}
	    	
	
	System.out.println(verif);
	return verif;
}

/**
 * La fonction permet de convertir un string en date
 * @param day le string a convertir
 * @return la date
 */
public static  Date  convertirdate(String day){
	
	 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	 Date date = new Date();

	 try {
		date = formatter.parse(day);		        		
		System.out.println(formatter.format(date));
	    
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
	
		return date;
}
	

    
}