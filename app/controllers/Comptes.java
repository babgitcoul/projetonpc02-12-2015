package controllers;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import models.Agent;
import models.*;
import models._Agent_decoration;
import play.data.validation.Required;
import play.i18n.Messages;

public class Comptes  extends AppController{
	//Agent agent = null;
	  public static void login() {
	    	render();
	    }
	 
	  public static void dashbord() {
	    	render();
	    }
	  
	  public static void motdepasse() {
	    	render();
	    }
	  public static void reinitialisation() {
	    	render();
	    } 
	  
	  

      public static void edits(long id) {
    if (id > 0) {
       Agent  agent = Agent.findById(id);
        Compte object =  new Compte(); 
        if (agent != null) { 
     renderTemplate("Comptes/edit.html", agent,object);
        }
    }
    flash.error("object.not_found");
    list(null, null, null, 1);
}
	    public static void edit(Long id) {
	        if (id != null) {
	        	Compte object = Compte.findById(id);
	            Agent agent  = object.getAgent();
	            System.out.println(agent);
	            if (object != null) {	         	 
	                render(object,agent);
	            } else {
	                flash.error("object.not_found");
	                list(null, null, null, 1);
	            }
	        }
	        render();
	    }
	  
	  /*
       * enregistrement des droit d un utilisateur
       *  */
             public static void agentdroitSave(Compte object, Agent agent,String type_droit,String login,String password,String type_cpte ){
                 if (object != null) {
                 	object.setAgent(agent);
                 	object.setLogin(login);
                 	object.setMot_passe(password);
                 	object.setTypecpte(type_cpte);
                  object.setMatricule(agent.getMatricule());
                 	if (object.validateAndSave()){
                 		flash.success("compte.saved.success");
                 		 list(null,null,null,1);
                 	}
                 	else
                 	{
                 		flash.error("compte.saved.error");
             	          renderTemplate("Comptes/edit.html");
                 		
                 	}
                 	}
                 else{
                 	flash.error("compte.saved.error");
       	          renderTemplate("Agents/edit.html");
                 }
                 	
                 	}
	 
             public static Agent  agt;
             
             
	  public static void authenticate(@Required String login, @Required String mot_passe) {
	        checkAuthenticity();
	  
	       	if( !login.equalsIgnoreCase("") &&  !mot_passe.equalsIgnoreCase("") )
	       	{
	        Compte user = Compte.find("byLogin",login).first();
	        
	        if (user == null) {
	           	 
             // flash.error(Messages.get("auth.error"));
	            flash.error("Verifier vos parametres de connexion");
	            renderArgs.put("login", login);
	           // renderTemplate("Comptes/login.html");
	            Application.login();
	        
	        } else {
	        	agt = user.getAgent();
	        	System.out.println(user.getAgent().getId().toString());
	        	
	        	if(user.getTypecpte().equalsIgnoreCase("1")){
	        		
	        		 System.out.println("Vous etes un utlisateur");	
	        	     session.put("user",user.getAgent().getId());
	        	     session.put("agt",user.getAgent().getId());
	        	  	 renderTemplate("clientStat.html",agt);
	        	  	 
	        	}
	        	else if(user.getTypecpte().equalsIgnoreCase("2")){
	        		System.out.println("Vous etes un administrateur");
	        		System.out.println("Connecte en tant que :"+agt);
	        		
	        		if (session.isEmpty()){
	        			 session.put("admin",user.getAgent().getId());
	        		     session.put("agt",user.getAgent().getId());
	        		}else{
	        			session.remove("user");
	        			session.put("admin",user.getAgent().getId());
	        		     session.put("agt",user.getAgent().getId());
	        		}
	            flash.success("Bienvenue");
	//            response.setCookie("playlonglivecookie",user.getAgent().getId().toString(), "admin");
	             List<Agent> agents = Agent.findAll();
		         List<Agent> agentsM = Agent.find("sexe is ?","Masculin").fetch();
		         List<Agent> agentsF = Agent.find("sexe is ?","Feminin").fetch();
		         renderArgs.put("agents",agents.size());
		         renderArgs.put("agentsM",agentsM.size());
		         renderArgs.put("agentsF",agentsF.size());
         
         /*les demandes de congés*/
         
		         List<_Agent_conge> alldemde = _Agent_conge.findAll();
		         List<_Agent_conge> alldemdeAccd = _Agent_conge.find("status_conge is ?","2").fetch();
		         List<_Agent_conge> alldemdeRef = _Agent_conge.find("status_conge is ?","1").fetch();
		         List<_Agent_conge> alldemdeRep = _Agent_conge.find("status_conge is ?","0").fetch();
		         renderArgs.put("alldemde",alldemde.size());
		         renderArgs.put("alldemdeAccd",alldemdeAccd.size());
		         renderArgs.put("alldemdeRep",alldemdeRep.size());
		         /*les mutations et promotion*/
		         List<_Agent_poste> alldpost = _Agent_poste.findAll();
		         List<_Agent_poste> alldpostMut = _Agent_poste.find("type_affectation is ?","Mutation").fetch();
		         List<_Agent_poste> alldpostPro = _Agent_poste.find("type_affectation is ?","Promotion").fetch();
		         
		         renderArgs.put("alldpost",alldpost.size());
		         renderArgs.put("alldpostMut",alldpostMut.size());
		         renderArgs.put("alldpostPro",alldpostPro.size());
		         
		         /*les mutations et formation*/
		         List<_Agent_formation> allform = _Agent_formation.findAll();
		         List<_Agent_formation> allformvacp = _Agent_formation.find("status is ?","1").fetch();
		         List<_Agent_formation> allformvali = _Agent_formation.find("status is ?","2").fetch();
		         
		         renderArgs.put("allform",allform.size());
		         renderArgs.put("allformvacp",allformvacp.size());
		         renderArgs.put("allformvali",allformvali.size());
		         renderArgs.put("",agt);
	           	 renderTemplate("stat.html",agt);
	        	}
	        }
	        }
	        
	        else{
	        	System.out.println("Renseignez les champs");	
	        }
	    }
	  public static void view(long id) {
	        if (id > 0) {
	        	Compte object = Compte.findById(id);
	            if (object != null) {
	            	Agent agent = object.getAgent();
	                render(agent,object);
	            }
	        }
	        flash.error("object.not_found");
	        list(null, null, null, 1);
	    }
	  
      public static void recherche_agent_cpte(String k, String sF, String sD, int page) {
      HashMap<String, Object> result =Agent.search(k, sF, sD, page);
      render(result, k, sF, sD, page);

  }
  	public static void delete(long id) {
        checkAuthenticity();
        Compte object = Compte.findById(id);
        if (object != null) {
            try {
            	object.delete();
                flash.success("agent.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("agent_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }
      
	  
	  public static void list(String k, String sF, String sD, int page) {
	        HashMap<String, Object> result = Compte.search(k, sF, sD, page);
	        render(result, k, sF, sD, page);
	    }
      public static void passwordForget(String email1, String email2){

        if( !email1.equalsIgnoreCase("") &&  !email2.equalsIgnoreCase("") ){
          if(email1.equalsIgnoreCase(email2)){
            Agent agent =Agent.find("byEmail", email1).first();
            if(agent != null){
              Compte compte =  Compte.find("byAgent",agent).first();
            System.out.println(compte.getLogin());
            System.out.println(compte.getMot_passe());

            }else{flash.error("aucun compte attribué");
            renderTemplate("Comptes/motdepasse.html");
          }

          }else {flash.error("les deux mail sont differents");
        renderTemplate("Comptes/motdepasse.html");}
        
        }else{flash.error("les champs sont obligatoires");
        renderTemplate("Comptes/motdepasse.html");}

      }
}
