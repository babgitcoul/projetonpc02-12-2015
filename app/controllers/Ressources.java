package controllers;

import java.util.HashMap;

import models.Ressource;
import play.i18n.Messages;

import com.google.gson.Gson;

public class Ressources extends AppController {
	
	public static void delete(long id) {
        checkAuthenticity();
        Ressource object = Ressource.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Ressource object = Ressource.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Ressource.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Ressource object, String disponibilite) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        System.out.println(disponibilite);
        object.setDisponibilite("Oui");
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("ENREGISTREE AVEC SUCCES");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Ressources/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Ressource object = Ressource.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
