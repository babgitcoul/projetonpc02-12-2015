package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Contrat;
import models.Csu;
import models.Emploi;
import models.Grade;
import models.Poste;
import models.Sous_direction;
import models._Agent_conge;
import models._Agent_piece_conge;
import models._Agent_poste;
import models._Piece_agent;
import play.db.jpa.Blob;
import play.i18n.Messages;
import play.libs.MimeTypes;
import play.mvc.Before;
    




import com.google.gson.Gson;

public class _Piece_agents extends AppController {

    @Before
       public static void addViewParameters() {
   	 /*
		  * Chargement des listes deroulante:les select
		  * */

           List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
           renderArgs.put("allemplois", allemplois); 

           }
/*
 * enregistrement des pieces de l agent dans la table _Piece_agent_conge
 * */
           public static void agentpieceSave(_Piece_agent object,String numero_piece,String libelle_piece,String date_piece,String lieu_delivrance,String auteur,String date_fin_validite,String poste_auteur,Blob piece){
           	if (object != null){ 
           		Missions mission = new Missions();
           		/*
           		 * recuperation des libellés en fonction des id des select
           		 * */
           		if (session.contains("user")){
           			long idagent =Integer.parseInt( session.get("user"));
                    Agent agent = Agent.findById(idagent);
                    object.setAgent(agent);
                    
           		}
            object.setNumero_piece(numero_piece);
            object.setLibelle_piece(libelle_piece);
            object.setDate_delivrance(mission.convertirdate(date_piece));
            object.setAuteur(auteur);
            object.setLieu_delivrance(lieu_delivrance);
            object.setPoste_auteur(poste_auteur);
            object.setDate_fin_validite(mission.convertirdate(date_fin_validite));
            object.setPiece(piece);	
           
           	/*
           	 * avant l'enregistrement de on verifie si l object est vide ou pas
           	 * dans le cas ou il l est alors il s agit d une modifcation
           	 * sinon d 'un nouvel enregistrement
           	 * */
           	if (object.validateAndSave()){
           		flash.success("aposte.saved.success");
           		list(null, null, null, 1);
           	}
            }
        	flash.error("aposte.saved.error");
 	          renderTemplate("_Piece_agent_conges/edit.html",object);	
           
           	}
           /*
            *recuperation de la piece enregistrée en blob en
            *fonction de l'id
            * */
           public static void agentPiece(long id) {
        	   final _Piece_agent pieceAgentConge = _Piece_agent.findById(id);
        	   notFoundIfNull(pieceAgentConge);
        	   response.setContentTypeIfNotSet(pieceAgentConge.getPiece().type());
        	   renderBinary(pieceAgentConge.getPiece().get());
        	}
           
           public static void AjoutAvecFileName(File piece) throws FileNotFoundException {
        	   final _Piece_agent pieceConge = new _Piece_agent();
        	   pieceConge.setFilename(piece.getName());
        	   pieceConge.setPiece(new Blob());
        	 
        	   pieceConge.save();
        	   
        	}
           


   public static void recherche(String k, String sF, String sD, int page) {
       HashMap<String, Object> result =Agent.search(k, sF, sD, page);
       render(result, k, sF, sD, page);

   }
   
	public static void delete(long id) {
       checkAuthenticity();
       _Piece_agent object = _Piece_agent.findById(id);
       if (object != null) {
           try {
               object.delete();
               flash.success("object.deleted.success");
               list(null, null, null, 1);
           } catch (Exception e) {
               flash.error(Messages.get("object_has_children.error"));
               view(id);
           }
       } else {
           flash.error("object.deleted.error");
           list(null, null, null, 1);
       }
   }

   public static void edit(Long id) {
       if (id != null) {
    	   _Piece_agent object = _Piece_agent.findById(id);
           Agent agent  = object.getAgent();

           if (object != null) {
               render(object,agent);
           } else {
               flash.error("object.not_found");
               list(null, null, null, 1);
           }
       }
       render();
   }
/*
 * recuperation de l'id de la demande afin d'enregistrer les pieces  liées a cette demande
 * id_dmd declaré en public static pour pouvoir conserver la valeur partout dans la classe
 * */
public static long id_dmd;

       public static void edits(long id,String k, String sF, String sD, int page) {
       if (id > 0) {
        id_dmd = id;
           _Agent_conge object = _Agent_conge.findById(id);
           if (object != null) {  
        	   if (session.contains("user")){
                   long idagent =Integer.parseInt( session.get("user"));
                   Agent agent = Agent.findById(idagent);
        	      List<_Piece_agent> objects = _Piece_agent.find("agent is ? ", agent).fetch();
        	      renderArgs.put("objects", objects);
        	      renderArgs.put("object", object);
             render(id_dmd,null,null,1);     	     
           }else{
            HashMap<String, Object> result = _Piece_agent.search(k, sF, sD, page);
            render(result, k, sF, sD, page,object);
          }          
           }  
       }
       else{
        System.out.println(id_dmd);
        _Agent_conge object = _Agent_conge.findById(id_dmd);
       if (object != null) {
    	   if (session.contains("user")){
               long idagent =Integer.parseInt( session.get("user"));
               Agent agent = Agent.findById(idagent);
    	      List<_Piece_agent> objects = _Piece_agent.find("agent is ? ", agent).fetch();
    	      renderArgs.put("objects", objects);
    	      renderArgs.put("object", object);
    	      
    		   render ();
    	     
       }else{
         HashMap<String, Object> result = _Piece_agent.search(k, sF, sD, page);
       render(result, k, sF, sD, page,object);
          }
       }
       }
       
   }
       /*
        * fonction qui permet d 'enregistrer les pieces liées a la demande de congé d un agent
        * */
   public static void agentPieceCongeSave(long id){
	   /*
	    * recuperation de l'id de la piece*/
	   System.out.println(id);
	   System.out.println(id_dmd);
	   /*
	    * Recuperation de l'objet piece correspondant a id passé en paramettre 
	    * ainsi que la demande via l'id_dmd 
	    * */
	   _Piece_agent maPieceConge = _Piece_agent.findById(id);
	   _Agent_conge monConge  = _Agent_conge.findById(id_dmd);
	   /*
	    * creation d'une nouvelle ligne dans la table _Agent_piece_conge
	    *  */
	   _Agent_piece_conge AgentcongePiece = new _Agent_piece_conge();
	   AgentcongePiece.setAgent_demande(monConge);
	   AgentcongePiece.setAuteur(maPieceConge.getAuteur());
	   AgentcongePiece.setDate_debut(monConge.getDate_debut());
	   AgentcongePiece.setDate_delivrance(maPieceConge.getDate_delivrance());
	   AgentcongePiece.setDate_fin_validite(monConge.getDate_fin());
	   AgentcongePiece.setDate_fin_validite(maPieceConge.getDate_fin_validite());
	   AgentcongePiece.setLibelle_conge(monConge.getLibelle_conge());
	   AgentcongePiece.setLibelle_piece(maPieceConge.getLibelle_piece());
	   AgentcongePiece.setLieu_delivrance(maPieceConge.getLieu_delivrance());
	   AgentcongePiece.setMotif(monConge.getMotif());
	   AgentcongePiece.setNumero_piece(maPieceConge.getNumero_piece());
	   AgentcongePiece.setPiece(maPieceConge.getPiece());
	   AgentcongePiece.setPoste_auteur(maPieceConge.getPoste_auteur());
	   
	   
	  
	  if (AgentcongePiece.validateAndSave()){
		  flash.success("object.saved.success");
        edits(id_dmd,null,null,null,1);
      }else{
	  flash.error("object.not_found"); 
	  renderTemplate("_Agent_conges/list.html");
	  
	   
	  }   
   }

   public static void list(String k, String sF, String sD, int page) {
	   
	   if (session.contains("user")){
           long idagent =Integer.parseInt( session.get("user"));
           Agent agent = Agent.findById(idagent);
	      List<_Piece_agent> objects = _Piece_agent.find("agent is ? ", agent).fetch();
	      renderArgs.put("objects", objects);
	     // System.out.println(objects);
	     render();
	     
   }else{
       HashMap<String, Object> result = _Piece_agent.search(k, sF, sD, page);
       render(result, k, sF, sD, page);
   }
   }

   public static void update(_Agent_poste object) {
       checkAuthenticity();
       System.out.println(new Gson().toJson(object));
       if (object != null) {
           if (object.validateAndSave()) {
               flash.success("object.updated.success");
               list(null, null, null, 1);
           }
       }
       flash.error("object.updated.error");
       renderTemplate("_Piece_agent_conges/edit.html", object);
   }

   public static void view(long id) {
       if (id > 0) {
    	   _Piece_agent object = _Piece_agent.findById(id);
           if (object != null) {
        	   if(session.contains("user")){
        		   String type = "user";
        		   render(object,type);
        	   }else{
               render(object);
        	   }
           }
       }
       flash.error("object.not_found");
       list(null, null, null, 1);
   }

}

