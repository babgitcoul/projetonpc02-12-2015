/**
 * 
 */
package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Sanction;
import models.Status;
import models._Agent_statu;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

/**
 * @author TOSHIBA
 *
 */
public class _Agent_status extends AppController{

	 @Before
	    public static void addViewParameters() {

	        List<Status> allstatu = Status.find("order by libelle_status").fetch();
	        renderArgs.put("allstatu", allstatu); 
	        }
		
		
		
		public static void delete(long id) {
	        checkAuthenticity();
	        _Agent_statu object = _Agent_statu.findById(id);
	        if (object != null) {
	            try {
	                object.delete();
	                flash.success("object.deleted.success");
	                list(null, null, null, 1);
	            } catch (Exception e) {
	                flash.error(Messages.get("object_has_children.error"));
	                view(id);
	            }
	        } else {
	            flash.error("object.deleted.error");
	            list(null, null, null, 1);
	        }
	    }

	public static void agentstatusSave(_Agent_statu object, Agent agent,long statuId,String sDate,String agent_motif_status)throws ParseException{
		

		if (object != null) {
			Missions mission = new Missions();
	    	
	    	Status nomstatus = Status.findById(statuId);
	    	object.setAgent(agent);
	    	object.setLibelle_status(nomstatus.getLibelle_status());
	    	object.setStatus(nomstatus);
	    	object.setNom_agent(agent.getNom());
	    	object.setPrenom_agent(agent.getPrenom());
	    	
	    	object.setDate_status(mission.convertirdate(sDate));

	    	
	    	if (object.validateAndSave()){
	    		flash.success("ENREGISTRE AVEC SUCCES");
	    		list(null, null, null, 1);
	    	}
	    	else
	    	{
	    		flash.error("statu.saved.error");
		          renderTemplate("_Agent_status/edit.html");
	    		
	    	}
	    	}
	    else{
	    	flash.error("statu.saved.error");
	        renderTemplate("_Agent_status/edit.html");
	    }
	    	
	    	}
	    public static void edit(Long id) {
	        if (id != null) {
	        	_Agent_statu object = _Agent_statu.findById(id);
	            Agent agent  = object.getAgent();
	            if (object != null) {
	           	 /* creation d un format de date
	            	  * */
	          	     SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	          	    /*
	          	     * formatage des date recuperées depuis la db*/
	          	   String nDate = formatter.format(agent.getDate_naissance());
	                render(object,agent,nDate);
	            } else {
	                flash.error("object.not_found");
	                list(null, null, null, 1);
	            }
	        }
	        render();
	    }

	            public static void edits(long id) {
	        if (id > 0) {
	            Agent agent = Agent.findById(id);
	            _Agent_statu object = new _Agent_statu();
	            if (agent != null) {
	            	/* creation d un format de date
	            	  * */
	          	     SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	          	    /*
	          	     * formatage des date recuperées depuis la db*/
	          	   String nDate = formatter.format(agent.getDate_naissance());
	         renderTemplate("_Agent_status/edit.html", agent,object,nDate);
	            }
	        }
	        flash.error("agent.not_found");
	        list(null, null, null, 1);
	    }

	    public static void recherche_agent_status(String k, String sF, String sD, int page) {
        HashMap<String, Object> result =Agent.search(k, sF, sD, page);
        render(result, k, sF, sD, page);

    }

	    public static void list(String k, String sF, String sD, int page) {
	        HashMap<String, Object> result = _Agent_statu.search(k, sF, sD, page);
	        render(result, k, sF, sD, page);
	    }

	    public static void update(_Agent_statu object) {
	        checkAuthenticity();
	        System.out.println(new Gson().toJson(object));
	        if (object != null) {
	            if (object.validateAndSave()) {
	                flash.success("object.updated.success");
	                list(null, null, null, 1);
	            }
	        }
	        flash.error("object.updated.error");
	        renderTemplate("_Agent_status/edit.html", object);
	    }

	    public static void view(long id) {
	        if (id > 0) {
	        	_Agent_statu object = _Agent_statu.findById(id);
	            if (object != null) {
	                render(object);
	            }
	        }
	        flash.error("object.not_found");
	        list(null, null, null, 1);
	    }

}
