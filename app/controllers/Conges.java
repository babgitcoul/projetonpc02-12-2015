package controllers;

import java.util.HashMap;

import models.Conge;
import play.i18n.Messages;

import com.google.gson.Gson;

public class Conges  extends AppController{
	

	public static void menuConge(){
		render();
	}
	public static void retourConge(){
		render();
	}
	public static void planningConges(){
		render();
	}
	public static void detailConges(){
		render();
	}
	
	

	public static void delete(long id) {
        checkAuthenticity();
        Conge object = Conge.findById(id);
        if (object != null) {
            try {
                object.delete();
                flash.success("object.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("object_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }

    public static void edit(Long id) {
        if (id != null) {
        	Conge object = Conge.findById(id);
            if (object != null) {
                render(object);
            } else {
                flash.error("object.not_found");
                list(null, null, null, 1);
            }
        }
        render();
    }

    public static void list(String k, String sF, String sD, int page) {
        HashMap<String, Object> result = Conge.search(k, sF, sD, page);
        render(result, k, sF, sD, page);
    }

    public static void update(Conge object) {
        checkAuthenticity();
        System.out.println(new Gson().toJson(object));
        if (object != null) {
            if (object.validateAndSave()) {
                flash.success("object.updated.success");
                list(null, null, null, 1);
            }
        }
        flash.error("object.updated.error");
        renderTemplate("Conges/edit.html", object);
    }

    public static void view(long id) {
        if (id > 0) {
        	Conge object = Conge.findById(id);
            if (object != null) {
                render(object);
            }
        }
        flash.error("object.not_found");
        list(null, null, null, 1);
    }

}
