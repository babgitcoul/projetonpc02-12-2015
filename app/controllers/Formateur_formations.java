package controllers;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import models.Agent;
import models.Contrat;
import models.Csu;
import models.Decoration;
import models.Emploi;
import models.Formateur;
import models.Formation;
import models.Grade;
import models.Poste;
import models.Sous_direction;
import models.Formateur_formation;
import models._Agent_formation;
import play.i18n.Messages;
import play.mvc.Before;

import com.google.gson.Gson;

public class Formateur_formations extends AppController {

    @Before
       public static void addViewParameters() {
   	 /*
		  * Chargement des liste deroulante:les select
		  * */

           List<Emploi> allemplois = Emploi.find("order by libelle_emploi").fetch();
           renderArgs.put("allemplois", allemplois); 

           List<Contrat> allcontrat = Contrat.find("order by libelle_contrat").fetch();
           renderArgs.put("allcontrat", allcontrat);
           
           List<Csu> allCsus = Csu.find("order by libelle_csu").fetch();
           renderArgs.put("allCsus", allCsus);
           
           List<Sous_direction> allSousDirections = Sous_direction.find("order by libelle_sous_direction").fetch();
           renderArgs.put("allSousDirections", allSousDirections);
           
           List<Poste> allPostes  =  Poste.find("order by libelle_poste").fetch();
           renderArgs.put("allPostes", allPostes);

           List<Grade> allgrades = Grade.find("order by libelle_grade").fetch();
           renderArgs.put("allgrades", allgrades);
   
           List<Decoration> alldecorations = Decoration.find("order by libelle_decoration").fetch();
           renderArgs.put("alldecorations", alldecorations);
           }

    /*
    
             
	/*
	 * Suppression d'un agent*/
	public static void delete(long id) {
       checkAuthenticity();
       Formateur_formation object = Formateur_formation.findById(id);
       if (object != null) {
           try {
               object.delete();
               flash.success("object.deleted.success");
               list(null, null, null, 1);
           } catch (Exception e) {
               flash.error(Messages.get("object_has_children.error"));
               view(id);
           }
       } else {
           flash.error("object.deleted.error");
           list(null, null, null, 1);
       }
   }
	

   public static void edit(Long id) {
       if (id != null) {
       	Formateur_formation object = Formateur_formation.findById(id);
         //  Agent agent  = object.getAgent();
          // System.out.println(agent);
           if (object != null) {
           	/* creation d un format de date
          	  * */
        	     SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        	    /*
        	     * formatage des date recuperées depuis la db*/
        	 
               render(object);
           } else {
               flash.error("object.not_found");
               list(null, null, null, 1);
           }
       }
       render();
   }
           public static void edits(long id) {
       if (id > 0) {
           Agent agent = Agent.findById(id);
           Formateur_formation object =  new Formateur_formation();
           if (agent != null) {
           
        renderTemplate("Formateur_formations/edit.html", agent,object);
           }
       }
       flash.error("object.not_found");
       list(null, null, null, 1);
   }
   
    /*
     * fonction de recherche de l agent qui sera decoré par la suite 
     * */


   public static void list(String k, String sF, String sD, int page) {
       	  HashMap<String, Object> result = Formateur_formation.search(k, sF, sD, page);	
       	  render(result, k, sF, sD, page);
       	  
         }
        
     
   

   public static void update(Formateur_formation object) {
       checkAuthenticity();
       System.out.println(new Gson().toJson(object));
       if (object != null) {
           if (object.validateAndSave()) {
               flash.success("object.updated.success");
               list(null, null, null, 1);
           }
       }
       flash.error("object.updated.error");
       renderTemplate("Formateur_formations/edit.html", object);
   }

   public static void view(long id) {
       if (id > 0) {
       	Formateur_formation object = Formateur_formation.findById(id);
           if (object != null) {
               render(object);
           }
       }
       flash.error("object.not_found");
       list(null, null, null, 1);
   }
   

}

