package models;

//import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_contrat extends BaseModel {
	
	
	private Date  date_enregistrement;
	
	
	private Date  date_debut;
	

	private Date   date_fin;
	
	@Type(type = "org.hibernate.type.TextType")
	private String theme;
	
	@Type(type = "org.hibernate.type.TextType")
	private String initiateur;
	
	@ManyToOne
	private Departement departement;
	
	@Type(type = "org.hibernate.type.TextType")
	private String lib_ss_direction;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_contrat;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Sous_direction sous_direction;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_departements;
	
	@ManyToOne
	private Contrat contrat;
	
	@ManyToOne
	private Agent agent;
	
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_enregistrement","date_debut","date_fin","theme","initiateur","departement","lib_ss_direction",
 "libelle_contrat","nom_agent","prenom_agent","matricule_agent","sous_direction","departements","contrat,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_contrat.sortDirections == null) {
            _Agent_contrat.sortDirections = Arrays.asList(_Agent_contrat.SORT_DIRECTIONS);
        }
        return _Agent_contrat.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_contrat.sortFields == null) {
            _Agent_contrat.sortFields = Arrays.asList(_Agent_contrat.SORT_FIELDS);
        }
        return _Agent_contrat.sortFields;
    }

	public Date getDate_enregistrement() {
		return date_enregistrement;
	}
	public void setDate_enregistrement(Date date_enregistrement) {
		this.date_enregistrement = date_enregistrement;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getInitiateur() {
		return initiateur;
	}
	public void setInitiateur(String initiateur) {
		this.initiateur = initiateur;
	}
	
	public String getLib_ss_direction() {
		return lib_ss_direction;
	}
	public void setLib_ss_direction(String lib_ss_direction) {
		this.lib_ss_direction = lib_ss_direction;
	}
	public String getLibelle_contrat() {
		return libelle_contrat;
	}
	public void setLibelle_contrat(String libelle_contrat) {
		this.libelle_contrat = libelle_contrat;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Sous_direction getSous_direction() {
		return sous_direction;
	}

	public void setSous_direction(Sous_direction sous_direction) {
		this.sous_direction = sous_direction;
	}

	public String getLibelle_departements() {
		return libelle_departements;
	}

	public void setLibelle_departements(String libelle_departements) {
		this.libelle_departements = libelle_departements;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_contrat(Date date_enregistrement, Date date_debut,
			Date date_fin, String theme, String initiateur,
			Departement departement, String lib_ss_direction,
			String libelle_contrat, String nom_agent, String prenom_agent,
			String matricule_agent, Sous_direction sous_direction,
			String libelle_departements, Contrat contrat, Agent agent) {
		super();
		this.date_enregistrement = date_enregistrement;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.theme = theme;
		this.initiateur = initiateur;
		this.departement = departement;
		this.lib_ss_direction = lib_ss_direction;
		this.libelle_contrat = libelle_contrat;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.sous_direction = sous_direction;
		this.libelle_departements = libelle_departements;
		this.contrat = contrat;
		this.agent = agent;
	}

	public _Agent_contrat() {
		super();
	}

	
	

}
