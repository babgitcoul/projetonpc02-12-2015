package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.deadbolt.Role;
import models.deadbolt.RoleHolder;
import models.enums.UserRoleType;

import org.mindrot.jbcrypt.BCrypt;

import play.Play;
import play.data.binding.NoBinding;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.GenericModel.JPAQuery;
import play.libs.Crypto;

@Entity(name="user_")
public class User extends BaseModel implements RoleHolder {

    @Required
    @Unique
    public String username;

    @Required
    public int roleCode;

    @NoBinding
    @Required
    private String passwordHash;


    public void setPassword(String password) throws IllegalArgumentException {
        if ((password == null) || (password.length() < 8)) {
            throw new IllegalArgumentException(
                    "password length has to be greater than 8 characters.");
        } else {
            this.passwordHash = User.doStrongPasswordHash(password);
        }
    }

    @Override
    public String toString() {
        return this.username;
    }

    final public static String SESSION_KEY = "username";

    final public static String VIEW_VARIABLE_NAME = "authUser";

    public static User authenticate(String username, String password) {
        if ((username == null) || (password == null)) {
            throw new IllegalArgumentException();
        }
        User employee = User.find("username is ?", username.toLowerCase()).first();
        if (employee != null) {
            if (BCrypt.checkpw(User.doWeakPasswordHash(password), employee.passwordHash)) {
                return employee;
            }
        }
        return null;
    }

    private static String doStrongPasswordHash(String password) {
        return BCrypt.hashpw(User.doWeakPasswordHash(password), BCrypt.gensalt());
    }

    private static String doWeakPasswordHash(String password) {
        return Crypto.passwordHash(password + Play.configuration.getProperty("application.secret"), Crypto.HashType.SHA512);
    }

    public static User findByUsername(String username)
            throws IllegalArgumentException {
        if (username == null) {
            throw new IllegalArgumentException("username parameter cannot be null.");
        } else {
            return User.find("username is ?", username.toLowerCase()).first();
        }
    }

    public UserRoleType getRoleType() {
        return UserRoleType.parseCode(this.roleCode);
    }
 
    @Override
    public List<UserRole> getRoles() {
        List<UserRole> roles = new ArrayList<UserRole>();
        roles.add(new UserRole(this.roleCode));
        return roles;
    }

    final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

    final private static String[] SORT_FIELDS =
    {
            "created",
            "id",
            "username",
            "roleCode",
            "updated"
    };

    private static List<String> sortDirections;

    private static List<String> sortFields;

    public static List<String> getSortDirections() {
        if (User.sortDirections == null) {
            User.sortDirections = Arrays.asList(User.SORT_DIRECTIONS);
        }
        return User.sortDirections;
    }

    public static List<String> getSortFields() {
        if (User.sortFields == null) {
            User.sortFields = Arrays.asList(User.SORT_FIELDS);
        }
        return User.sortFields;
    }

    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if (sortField == null || sortField.length() < 1) {
            sortField = "username";
        }

        if (sortDirection == null || sortDirection.length() < 1) {
            sortDirection = "asc";
        }

        if (!User.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!User.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<User> objects = new ArrayList<User>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and username is ?");
            paramList.add(keyword);
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }
    
}
