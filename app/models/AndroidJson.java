package models;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import play.db.jpa.GenericModel.JPAQuery;

@Entity

public class AndroidJson extends BaseModel {
	
	@JsonProperty
	public long total_onpc;
	@JsonProperty
	public long total_mas;
	@JsonProperty
	public long total_fem;
	@JsonProperty
	public long total_depart;
	@JsonProperty
	public long total_dispo;
	
 //pour la repartition par csu
	@JsonProperty
	public String libelle_csu;
	@JsonProperty
	public long total_csu;
	@JsonProperty
	public long total_csu_mas;
	@JsonProperty
	public long total_csu_fem;
	
	@JsonProperty
	public long csu_total;
	
    public long getTotal_onpc() {
		return total_onpc;
	}

	public void setTotal_onpc(long total_onpc) {
		this.total_onpc = total_onpc;
	}

	public long getTotal_mas() {
		return total_mas;
	}

	public void setTotal_mas(long total_mas) {
		this.total_mas = total_mas;
	}

	public long getTotal_fem() {
		return total_fem;
	}

	public void setTotal_fem(long total_fem) {
		this.total_fem = total_fem;
	}

	public long getTotal_depart() {
		return total_depart;
	}

	public void setTotal_depart(long total_depart) {
		this.total_depart = total_depart;
	}

	public long getTotal_dispo() {
		return total_dispo;
	}

	public void setTotal_dispo(long total_dispo) {
		this.total_dispo = total_dispo;
	}

	public String getLibelle_csu() {
		return libelle_csu;
	}

	public void setLibelle_csu(String libelle_csu) {
		this.libelle_csu = libelle_csu;
	}

	public long getTotal_csu() {
		return total_csu;
	}

	public void setTotal_csu(long total_csu) {
		this.total_csu = total_csu;
	}

	public long getTotal_csu_mas() {
		return total_csu_mas;
	}

	public void setTotal_csu_mas(long total_csu_mas) {
		this.total_csu_mas = total_csu_mas;
	}

	public long getTotal_csu_fem() {
		return total_csu_fem;
	}

	public void setTotal_csu_fem(long total_csu_fem) {
		this.total_csu_fem = total_csu_fem;
	}
	
	

	public long getCsu_total() {
		return csu_total;
	}

	public void setCsu_total(long csu_total) {
		this.csu_total = csu_total;
	}



	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "libelle_csu" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	   
    public static List<String> getSortDirections() {
        if (AndroidJson.sortDirections == null) {
            AndroidJson.sortDirections = Arrays.asList(AndroidJson.SORT_DIRECTIONS);
        }
        return AndroidJson.sortDirections;
    }

    public static List<String> getSortFields() {
        if (AndroidJson.sortFields == null) {
            AndroidJson.sortFields = Arrays.asList(AndroidJson.SORT_FIELDS);
        }
        return AndroidJson.sortFields;
    }
	
    

	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_csu";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!AndroidJson.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!AndroidJson.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<AndroidJson> objects = new ArrayList<AndroidJson>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_csu) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public AndroidJson() {
		
		
	}
	public static List<Pie> plotByType(){
    	HashMap<String, Integer> map = new HashMap<String, Integer>();
    	List<AndroidJson> sites = new ArrayList<AndroidJson>();
    	sites = AndroidJson.findAll();
    	for(AndroidJson site : sites){
    		if(map.containsKey(site.getLibelle_csu())){
    			map.put(site.getLibelle_csu(), map.get(site.getLibelle_csu())+1);
    		}else{
    			map.put(site.getLibelle_csu(), 1);
    		}
    	} 
    	Set<String> sets = map.keySet();
    	List<Pie> pies = new ArrayList<Pie>();
    	for(String set : sets){
    		pies.add(new Pie(set, map.get(set)));
    	}
    	return pies;
    	
    }
	

}
