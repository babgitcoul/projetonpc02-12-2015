package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;
@Entity
public class Contrat extends BaseModel {
	
	//private long id_contrat;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_contrat;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
	final private static String[] SORT_FIELDS = {  "libelle_contrat" };
		
	private static List<String> sortDirections;

	private static List<String> sortFields;
		
		    
		    
    public static List<String> getSortDirections() {
        if (Contrat.sortDirections == null) {
            Contrat.sortDirections = Arrays.asList(Contrat.SORT_DIRECTIONS);
        }
        return Contrat.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Contrat.sortFields == null) {
            Contrat.sortFields = Arrays.asList(Contrat.SORT_FIELDS);
        }
        return Contrat.sortFields;
    }

	public String getLibelle_contrat() {
		return libelle_contrat;
	}

	public void setLibelle_contrat(String libelle_contrat) {
		this.libelle_contrat = libelle_contrat;
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_contrat";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Contrat.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Contrat.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Contrat> objects = new ArrayList<Contrat>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_contrat) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Contrat(String libelle_contrat) {
		super();
		this.libelle_contrat = libelle_contrat;
	}
	
}
