package models;

//import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_echelon  extends BaseModel{
	

	private Date   date_echelon;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_echelon;

	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Echelon echelon;
	
	@ManyToOne
	private Agent agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_echelon","libelle_echelon","nom_agent","prenom_agent","matricule_agent","echelon,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_echelon.sortDirections == null) {
            _Agent_echelon.sortDirections = Arrays.asList(_Agent_echelon.SORT_DIRECTIONS);
        }
        return _Agent_echelon.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_echelon.sortFields == null) {
            _Agent_echelon.sortFields = Arrays.asList(_Agent_echelon.SORT_FIELDS);
        }
        return _Agent_echelon.sortFields;
    }

	public Date getDate_echelon() {
		return date_echelon;
	}
	public void setDate_echelon(Date date_echelon) {
		this.date_echelon = date_echelon;
	}
	public String getLibelle_echelon() {
		return libelle_echelon;
	}
	public void setLibelle_echelon(String libelle_echelon) {
		this.libelle_echelon = libelle_echelon;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}

	public Echelon getEchelon() {
		return echelon;
	}

	public void setEchelon(Echelon echelon) {
		this.echelon = echelon;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_echelon(Date date_echelon, String libelle_echelon,
			String nom_agent, String prenom_agent, String matricule_agent,
			Echelon echelon, Agent agent) {
		super();
		this.date_echelon = date_echelon;
		this.libelle_echelon = libelle_echelon;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.echelon = echelon;
		this.agent = agent;
	}

	public _Agent_echelon() {
		super();
	}

	
	

}
