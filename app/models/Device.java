package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import play.Logger;
import play.data.validation.Required;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Device extends BaseModel {

	final private static String[] _PAGINATE_DIRECTIONS = { "desc", "asc" };

    final private static String[] _PAGINATE_FIELDS = { "created", "registrationId", "imei", "updated","type" };

    private static List<String> paginateDirections;

    private static List<String> paginateFields;

    public static List<String> getPaginateDirections() {
        if (Device.paginateDirections == null) {
            Device.paginateDirections = Arrays.asList(Device._PAGINATE_DIRECTIONS);
        }
        return Device.paginateDirections;
    }

    public static List<String> getPaginateFields() {
        if (Device.paginateFields == null) {
            Device.paginateFields = Arrays.asList(Device._PAGINATE_FIELDS);
        }
        return Device.paginateFields;
    }

    public static HashMap<String, Object> search(String keyword, String paginateField, String paginateDirection, int page) {

        if ((paginateField == null) || (paginateField.length() < 1)) {
            paginateField = _PAGINATE_FIELDS[0];
        }

        if ((paginateDirection == null) || (paginateDirection.length() < 1)) {
            paginateDirection = _PAGINATE_DIRECTIONS[0];
        }

        if (!getPaginateFields().contains(paginateField)) {
            String msg = String.format("paginateField parameter '%s' is not valid.", paginateDirection);
            Logger.error(msg, paginateField);
            throw new IllegalArgumentException(msg);
        }

        if (!getPaginateDirections().contains(paginateDirection)) {
            String msg = String.format("paginateDirection parameter '%s' is not valid.", paginateDirection);
            Logger.error(msg);
            throw new IllegalArgumentException(msg);
        }

        List<Device> objects = new ArrayList<Device>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder queryStringBuilder = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        queryStringBuilder.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            queryStringBuilder.append(" and (imei like ? or brand like ? or model like ? or os like ?)");
            paramList.add("%" + keyword + "%");
            paramList.add("%" + keyword + "%");
            paramList.add("%" + keyword + "%");
            paramList.add("%" + keyword + "%");
        }
        // Start other query criteria

        // End other query criteria

        final Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(queryStringBuilder.toString(), params);

        queryStringBuilder.append(" order by " + paginateField + " " + paginateDirection);
        final JPAQuery query = Device.find(queryStringBuilder.toString(), params);
        if (page < 1) {
            page = 1;
        }

        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

    @JsonProperty
    @Required
    private String imei;
    

    @JsonProperty
    @Required
    private String type;

    @JsonProperty
    private String registrationId;

    public Device() {
    }

    public Device(String imei) {
        this.imei = imei;
    }
   
    public String getImei() {
        return imei;
    }

    public String getType() {
        return type;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }


    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return registrationId;
    }

}
