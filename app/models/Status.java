package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

@Entity
public class Status extends BaseModel {
	
	//private long id_status;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_status;
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_status" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Status.sortDirections == null) {
	            Status.sortDirections = Arrays.asList(Status.SORT_DIRECTIONS);
	        }
	        return Status.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Status.sortFields == null) {
	            Status.sortFields = Arrays.asList(Status.SORT_FIELDS);
	        }
	        return Status.sortFields;
	    }
	    

	public  String getLibelle_status() {
		return libelle_status;
	}

	public  void setLibelle_status(String libelle_status) {
		this.libelle_status = libelle_status;
	}
	
	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_status";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Status.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Status.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Status> objects = new ArrayList<Status>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_status) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	      
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Status(String libelle_status) {
		super();
		this.libelle_status = libelle_status;
	}


}
