package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel.JPAQuery;

@Entity

public class _Agent_formation extends BaseModel {
	

	private Date   date_enregistrement;
	private String lieu_formation;
	private Date   date_debut;
	private Date   date_fin;
	private String status;
	private String numero_pv;
	private String numero_attestation;
	private String nom_agent;
	private String prenom_agent;
	private String matricule_agent;
	private String libelle_formation;
	private String diplome;
	private String carte;
	private String formateur_nom;
	
	public String getFormateur_nom() {
		return formateur_nom;
	}

	public void setFormateur_nom(String formateur_nom) {
		this.formateur_nom = formateur_nom;
	}

	@ManyToOne
	private Formation formation;
	@ManyToOne
	private Agent agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_enregistrement","lieu_formation","date_debut","date_fin","status",
		"numero_pv","numero_attestation","nom_agent","prenom_agent","matricule_agent","formateur_nom","formation","agent","libelle_formation","diplome","carte" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_formation.sortDirections == null) {
            _Agent_formation.sortDirections = Arrays.asList(_Agent_formation.SORT_DIRECTIONS);
        }
        return _Agent_formation.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_formation.sortFields == null) {
            _Agent_formation.sortFields = Arrays.asList(_Agent_formation.SORT_FIELDS);
        }
        return _Agent_formation.sortFields;
    }

	public Date getDate_enregistrement() {
		return date_enregistrement;
	}
	public void setDate_enregistrement(Date date_enregistrement) {
		this.date_enregistrement = date_enregistrement;
	}
	public String getLieu_formation() {
		return lieu_formation;
	}
	public void setLieu_formation(String lieu_formation) {
		this.lieu_formation = lieu_formation;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNumero_pv() {
		return numero_pv;
	}
	public void setNumero_pv(String numero_pv) {
		this.numero_pv = numero_pv;
	}
	public String getNumero_attestation() {
		return numero_attestation;
	}
	public void setNumero_attestation(String numero_attestation) {
		this.numero_attestation = numero_attestation;
	}
	
	public String getNom_agent() {
		return nom_agent;
	}

	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}

	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	
	public String getLibelle_formation() {
		return libelle_formation;
	}

	public void setLibelle_formation(String libelle_formation) {
		this.libelle_formation = libelle_formation;
	}

	public String getDiplome() {
		return diplome;
	}

	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}

	public String getCarte() {
		return carte;
	}

	public void setCarte(String carte) {
		this.carte = carte;
	}

	public _Agent_formation(Date date_enregistrement, String lieu_formation,
			Date date_debut, Date date_fin, String status, String numero_pv,
			String numero_attestation, String nom_agent, String prenom_agent,
			String matricule_agent, Formation formation, Agent agent) {
		super();
		this.date_enregistrement = date_enregistrement;
		this.lieu_formation = lieu_formation;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.status = status;
		this.numero_pv = numero_pv;
		this.numero_attestation = numero_attestation;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.formation = formation;
		this.agent = agent;
	}
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_formation";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_formation.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_formation.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_formation> objects = new ArrayList<_Agent_formation>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom_agent) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public _Agent_formation() {
		super();
	}	
	
	
	
	

}
