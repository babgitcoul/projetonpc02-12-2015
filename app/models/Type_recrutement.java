package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
 @Entity
public class Type_recrutement  extends BaseModel{
	
//	private long id_type_agent;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_type_recrutement;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_type_recrutement" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Type_recrutement.sortDirections == null) {
	            Type_recrutement.sortDirections = Arrays.asList(Type_recrutement.SORT_DIRECTIONS);
	        }
	        return Type_recrutement.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Type_recrutement.sortFields == null) {
	            Type_recrutement.sortFields = Arrays.asList(Type_recrutement.SORT_FIELDS);
	        }
	        return Type_recrutement.sortFields;
	    }

		public String getLibelle_type_recrutement() {
			return libelle_type_recrutement;
		}

		public void setLibelle_type_recrutement(String libelle_type_recrutement) {
			this.libelle_type_recrutement = libelle_type_recrutement;
		}
	    
		 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

		        if ((sortField == null) || (sortField.length() < 1)) {
		            sortField = "libelle_type_recrutement";
		        }

		        if ((sortDirection == null) || (sortDirection.length() < 1)) {
		            sortDirection = "desc";
		        }

		        if (!Type_recrutement.getSortFields().contains(sortField)) {
		            throw new IllegalArgumentException("sortField is not valid.");
		        }

		        if (!Type_recrutement.getSortDirections().contains(sortDirection)) {
		            throw new IllegalArgumentException("sortDirection is not valid");
		        }

		        List<Type_recrutement> objects = new ArrayList<Type_recrutement>();
		        List<Object> paramList = new ArrayList<Object>();
		        StringBuilder sb = new StringBuilder();
		        HashMap<String, Object> result = new HashMap();
		        Long count;

		        sb.append("id > 0");

		        if ((keyword != null) && (keyword.trim().length() > 0)) {
		            sb.append(" and (LOWER(libelle_type_recrutement) like ? ) ");
		            paramList.add("%" + keyword.toLowerCase() + "%");
		       
		        }

		        Object[] params = paramList.toArray(new Object[paramList.size()]);
		        count = count(sb.toString(), params);

		        sb.append(" order by " + sortField + " " + sortDirection);
		        JPAQuery query = find(sb.toString(), params);
		        if (page < 1) {
		            page = 1;
		        }
		        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

		        result.put("objects", objects);
		        result.put("count", count);
		        return result;
		    }

		public Type_recrutement(String libelle_type_recrutement) {
			super();
			this.libelle_type_recrutement = libelle_type_recrutement;
		}




}
