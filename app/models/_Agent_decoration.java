package models;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class _Agent_decoration  extends BaseModel{
	
	private Date   date_decoration;
	
	@Type(type = "org.hibernate.type.TextType")
	private String decorateur;
	
	@Type(type = "org.hibernate.type.TextType")
	private String motif_decoration;
	
	public String getMotif_decoration() {
		return motif_decoration;
	}

	public void setMotif_decoration(String motif_decoration) {
		this.motif_decoration = motif_decoration;
	}

	@Type(type = "org.hibernate.type.TextType")
	private String lieu_decoration;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_decoration;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Decoration decoration;
	
	@ManyToOne
	private Agent agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_decoration","decorateur","lieu_decoration","nom_agent","prenom_agent"
		, "matricule_agent","decoration","agent","libelle_decoration","motif_decoration"};
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_decoration.sortDirections == null) {
            _Agent_decoration.sortDirections = Arrays.asList(_Agent_decoration.SORT_DIRECTIONS);
        }
        return _Agent_decoration.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_decoration.sortFields == null) {
            _Agent_decoration.sortFields = Arrays.asList(_Agent_decoration.SORT_FIELDS);
        }
        return _Agent_decoration.sortFields;
    }

	public Date getDate_decoration() {
		return date_decoration;
	}
	public void setDate_decoration(Date date_decoration) {
		this.date_decoration = date_decoration;
	}
	public String getDecorateur() {
		return decorateur;
	}
	public void setDecorateur(String decorateur) {
		this.decorateur = decorateur;
	}
	public String getLieu_decoration() {
		return lieu_decoration;
	}
	public void setLieu_decoration(String lieu_decoration) {
		this.lieu_decoration = lieu_decoration;
	}
	public String getLibelle_decoration() {
		return libelle_decoration;
	}
	public void setLibelle_decoration(String libelle_decoration) {
		this.libelle_decoration = libelle_decoration;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	public Decoration getDecoration() {
		return decoration;
	}
	public void setDecoration(Decoration decoration) {
		this.decoration = decoration;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "nom_agent";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        

        if (!_Agent_decoration.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_decoration> objects = new ArrayList<_Agent_decoration>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom_agent) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public _Agent_decoration() {
		super();
	}
}
