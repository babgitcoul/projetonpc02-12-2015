package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity 
public class Poste extends BaseModel {
	
//	private long id_poste;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_poste;

	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_poste" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Poste.sortDirections == null) {
	            Poste.sortDirections = Arrays.asList(Poste.SORT_DIRECTIONS);
	        }
	        return Poste.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Poste.sortFields == null) {
	            Poste.sortFields = Arrays.asList(Poste.SORT_FIELDS);
	        }
	        return Poste.sortFields;
	    }
	    
	public String getLibelle_poste() {
		return libelle_poste;
	}

	public void setLibelle_poste(String libelle_poste) {
		this.libelle_poste = libelle_poste;
	}

	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_poste";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Poste.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Poste.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Poste> objects = new ArrayList<Poste>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_poste) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
         ;
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Poste(String libelle_poste) {
		super();
		this.libelle_poste = libelle_poste;
	}
}
