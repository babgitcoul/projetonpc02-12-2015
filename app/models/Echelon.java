package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Echelon extends BaseModel {
	
	//private long id_echelon;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_echelon;
	  
	@Type(type = "org.hibernate.type.TextType")
	private String decription_echelon;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_echelon","decription_echelon" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Echelon.sortDirections == null) {
	            Echelon.sortDirections = Arrays.asList(Echelon.SORT_DIRECTIONS);
	        }
	        return Echelon.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Echelon.sortFields == null) {
	            Echelon.sortFields = Arrays.asList(Echelon.SORT_FIELDS);
	        }
	        return Echelon.sortFields;
	    }

	public  String getLibelle_echelon() {
		return libelle_echelon;
	}

	public  void setLibelle_echelon(String libelle_echelon) {
		this.libelle_echelon = libelle_echelon;
	}

	public  String getDecription_echelon() {
		return decription_echelon;
	}

	public  void setDecription_echelon(String decription_echelon) {
		this.decription_echelon = decription_echelon;
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_echelon";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Echelon.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Echelon.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Echelon> objects = new ArrayList<Echelon>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_echelon) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Echelon(String libelle_echelon, String decription_echelon) {
		super();
		this.libelle_echelon = libelle_echelon;
		this.decription_echelon = decription_echelon;
	}
	
}
