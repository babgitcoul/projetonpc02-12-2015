package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;
@Entity
public class Sanction  extends BaseModel{
	
	//private long id_sanction;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sanction;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_sanction" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Sanction.sortDirections == null) {
	            Sanction.sortDirections = Arrays.asList(Sanction.SORT_DIRECTIONS);
	        }
	        return Sanction.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Sanction.sortFields == null) {
	            Sanction.sortFields = Arrays.asList(Sanction.SORT_FIELDS);
	        }
	        return Sanction.sortFields;
	    }
	    

	public  String getLibelle_sanction() {
		return libelle_sanction;
	}

	public  void setLibelle_sanction(String libelle_sanction) {
		this.libelle_sanction = libelle_sanction;
	}

	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_sanction";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Sanction.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Sanction.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Sanction> objects = new ArrayList<Sanction>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_sanction) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	           
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Sanction(String libelle_sanction) {
		super();
		this.libelle_sanction = libelle_sanction;
	}

}
