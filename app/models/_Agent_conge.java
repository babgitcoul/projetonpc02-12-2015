package models;
      

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class _Agent_conge extends BaseModel{
	@ManyToOne
	private Agent agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	private long sId;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sous_direction;
	
	@Type(type = "org.hibernate.type.TextType")
  private String libelle_csu;

  @Type(type = "org.hibernate.type.TextType")
	private String nombreJours;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_poste_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String status_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_conge;
	
	@Type(type = "org.hibernate.type.TextType")
	private String motif;
	
    private Date date_debut;
	
	private Date date_fin;
	
	@Type(type = "org.hibernate.type.TextType")
	private String status_conge;

	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_direction;
	
	@Type(type = "org.hibernate.type.TextType")
	private String numero_demande;
	
    @ManyToOne
	private Direction direction;
	
	@ManyToOne
	private Conge conge;
	
	@ManyToOne
	private Sous_direction sous_direction;
	
	@ManyToOne
	private Csu csu;
	
    public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getLibelle_sous_direction() {
    return libelle_sous_direction;
  }
  public String getNombreJours() {
		return nombreJours;
	}

  public void setNombreJours(String nombreJours) {
    this.nombreJours = nombreJours;

  }	public void setLibelle_sous_direction(String libelle_sous_direction) {
		this.libelle_sous_direction = libelle_sous_direction;
	}

	public String getLibelle_csu() {
		return libelle_csu;
	}

	public void setLibelle_csu(String libelle_csu) {
		this.libelle_csu = libelle_csu;
	}

	public String getLibelle_poste_agent() {
		return libelle_poste_agent;
	}

	public void setLibelle_poste_agent(String libelle_poste_agent) {
		this.libelle_poste_agent = libelle_poste_agent;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Conge getConge() {
		return conge;
	}

	public void setConge(Conge conge) {
		this.conge = conge;
	}

	public Sous_direction getSous_direction() {
		return sous_direction;
	}

	public void setSous_direction(Sous_direction sous_direction) {
		this.sous_direction = sous_direction;
	}

	public Csu getCsu() {
		return csu;
	}

	public void setCsu(Csu csu) {
		this.csu = csu;
	}
	public String getMotif() {
		return motif;
	}
	public void setMotif(String motif) {
		this.motif = motif;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getStatus_agent() {
		return status_agent;
	}
	public void setStatus_agent(String status_agent) {
		this.status_agent = status_agent;
	}
	public String getStatus_conge() {
		return status_conge;
	}
	public void setStatus_conge(String status_conge) {
		this.status_conge = status_conge;
	}
	
	public long getsId() {
		return sId;
	}

	public void setsId(long sId) {
		this.sId = sId;
	}

	public String getLibelle_direction() {
		return libelle_direction;
	}
	public void setLibelle_direction(String libelle_direction) {
		this.libelle_direction = libelle_direction;
	}
	public String getNumero_demande() {
		return numero_demande;
	}
	public void setNumero_demande(String numero_demande) {
		this.numero_demande = numero_demande;
	}
	public String getLibelle_conge() {
		return libelle_conge;
	}
	public void setLibelle_conge(String libelle_conge) {
		this.libelle_conge = libelle_conge;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "agent","conge","direction","status_agent","sous_direction",
		"matricule_agent","prenom_agent","nom_agent","libelle_conge","numero_demande","libelle_direction",
		"libelle_poste_agent","status_conge","date_debut","date_fin","motif","libelle_sous_direction" ,"libelle_csu","sId","nombreJours"};
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_conge.sortDirections == null) {
            _Agent_conge.sortDirections = Arrays.asList(_Agent_conge.SORT_DIRECTIONS);
        }
        return _Agent_conge.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_conge.sortFields == null) {
            _Agent_conge.sortFields = Arrays.asList(_Agent_conge.SORT_FIELDS);
        }
        return _Agent_conge.sortFields;
    }
    
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_conge";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_conge.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_decoration.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_conge> objects = new ArrayList<_Agent_conge>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_conge) like ? or LOWER(nom_agent) like ? or LOWER(prenom_agent) like ? or LOWER(libelle_sous_direction) like ?) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }
	
}
