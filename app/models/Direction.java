package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Direction extends BaseModel {

//	private long id_direction;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_direction;
	  
	@Type(type = "org.hibernate.type.TextType")
	private String localite;
	@ManyToOne
	private Departement departement;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = { "libelle_direction","localite" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Direction.sortDirections == null) {
	            Direction.sortDirections = Arrays.asList(Direction.SORT_DIRECTIONS);
	        }
	        return Direction.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Direction.sortFields == null) {
	            Direction.sortFields = Arrays.asList(Direction.SORT_FIELDS);
	        }
	        return Direction.sortFields;
	    }


	public  String getLibelle_direction() {
		return libelle_direction;
	}

	public  void setLibelle_direction(String libelle_direction) {
		this.libelle_direction = libelle_direction;
	}

	public  String getLocalite() {
		return localite;
	}

	public  void setLocalite(String localite) {
		this.localite = localite;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_direction";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Direction.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Direction.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Direction> objects = new ArrayList<Direction>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_direction) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
       
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Direction(String libelle_direction, String localite,
			Departement departement) {
		super();
		this.libelle_direction = libelle_direction;
		this.localite = localite;
		this.departement = departement;
	}



}
