package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Compte extends BaseModel  {
//  private long id_compte;
     
	 @Type(type = "org.hibernate.type.TextType")
	    private String typecpte;
    
    @Type(type = "org.hibernate.type.TextType")
    private String login;

    @Type(type = "org.hibernate.type.TextType")
    private String matricule;
    
    @Type(type = "org.hibernate.type.TextType")
    private String mot_passe;   
    
     @Type(type = "org.hibernate.type.TextType")
     @ManyToOne
     private Agent agent;

    public String getLogin() {
        return login;

    }public String getMatricule() {
        return login;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;

    } public void setLogin(String login) {
        this.login = login;
    }
    

    public String getTypecpte() {
		return typecpte;
	}

	public void setTypecpte(String typecpte) {
		this.typecpte = typecpte;
	}

	public String getMot_passe() {
        return mot_passe;
    }

    public void setMot_passe(String mot_passe) {
        this.mot_passe = mot_passe;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

        final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
        
        final private static String[] SORT_FIELDS = { "login", "mot_passe","agent.id","typecpte","matricule"};
        
        private static List<String> sortDirections;

        private static List<String> sortFields;
        
            
            
    public static List<String> getSortDirections() {
        if (Compte.sortDirections == null) {
            Compte.sortDirections = Arrays.asList(Compte.SORT_DIRECTIONS);
        }
        return Compte.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Compte.sortFields == null) {
            Compte.sortFields = Arrays.asList(Compte.SORT_FIELDS);
        }
        return Compte.sortFields;
    }     
    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "mot_passe";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Compte.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Compte.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Compte> objects = new ArrayList<Compte>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(login) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

    
    public static Compte authenticate(String login, String mot_passe) {
        if ((login == null) || (mot_passe == null)) {
            throw new IllegalArgumentException();
        }
        Compte employee = Compte.find("login is ?", login.toLowerCase()).first();
        
        if (employee != null && employee.getMot_passe().equalsIgnoreCase(mot_passe)) {            
                return employee;
        }
        return null;
    }
    
   
}
