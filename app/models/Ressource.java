package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;
@Entity
public class Ressource extends BaseModel{

	//private long id_ressource;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_ressource;
	
	@Type(type = "org.hibernate.type.TextType")
	private String reference;
	
	@Type(type = "org.hibernate.type.TextType")
	private String description;
	
	@Type(type = "org.hibernate.type.TextType")
	private String disponibilite;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "disponibilite","libelle_ressource","description","reference" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Ressource.sortDirections == null) {
	            Ressource.sortDirections = Arrays.asList(Ressource.SORT_DIRECTIONS);
	        }
	        return Ressource.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Ressource.sortFields == null) {
	            Ressource.sortFields = Arrays.asList(Ressource.SORT_FIELDS);
	        }
	        return Ressource.sortFields;
	    }
	    

	public  String getLibelle_ressource() {
		return libelle_ressource;
	}

	public  void setLibelle_ressource(String libelle_ressource) {
		this.libelle_ressource = libelle_ressource;
	}

	public  String getDisponibilite() {
		return disponibilite;
	}

	public  void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}
	
	
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_ressource";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Ressource.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Ressource.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Ressource> objects = new ArrayList<Ressource>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_ressource) like ? or LOWER(reference) like ?) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
        
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Ressource(String libelle_ressource, String disponibilite) {
		super();
		this.libelle_ressource = libelle_ressource;
		this.disponibilite = disponibilite;
	}
}
