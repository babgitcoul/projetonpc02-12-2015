package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_grade extends BaseModel {
	
	
	private  Date  date_grade;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_grade;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Grade grade;
	
	@ManyToOne
	private Agent agent;
	
	private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_grade","libelle_grade","nom_agent","premon_agent","matricule_agent","grade,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_grade.sortDirections == null) {
            _Agent_grade.sortDirections = Arrays.asList(_Agent_grade.SORT_DIRECTIONS);
        }
        return _Agent_grade.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_grade.sortFields == null) {
            _Agent_grade.sortFields = Arrays.asList(_Agent_grade.SORT_FIELDS);
        }
        return _Agent_grade.sortFields;
    }

	public Date getDate_grade() {
		return date_grade;
	}
	public void setDate_grade(Date date_grade) {
		this.date_grade = date_grade;
	}
	public String getLibelle_grade() {
		return libelle_grade;
	}
	public void setLibelle_grade(String libelle_grade) {
		this.libelle_grade = libelle_grade;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_grade(Date date_grade, String libelle_grade,
			String nom_agent, String prenom_agent, String matricule_agent,
			Grade grade, Agent agent) {
		super();
		this.date_grade = date_grade;
		this.libelle_grade = libelle_grade;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.grade = grade;
		this.agent = agent;
	}

	public _Agent_grade() {
		super();
	}


	
	
	

}
