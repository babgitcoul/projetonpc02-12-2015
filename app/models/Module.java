package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;
@Entity
public class Module extends BaseModel {
	
//private long id_module;
@Type(type = "org.hibernate.type.TextType")
private String libelle_module;
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

	final private static String[] SORT_FIELDS = {  "libelle_module" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
       if (Module.sortDirections == null) {
           Module.sortDirections = Arrays.asList(Module.SORT_DIRECTIONS);
       }
       return Module.sortDirections;
   }

   public static List<String> getSortFields() {
       if (Module.sortFields == null) {
           Module.sortFields = Arrays.asList(Module.SORT_FIELDS);
       }
       return Module.sortFields;
   }
   

public  String getLibelle_module() {
	return libelle_module;
}

public  void setLibelle_module(String libelle_module) {
	this.libelle_module = libelle_module;
}


public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

    if ((sortField == null) || (sortField.length() < 1)) {
        sortField = "libelle_module";
    }

    if ((sortDirection == null) || (sortDirection.length() < 1)) {
        sortDirection = "desc";
    }

    if (!Module.getSortFields().contains(sortField)) {
        throw new IllegalArgumentException("sortField is not valid.");
    }

    if (!Module.getSortDirections().contains(sortDirection)) {
        throw new IllegalArgumentException("sortDirection is not valid");
    }

    List<Module> objects = new ArrayList<Module>();
    List<Object> paramList = new ArrayList<Object>();
    StringBuilder sb = new StringBuilder();
    HashMap<String, Object> result = new HashMap();
    Long count;

    sb.append("id > 0");

    if ((keyword != null) && (keyword.trim().length() > 0)) {
        sb.append(" and (LOWER(libelle_module) like ? ) ");
        paramList.add("%" + keyword.toLowerCase() + "%");
     
    }

    Object[] params = paramList.toArray(new Object[paramList.size()]);
    count = count(sb.toString(), params);

    sb.append(" order by " + sortField + " " + sortDirection);
    JPAQuery query = find(sb.toString(), params);
    if (page < 1) {
        page = 1;
    }
    objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

    result.put("objects", objects);
    result.put("count", count);
    return result;
}

public Module(String libelle_module) {
	super();
	this.libelle_module = libelle_module;
}

}
