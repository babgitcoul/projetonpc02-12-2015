package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.Blob;
import play.db.jpa.GenericModel.JPAQuery;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Formateur_formation extends BaseModel{
	
	@JsonProperty	
	private String diplome;
	
	@JsonProperty
	private String carte;	
	
	@JsonProperty
	private String libelle_formation;
 
	@JsonProperty
	private String description;
	
	@JsonProperty
	private String formateur_nom;
	
	@JsonProperty
	private String adresse;
	
	@JsonProperty
	private String telephone;
	
	@JsonProperty
	private String email;
	
	@JsonProperty
	private String localite;
	
	@JsonProperty
	private String siege;
	
	@JsonProperty
	private String site_web;
	
	private Blob logo ;
	
	@ManyToOne
	private Formation formation;
	
	@ManyToOne
	private Formateur formateur;

	public String getDiplome() {
		return diplome;
	}

	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}

	public String getCarte() {
		return carte;
	}

	public void setCarte(String carte) {
		this.carte = carte;
	}

	public String getLibelle_formation() {
		return libelle_formation;
	}

	public void setLibelle_formation(String libelle_formation) {
		this.libelle_formation = libelle_formation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormateur_nom() {
		return formateur_nom;
	}

	public void setFormateur_nom(String formateur_nom) {
		this.formateur_nom = formateur_nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocalite() {
		return localite;
	}

	public void setLocalite(String localite) {
		this.localite = localite;
	}

	public String getSiege() {
		return siege;
	}

	public void setSiege(String siege) {
		this.siege = siege;
	}

	public String getSite_web() {
		return site_web;
	}

	public void setSite_web(String site_web) {
		this.site_web = site_web;
	}

	public Blob getLogo() {
		return logo;
	}

	public void setLogo(Blob logo) {
		this.logo = logo;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Formateur getFormateur() {
		return formateur;
	}

	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}
	

	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

  	final private static String[] SORT_FIELDS = { "diplome","formateur","formation","logo","site_web","siege","localite","email",
  		"telephone","adresse","formateur_nom","description","libelle_formation","carte"};

  	private static List<String> sortDirections;

  	private static List<String> sortFields;    
    
	    public static List<String> getSortDirections() {
	        if (Formateur_formation.sortDirections == null) {
	            Formateur_formation.sortDirections = Arrays.asList(Formateur_formation.SORT_DIRECTIONS);
	        }
	        return Formateur_formation.sortDirections;
	    }
	
	    public static List<String> getSortFields() {
	        if (Formateur_formation.sortFields == null) {
	            Formateur_formation.sortFields = Arrays.asList(Formateur_formation.SORT_FIELDS);
	        }
	        return Formateur_formation.sortFields;
	    }
	    
	    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
	    	
	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "formateur_nom";
	        }
	
	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }
	
	        if (!Formateur_formation.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }
	
	        if (!Formateur_formation.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }
	
	        List<Formateur_formation> objects = new ArrayList<Formateur_formation>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;
	
	        sb.append("id > 0");
	
	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	        	 sb.append(" and LOWER(formateur_nom) like ? or LOWER(libelle_formation) like ?) ");
	             paramList.add("%" + keyword.toLowerCase() + "%");
	             paramList.add("%" + keyword.toLowerCase() + "%");
	          
	        }
	
	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);
	
	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);
	
	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }


}
