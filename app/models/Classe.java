package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Classe  extends BaseModel{
	
	//private long id_classe;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_classe;
	
	@Type(type = "org.hibernate.type.TextType")
	private String description_classe;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

  	final private static String[] SORT_FIELDS = { "libelle_classe","description_classe" };

  	private static List<String> sortDirections;

  	private static List<String> sortFields;    
    
	    public static List<String> getSortDirections() {
	        if (Classe.sortDirections == null) {
	            Classe.sortDirections = Arrays.asList(Classe.SORT_DIRECTIONS);
	        }
	        return Classe.sortDirections;
	    }
	
	    public static List<String> getSortFields() {
	        if (Classe.sortFields == null) {
	            Classe.sortFields = Arrays.asList(Classe.SORT_FIELDS);
	        }
	        return Classe.sortFields;
	    }
			
		
		public String getLibelle_classe() {
			return libelle_classe;
		}
		public void setLibelle_classe(String libelle_classe) {
			this.libelle_classe = libelle_classe;
		}
		public String getDescription_classe() {
			return description_classe;
		}
		public void setDescription_classe(String description_classe) {
			this.description_classe = description_classe;
		}
		
		
		public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
	
	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_classe";
	        }
	
	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }
	
	        if (!Classe.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }
	
	        if (!Classe.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }
	
	        List<Classe> objects = new ArrayList<Classe>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;
	
	        sb.append("id > 0");
	
	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_classe) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	          
	        }
	
	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);
	
	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);
	
	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

		public Classe(String libelle_classe, String description_classe) {
			super();
			this.libelle_classe = libelle_classe;
			this.description_classe = description_classe;
		}
		
		
}
