package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Emploi  extends BaseModel{
	
//	private long id_emploi;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_emploi;
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_emploi" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Emploi.sortDirections == null) {
	            Emploi.sortDirections = Arrays.asList(Emploi.SORT_DIRECTIONS);
	        }
	        return Emploi.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Emploi.sortFields == null) {
	            Emploi.sortFields = Arrays.asList(Emploi.SORT_FIELDS);
	        }
	        return Emploi.sortFields;
	    }
	    
	public String getLibelle_emploi() {
		return libelle_emploi;
	}
	public void setLibelle_emploi(String libelle_emploi) {
		this.libelle_emploi = libelle_emploi;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_emploi";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Emploi.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Emploi.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Emploi> objects = new ArrayList<Emploi>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_emploi) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
         
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Emploi(String libelle_emploi) {
		super();
		this.libelle_emploi = libelle_emploi;
	}
	
	


}
