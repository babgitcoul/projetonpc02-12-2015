package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class _Agent_statu extends BaseModel {
	


	private  Date date_status;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String libelle_status;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String motif;

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	@Type(type = "org.hibernate.type.TextType")	
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String matricule_agent;
	
	@ManyToOne
	private Status status;
	
	@ManyToOne	
	private Agent agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "agent","status","matricule_agent","nom_agent","prenom_agent","libelle_status","date_status","status.id" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_statu.sortDirections == null) {
            _Agent_statu.sortDirections = Arrays.asList(_Agent_statu.SORT_DIRECTIONS);
        }
        return _Agent_statu.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_statu.sortFields == null) {
            _Agent_statu.sortFields = Arrays.asList(_Agent_statu.SORT_FIELDS);
        }
        return _Agent_statu.sortFields;
    }

	public Date getDate_status() {
		return date_status;
	}
	public void setDate_status(Date date_status) {
		this.date_status = date_status;
	}
	public String getLibelle_status() {
		return libelle_status;
	}
	public void setLibelle_status(String libelle_status) {
		this.libelle_status = libelle_status;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	
	

	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public _Agent_statu(Date date_status, String libelle_status,
			String nom_agent, String prenom_agent, String matricule_agent,
			Status status, Agent agent) {
		super();
		this.date_status = date_status;
		this.libelle_status = libelle_status;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.status = status;
		this.agent = agent;
	}

	public _Agent_statu() {
		super();
	}
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "nom_agent";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_statu.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_statu.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_statu> objects = new ArrayList<_Agent_statu>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom_agent) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }
	
	
	

}
