package models;

//import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Autorisation_stage_ext extends BaseModel {
	

	@Type(type = "org.hibernate.type.TextType")	
	private String    lieu;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String    intiateur;
	
		
	private Date      date_debut;
	
	
	private Date      date_fin;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String    libelle_grade;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String 	  objet;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String    description;	
	@ManyToOne
	private Grade    grade;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String    formation;
	
	@ManyToOne	
	private Agent    agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "lieu","initiateur","date_debut","date_fin","libelle_grade","objet","description","grade","formation","agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Autorisation_stage_ext.sortDirections == null) {
            _Autorisation_stage_ext.sortDirections = Arrays.asList(_Autorisation_stage_ext.SORT_DIRECTIONS);
        }
        return _Autorisation_stage_ext.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Autorisation_stage_ext.sortFields == null) {
            _Autorisation_stage_ext.sortFields = Arrays.asList(_Autorisation_stage_ext.SORT_FIELDS);
        }
        return _Autorisation_stage_ext.sortFields;
    }

	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public String getIntiateur() {
		return intiateur;
	}
	public void setIntiateur(String intiateur) {
		this.intiateur = intiateur;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getLibelle_grade() {
		return libelle_grade;
	}
	public void setLibelle_grade(String libelle_grade) {
		this.libelle_grade = libelle_grade;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getFormation() {
		return formation;
	}
	public void setFormation(String formation) {
		this.formation = formation;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Autorisation_stage_ext(String lieu, String intiateur,
			Date date_debut, Date date_fin, String libelle_grade, String objet,
			String description, Grade grade, String formation, Agent agent) {
		super();
		this.lieu = lieu;
		this.intiateur = intiateur;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.libelle_grade = libelle_grade;
		this.objet = objet;
		this.description = description;
		this.grade = grade;
		this.formation = formation;
		this.agent = agent;
	}
	
	
	
	

}
