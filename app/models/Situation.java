package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Situation  extends BaseModel{
  
  //private long id_situation;
  
  @Type(type = "org.hibernate.type.TextType")
  private String libelle_situation;
  
  @Type(type = "org.hibernate.type.TextType")
  private String description_situation;
  
  final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

    final private static String[] SORT_FIELDS = { "libelle_situation","description_situation" };

    private static List<String> sortDirections;

    private static List<String> sortFields;    
    
      public static List<String> getSortDirections() {
          if (Situation.sortDirections == null) {
              Situation.sortDirections = Arrays.asList(Situation.SORT_DIRECTIONS);
          }
          return Situation.sortDirections;
      }
  
      public static List<String> getSortFields() {
          if (Situation.sortFields == null) {
              Situation.sortFields = Arrays.asList(Situation.SORT_FIELDS);
          }
          return Situation.sortFields;
      }
      
    
    public String getLibelle_situation() {
      return libelle_situation;
    }
    public void setLibelle_situation(String libelle_situation) {
      this.libelle_situation = libelle_situation;
    }
    public String getDescription_situation() {
      return description_situation;
    }
    public void setDescription_situation(String description_situation) {
      this.description_situation = description_situation;
    }
    
    
    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
  
          if ((sortField == null) || (sortField.length() < 1)) {
              sortField = "libelle_situation";
          }
  
          if ((sortDirection == null) || (sortDirection.length() < 1)) {
              sortDirection = "desc";
          }
  
          if (!Situation.getSortFields().contains(sortField)) {
              throw new IllegalArgumentException("sortField is not valid.");
          }
  
          if (!Situation.getSortDirections().contains(sortDirection)) {
              throw new IllegalArgumentException("sortDirection is not valid");
          }
  
          List<Situation> objects = new ArrayList<Situation>();
          List<Object> paramList = new ArrayList<Object>();
          StringBuilder sb = new StringBuilder();
          HashMap<String, Object> result = new HashMap();
          Long count;
  
          sb.append("id > 0");
  
          if ((keyword != null) && (keyword.trim().length() > 0)) {
              sb.append(" and (LOWER(libelle_situation) like ? ) ");
              paramList.add("%" + keyword.toLowerCase() + "%");
            
          }
  
          Object[] params = paramList.toArray(new Object[paramList.size()]);
          count = count(sb.toString(), params);
  
          sb.append(" order by " + sortField + " " + sortDirection);
          JPAQuery query = find(sb.toString(), params);
          if (page < 1) {
              page = 1;
          }
          objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);
  
          result.put("objects", objects);
          result.put("count", count);
          return result;
      }

    public Situation(String libelle_situation, String description_situation) {
      super();
      this.libelle_situation = libelle_situation;
      this.description_situation = description_situation;
    }
    
    
}
