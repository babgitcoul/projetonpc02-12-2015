package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Type;

@Entity
public class _Agent_classe extends BaseModel {
	
	
	private Date date_classe;
	
	@Type(type = "org.hibernate.type.TextType")
	private  String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String  prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String  matricule_agent;
	
	@ManyToOne
	private Agent agent;
	
	@ManyToOne
	private Classe classe;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_classe","nom_agent","prenom_agent","matricule_agent","agent","classe" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_classe.sortDirections == null) {
            _Agent_classe.sortDirections = Arrays.asList(_Agent_classe.SORT_DIRECTIONS);
        }
        return _Agent_classe.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_classe.sortFields == null) {
            _Agent_classe.sortFields = Arrays.asList(_Agent_classe.SORT_FIELDS);
        }
        return _Agent_classe.sortFields;
    }

	public Date getDate_classe() {
		return date_classe;
	}
	public void setDate_classe(Date date_classe) {
		this.date_classe = date_classe;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public _Agent_classe(Date date_classe, String nom_agent,
			String prenom_agent, String matricule_agent, Agent agent,
			Classe classe) {
		super();
		this.date_classe = date_classe;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.agent = agent;
		this.classe = classe;
	}

	public _Agent_classe() {
		super();
	}
	
	
	

}
