package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
   

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class _Agent_sanction  extends BaseModel{

	
	private Date date_debut;  
	private Date date_fin;
	 
	@Type(type = "org.hibernate.type.TextType")
	private String motif ;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sanction;
	
	public String getLibelle_sanction() {
		return libelle_sanction;
	}

	public void setLibelle_sanction(String libelle_sanction) {
		this.libelle_sanction = libelle_sanction;
	}

	public String getNom_agent() {
		return nom_agent;
	}

	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}

	public String getPrenom_agent() {
		return prenom_agent;
	}

	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}

	public String getMatricule_agent() {
		return matricule_agent;
	}

	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}

	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Sanction sanction;

	@ManyToOne
	private Agent agent;
	
    final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_enregistrement","date_debut","date_fin","motif","sanction","agent","libelle_sanction","nom_agent", "matricule_agent","prenom_agent","sanction.id"};
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_sanction.sortDirections == null) {
            _Agent_sanction.sortDirections = Arrays.asList(_Agent_sanction.SORT_DIRECTIONS);
        }
        return _Agent_sanction.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_sanction.sortFields == null) {
            _Agent_sanction.sortFields = Arrays.asList(_Agent_sanction.SORT_FIELDS);
        }
        return _Agent_sanction.sortFields;
    }

	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getMotif() {
		return motif;
	}
	public void setMotif(String motif) {
		this.motif = motif;
	}

	public Sanction getSanction() {
		return sanction;
	}

	public void setSanction(Sanction sanction) {
		this.sanction = sanction;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_sanction( Date date_debut,
			Date date_fin, String motif, Sanction sanction, Agent agent) {
		super();
	
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.motif = motif;
		this.sanction = sanction;
		this.agent = agent;
		
	}


public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "nom_agent";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_sanction.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_sanction.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_sanction> objects = new ArrayList<_Agent_sanction>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom_agent) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

public _Agent_sanction() {
	super();
}
	

}
