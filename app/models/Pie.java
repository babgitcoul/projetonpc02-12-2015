package models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pie {
	
	@JsonProperty
	private String label;
	
	@JsonProperty
	private int data;
	
	
	public Pie(){
		
	}
	
	public Pie(String label, int data){
		this.label = label;
		this.data = data;
	}
	
	public void inc(){
		this.data = getData() + 1;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}
	
	

}
