package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
 @Entity
public class Type_agent  extends BaseModel{
	
//	private long id_type_agent;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_type_agent;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_type_agent" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Type_agent.sortDirections == null) {
	            Type_agent.sortDirections = Arrays.asList(Type_agent.SORT_DIRECTIONS);
	        }
	        return Type_agent.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Type_agent.sortFields == null) {
	            Type_agent.sortFields = Arrays.asList(Type_agent.SORT_FIELDS);
	        }
	        return Type_agent.sortFields;
	    }
	    

	public String getLibelle_type_agent() {
		return libelle_type_agent;
	}

	public void setLibelle_type_agent(String libelle_type_agent) {
		this.libelle_type_agent = libelle_type_agent;
	}
	
	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_type_agent";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Type_agent.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Type_agent.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Type_agent> objects = new ArrayList<Type_agent>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_type_agent) like ?) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	     
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Type_agent(String libelle_type_agent) {
		super();
		this.libelle_type_agent = libelle_type_agent;
	}


}
