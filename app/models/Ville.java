package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

@Entity
public class Ville extends BaseModel {
	
 @Type(type = "org.hibernate.type.TextType")
	private String libelle_ville;

 	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "libelle_ville" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (Ville.sortDirections == null) {
            Ville.sortDirections = Arrays.asList(Ville.SORT_DIRECTIONS);
        }
        return Ville.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Ville.sortFields == null) {
            Ville.sortFields = Arrays.asList(Ville.SORT_FIELDS);
        }
        return Ville.sortFields;
    }
    
	public String getLibelle_ville() {
		return libelle_ville;
	}

	public void setLibelle_ville(String libelle_ville) {
		this.libelle_ville = libelle_ville;
	}
	
	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_ville";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Ville.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Ville.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Ville> objects = new ArrayList<Ville>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_ville) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	           	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Ville(String libelle_ville) {
		super();
		this.libelle_ville = libelle_ville;
	}


	
}
