package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonProperty;

import play.data.validation.Required;

@Entity
public class Page extends BaseModel {

    final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

    final private static String[] SORT_FIELDS = { "created", "title", "position", "content" };

    private static List<String> sortDirections;

    private static List<String> sortFields;

    @Required
    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    @Required
    @JsonProperty
    private int position;

    @Required
    @Type(type = "org.hibernate.type.TextType")
    private String title;

    public Page(String content, int position, String title) {
        super();
        this.content = content;
        this.position = position;
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public int getPosition() {
        return position;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static List<String> getSortDirections() {
        if (Page.sortDirections == null) {
            Page.sortDirections = Arrays.asList(Page.SORT_DIRECTIONS);
        }
        return Page.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Page.sortFields == null) {
            Page.sortFields = Arrays.asList(Page.SORT_FIELDS);
        }
        return Page.sortFields;
    }

    public static int findLastPosition() {
        Page page = Page.find("order by position desc").first();
        if (page != null) {
            return page.position;
        } else {
            return 0;
        }
    }

    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "position";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "asc";
        }

        if (!Page.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Page.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Page> objects = new ArrayList<Page>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(content) like ? or LOWER(title) like ?) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = Page.count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = Page.find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, DEFAULT_PAGINATE_COUNT);

        result.put(PAGINATE_OBJECT_KEY, objects);
        result.put(PAGINATE_COUNT_KEY, count);
        return result;
    }

}
