package models;

import java.io.IOException;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import play.Logger;
import play.data.binding.NoBinding;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@MappedSuperclass
public abstract class BaseModel extends Model {

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty
    @NoBinding
    private Date created;

    // Hack to remove persistent property from JSON output
    private transient boolean persistent;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty
    @NoBinding
    private Date updated;

    final public static int DEFAULT_PAGINATE_COUNT = 50;
    final public static String PAGINATE_OBJECT_KEY = "objects";
    final public static String PAGINATE_COUNT_KEY = "count";
    public Date getCreated() {
        return this.created;
    }

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    @JsonIgnore
    public Boolean getPersistent() {
        return this.persistent;
    }

    public Date getUpdated() {
        return this.updated;
    }

    @PrePersist
    protected void onCreate() {
        this.updated = this.created = new Date();
    }

  

    @PreUpdate
    protected void onUpdate() {
        this.updated = new Date();
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    protected String toJson() {
        String json = "";
        ObjectMapper jsonObjectMapper = new ObjectMapper().configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        try {
            json = jsonObjectMapper.writeValueAsString(this);
        } catch (JsonGenerationException e) {
            Logger.error(e.getMessage());
        } catch (JsonMappingException e) {
            Logger.error(e.getMessage());
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }
        return json;
    }

    public static int getStartFromPage(Integer page) {
        if ((page == null) || (page < 1)) {
            page = 1;
        }
        return ((page - 1) * DEFAULT_PAGINATE_COUNT);
    }
}
