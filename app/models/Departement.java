package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Departement extends BaseModel {
	
//	private long id_departement;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_departement;
	
	@Type(type = "org.hibernate.type.TextType")
	@ManyToOne
	private Direction direction;


	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_departement","direction.id" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Departement.sortDirections == null) {
	            Departement.sortDirections = Arrays.asList(Departement.SORT_DIRECTIONS);
	        }
	        return Departement.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Departement.sortFields == null) {
	            Departement.sortFields = Arrays.asList(Departement.SORT_FIELDS);
	        }
	        return Departement.sortFields;
	    }

	public  String getLibelle_departement() {
		return libelle_departement;
	}

	public  void setLibelle_departement(String libelle_departement) {
		this.libelle_departement = libelle_departement;
	}



	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_departement";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Departement.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Departement.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Departement> objects = new ArrayList<Departement>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_departement) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Departement(String libelle_departement) {
		super();
		this.libelle_departement = libelle_departement;
		
	}
	
}
