package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonProperty;

import play.data.validation.Required;

@Entity
public class Alert extends BaseModel {

    final private static String[] SORT_DIRECTIONS = { "asc", "desc" };

    final private static String[] SORT_FIELDS = { "updated", "created", "title", "content" };

    private static List<String> sortDirections;

    private static List<String> sortFields;

    @Required
    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    @Required
    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String title;

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static List<String> getSortDirections() {
        if (Alert.sortDirections == null) {
            Alert.sortDirections = Arrays.asList(Alert.SORT_DIRECTIONS);
        }
        return Alert.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Alert.sortFields == null) {
            Alert.sortFields = Arrays.asList(Alert.SORT_FIELDS);
        }
        return Alert.sortFields;
    }

    public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "created";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Alert.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Alert.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Alert> objects = new ArrayList<Alert>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(content) like ? or LOWER(title) like ?) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Alert() {
		super();
	}
    

}
