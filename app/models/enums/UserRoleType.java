package models.enums;

import play.i18n.Messages;

public enum UserRoleType {

    CAISSE (100),
    COMPTABILITE (200),
    SAISIE (300),
    STOCK (400),
    ADMIN (1000);

    private int code;

    UserRoleType(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return this.code;
    }

    public static UserRoleType parseCode(int code) {
        UserRoleType userRole = null;

        for (UserRoleType item : userRole.values()) {
            if (item.getCode() == code) {
                userRole = item;
                break;
            }
        }
        return userRole;
    }

    @Override
    public String toString() {
        return Messages.get(this.name());
    }

}
