package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.Blob;
import play.db.jpa.GenericModel.JPAQuery;



@Entity
public class Formateur extends BaseModel {
	
	
	private String formateur_nom;
	private String adresse;
	private String telephone;
	private String email;
	private String localite;
	private String siege;
	private String site_web;
	private Blob logo ;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "logo","formateur_nom","adresse","telephone","email","localite","siege,site_web" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Formateur.sortDirections == null) {
	            Formateur.sortDirections = Arrays.asList(Formateur.SORT_DIRECTIONS);
	        }
	        return Formateur.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Formateur.sortFields == null) {
	            Formateur.sortFields = Arrays.asList(Formateur.SORT_FIELDS);
	        }
	        return Formateur.sortFields;
	    }
	    
	public String getFormateur_nom() {
			return formateur_nom;
		}

		public void setFormateur_nom(String formateur_nom) {
			this.formateur_nom = formateur_nom;
		}

	public  String getAdresse() {
		return adresse;
	}

	public  void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public  String getTelephone() {
		return telephone;
	}

	public  void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public  String getEmail() {
		return email;
	}

	public  void setEmail(String email) {
		this.email = email;
	}

	public  String getLocalite() {
		return localite;
	}

	public  void setLocalite(String localite) {
		this.localite = localite;
	}

	public  String getSiege() {
		return siege;
	}

	public  void setSiege(String siege) {
		this.siege = siege;
	}

	public  String getSite_web() {
		return site_web;
	}

	public  void setSite_web(String site_web) {
		this.site_web = site_web;
	}

	public Blob getLogo() {
		return logo;
	}

	public void setLogo(Blob logo) {
		this.logo = logo;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "formateur_nom";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Formateur.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Formateur.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Formateur> objects = new ArrayList<Formateur>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(formateur_nom) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Formateur(String formateur_nom, String adresse, String telephone,
			String email, String localite, String siege, String site_web,
			Blob logo) {
		super();
		this.formateur_nom = formateur_nom;
		this.adresse = adresse;
		this.telephone = telephone;
		this.email = email;
		this.localite = localite;
		this.siege = siege;
		this.site_web = site_web;
		this.logo = logo;
	}

}
