package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Blob;
import play.db.jpa.GenericModel.JPAQuery;
@Entity
public class _Piece_agent extends BaseModel {
	
	

	private Date date_piece;
	private String numero_piece;
	private Date date_delivrance ;
	private String auteur;
	private String lieu_delivrance;
	private String poste_auteur;
	private String libelle_piece;
	private Date date_fin_validite;
	private String filename;
	private Blob piece;
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Blob getPiece() {
		return piece;
	}

	public void setPiece(Blob piece) {
		this.piece = piece;
	}

	@ManyToOne
	private _Agent_conge conge;
	@ManyToOne
	private Agent agent;
	
	
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = { "filename","piece", "date_piece","numero_piece","date_delivrance","auteur","lieu_delivrance","poste_auteur","libelle_piece","date_fin_valide","conge,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Piece_agent.sortDirections == null) {
            _Piece_agent.sortDirections = Arrays.asList(_Piece_agent.SORT_DIRECTIONS);
        }
        return _Piece_agent.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Piece_agent.sortFields == null) {
            _Piece_agent.sortFields = Arrays.asList(_Piece_agent.SORT_FIELDS);
        }
        return _Piece_agent.sortFields;
    }

	public Date getDate_piece() {
		return date_piece;
	}
	public void setDate_piece(Date date_piece) {
		this.date_piece = date_piece;
	}
	public String getNumero_piece() {
		return numero_piece;
	}
	public void setNumero_piece(String numero_piece) {
		this.numero_piece = numero_piece;
	}
	public Date getDate_delivrance() {
		return date_delivrance;
	}
	public void setDate_delivrance(Date date_delivrance) {
		this.date_delivrance = date_delivrance;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getLieu_delivrance() {
		return lieu_delivrance;
	}
	public void setLieu_delivrance(String lieu_delivrance) {
		this.lieu_delivrance = lieu_delivrance;
	}
	public String getPoste_auteur() {
		return poste_auteur;
	}
	public void setPoste_auteur(String poste_auteur) {
		this.poste_auteur = poste_auteur;
	}
	public String getLibelle_piece() {
		return libelle_piece;
	}
	public void setLibelle_piece(String libelle_piece) {
		this.libelle_piece = libelle_piece;
	}
	public Date getDate_fin_validite() {
		return date_fin_validite;
	}
	public void setDate_fin_validite(Date date_fin_validite) {
		this.date_fin_validite = date_fin_validite;
	}
	

	public _Agent_conge getConge() {
		return conge;
	}

	public void setConge(_Agent_conge conge) {
		this.conge = conge;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Piece_agent(Date date_piece, String numero_piece,
			Date date_delivrance, String auteur, String lieu_delivrance,
			String poste_auteur, String libelle_piece, Date date_fin_validite,
			String filename, Blob piece, _Agent_conge conge, Agent agent) {
		super();
		this.date_piece = date_piece;
		this.numero_piece = numero_piece;
		this.date_delivrance = date_delivrance;
		this.auteur = auteur;
		this.lieu_delivrance = lieu_delivrance;
		this.poste_auteur = poste_auteur;
		this.libelle_piece = libelle_piece;
		this.date_fin_validite = date_fin_validite;
		this.filename = filename;
		this.piece = piece;
		this.conge = conge;
		this.agent = agent;
	}

	public _Piece_agent() {
		super();
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_piece";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Piece_agent.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Piece_agent.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Piece_agent> objects = new ArrayList<_Piece_agent>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_piece) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
         
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public _Piece_agent(String libelle_piece) {
		super();
		this.libelle_piece = libelle_piece;
	}

	
	

}
