package models;

import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

@Entity
public class _Action  extends BaseModel{
	
//	private long id_action;	
	@Type(type = "org.hibernate.type.TextType")
	private Date date;
	
	@Type(type = "org.hibernate.type.TextType")
	private String traitement;
	
	@Type(type = "org.hibernate.type.TextType")
	private String 	compte;
	
	@Type(type = "org.hibernate.type.TextType")
	private String  module;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "compte","module","traitement" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Action.sortDirections == null) {
            _Action.sortDirections = Arrays.asList(_Action.SORT_DIRECTIONS);
        }
        return _Action.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Action.sortFields == null) {
            _Action.sortFields = Arrays.asList(_Action.SORT_FIELDS);
        }
        return _Action.sortFields;
    }
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTraitement() {
		return traitement;
	}
	public void setTraitement(String traitement) {
		this.traitement = traitement;
	}
	public String getCompte() {
		return compte;
	}
	public void setCompte(String compte) {
		this.compte = compte;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}

	public _Action( String traitement, String compte, String module) {
		super();
		//this.date = date;
		this.traitement = traitement;
		this.compte = compte;
		this.module = module;
	}
	

}
