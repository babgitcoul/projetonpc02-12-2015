package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Pv  extends BaseModel{
	
	//private long id_pv;
	@Type(type = "org.hibernate.type.TextType")
	private String description;
	
	@Type(type = "org.hibernate.type.TextType")
	private String numero_pv;
	
	@Type(type = "org.hibernate.type.TextType")
	@ManyToOne
	private Formation formation;
	
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "numero_pv","formation.id","description" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Pv.sortDirections == null) {
	            Pv.sortDirections = Arrays.asList(Pv.SORT_DIRECTIONS);
	        }
	        return Pv.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Pv.sortFields == null) {
	            Pv.sortFields = Arrays.asList(Pv.SORT_FIELDS);
	        }
	        return Pv.sortFields;
	    }
	    

	public  String getDescription() {
		return description;
	}

	public  void setDescription(String description) {
		this.description = description;
	}

	public  String getNumero_pv() {
		return numero_pv;
	}

	public  void setNumero_pv(String numero_pv) {
		this.numero_pv = numero_pv;
	}

	public  Formation getFormation() {
		return formation;
	}

	public  void setFormation(Formation formation) {
		this.formation = formation;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "numero_pv";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Pv.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Pv.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Pv> objects = new ArrayList<Pv>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(numero_pv) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Pv(String description, String numero_pv, Formation formation) {
		super();
		this.description = description;
		this.numero_pv = numero_pv;
		this.formation = formation;
	}

}
