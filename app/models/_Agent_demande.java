package models;

//import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_demande extends BaseModel {
	
	
	private Date   date_demande;
	
	@Type(type = "org.hibernate.type.TextType")
	private String status;
	
	@Type(type = "org.hibernate.type.TextType")
	private String type_demande;
	
	
	private Date   date_retrait;
	
	@Type(type = "org.hibernate.type.TextType")
	private String diplome;
	
	@Type(type = "org.hibernate.type.TextType")
	private String carte;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne	
	private Formation formation;
	
	@ManyToOne
	private Agent agent;
	
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_demande","status","type_demande","date_retrait","diplome","carte"
		, "nom_agent","prenom_agent","matricule_agent","formation,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_demande.sortDirections == null) {
            _Agent_demande.sortDirections = Arrays.asList(_Agent_demande.SORT_DIRECTIONS);
        }
        return _Agent_demande.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_demande.sortFields == null) {
            _Agent_demande.sortFields = Arrays.asList(_Agent_demande.SORT_FIELDS);
        }
        return _Agent_demande.sortFields;
    }

	public Date getDate_demande() {
		return date_demande;
	}
	public void setDate_demande(Date date_demande) {
		this.date_demande = date_demande;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType_demande() {
		return type_demande;
	}
	public void setType_demande(String type_demande) {
		this.type_demande = type_demande;
	}
	public Date getDate_retrait() {
		return date_retrait;
	}
	public void setDate_retrait(Date date_retrait) {
		this.date_retrait = date_retrait;
	}
	public String getDiplome() {
		return diplome;
	}
	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}
	public String getCarte() {
		return carte;
	}
	public void setCarte(String carte) {
		this.carte = carte;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}
	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_demande(Date date_demande, String status,
			String type_demande,  String diplome,
			String carte, String nom_agent, String prenom_agent,
			String matricule_agent, Formation formation, Agent agent) {
			super();
			this.date_demande = date_demande;
			this.status = status;
			this.type_demande = type_demande;
		
			this.diplome = diplome;
			this.carte = carte;
			this.nom_agent = nom_agent;
			this.prenom_agent = prenom_agent;
			this.matricule_agent = matricule_agent;
			this.formation = formation;
			this.agent = agent;
	}
	
	
	

}
