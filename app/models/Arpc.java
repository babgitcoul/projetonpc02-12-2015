package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import play.db.jpa.GenericModel.JPAQuery;

@Entity

public class Arpc extends BaseModel {
	
    final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "libelle_arpc" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	   
    public static List<String> getSortDirections() {
        if (Arpc.sortDirections == null) {
            Arpc.sortDirections = Arrays.asList(Arpc.SORT_DIRECTIONS);
        }
        return Arpc.sortDirections;
    }

    public static List<String> getSortFields() {
        if (Arpc.sortFields == null) {
            Arpc.sortFields = Arrays.asList(Arpc.SORT_FIELDS);
        }
        return Arpc.sortFields;
    }
	
//	private long id_artc;
	private String libelle_arpc;

	public String getLibelle_arpc() {
		return libelle_arpc;
	}

	public void setLibelle_arpc(String libelle_arpc) {
		this.libelle_arpc = libelle_arpc;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_arpc";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Arpc.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Arpc.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Arpc> objects = new ArrayList<Arpc>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_arpc) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Arpc(String libelle_arpc) {
		super();
		this.libelle_arpc = libelle_arpc;
	}
	

}
