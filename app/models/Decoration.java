package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Decoration extends BaseModel {
	
//	private long id_decoration;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_decoration;
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_decoration" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Decoration.sortDirections == null) {
	            Decoration.sortDirections = Arrays.asList(Decoration.SORT_DIRECTIONS);
	        }
	        return Decoration.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Decoration.sortFields == null) {
	            Decoration.sortFields = Arrays.asList(Decoration.SORT_FIELDS);
	        }
	        return Decoration.sortFields;
	    }

	public String getLibelle_decoration() {
		return libelle_decoration;
	}

	public void setLibelle_decoration(String libelle_decoration) {
		this.libelle_decoration = libelle_decoration;
	}

	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_decoration";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Decoration.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Decoration.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Decoration> objects = new ArrayList<Decoration>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_decoration) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
     
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Decoration(String libelle_decoration) {
		super();
		this.libelle_decoration = libelle_decoration;
	}
}
