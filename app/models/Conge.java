package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Conge extends BaseModel {
	

  @Type(type = "org.hibernate.type.TextType")
  private String libelle_conge;

  @Type(type = "org.hibernate.type.TextType")
  private String nbreJours;

  public String getLibelle_conge() {
    return libelle_conge;
  }

  public void setLibelle_conge(String libelle_conge) {
    this.libelle_conge = libelle_conge;
  }
  
  public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_conge";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Conge.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Conge.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Conge> objects = new ArrayList<Conge>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_conge) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
         
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }
    

  public Conge(String libelle_conge) {
    super();
    this.libelle_conge = libelle_conge;
  }
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_conge","nbreJours" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		    
		    
		    public static List<String> getSortDirections() {
		        if (Conge.sortDirections == null) {
		            Conge.sortDirections = Arrays.asList(Conge.SORT_DIRECTIONS);
		        }
		        return Conge.sortDirections;
		    }

		    public static List<String> getSortFields() {
		        if (Conge.sortFields == null) {
		            Conge.sortFields = Arrays.asList(Conge.SORT_FIELDS);
		        }
		        return Conge.sortFields;
		    }

}
