package models;

import models.deadbolt.Role;
import models.enums.UserRoleType;

public class UserRole implements Role {

    private int code;

    public UserRole(int code) {
        this.code = code;
    }

    @Override
    public String getRoleName() {
        return UserRoleType.parseCode(this.code).name();
    }

    public final static String ADMIN = "ADMIN";
    public final static String CAISSE = "CAISSE";
    public final static String COMPTABILITE = "COMPTABILITE";
    public final static String SAISIE = "SAISIE";
    public final static String STOCK = "STOCK";
    public final static String [] ROLES = {ADMIN, COMPTABILITE, CAISSE, SAISIE, STOCK};

}
