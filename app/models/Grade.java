package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Grade extends BaseModel {
	
//	private long id_grade;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_grade;
	
	@Type(type = "org.hibernate.type.TextType")
	private String description_grade;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_grade","description_grade" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Grade.sortDirections == null) {
	            Grade.sortDirections = Arrays.asList(Grade.SORT_DIRECTIONS);
	        }
	        return Grade.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Grade.sortFields == null) {
	            Grade.sortFields = Arrays.asList(Grade.SORT_FIELDS);
	        }
	        return Grade.sortFields;
	    }
	    
	public String getLibelle_grade() {
		return libelle_grade;
	}
	public void setLibelle_grade(String libelle_grade) {
		this.libelle_grade = libelle_grade;
	}
	public String getDescription_grade() {
		return description_grade;
	}
	public void setDescription_grade(String description_grade) {
		this.description_grade = description_grade;
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_grade";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Grade.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Grade.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Grade> objects = new ArrayList<Grade>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_grade) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
        
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Grade(String libelle_grade, String description_grade) {
		super();
		this.libelle_grade = libelle_grade;
		this.description_grade = description_grade;
	}

}
