package models;

import java.sql.Blob;
//import java.sql.Date;
import java.util.*; 

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.Model;
import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Agent  extends BaseModel{
	
	//private long   id_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_csu_courant;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sous_dir_courant;
	
	@Type(type = "org.hibernate.type.TextType")
	private Date date_prise_svce_poste_courant;
	
	public String getLibelle_csu_courant() {
		return libelle_csu_courant;
	}

	public void setLibelle_csu_courant(String libelle_csu_courant) {
		this.libelle_csu_courant = libelle_csu_courant;
	}

	public String getLibelle_sous_dir_courant() {
		return libelle_sous_dir_courant;
	}

	public void setLibelle_sous_dir_courant(String libelle_sous_dir_courant) {
		this.libelle_sous_dir_courant = libelle_sous_dir_courant;
	}

	public Date getDate_prise_svce_poste_courant() {
		return date_prise_svce_poste_courant;
	}

	public void setDate_prise_svce_poste_courant(Date date_prise_svce_poste_courant) {
		this.date_prise_svce_poste_courant = date_prise_svce_poste_courant;
	}
	private String matricule;
	private String libelle_echelon;
	private String libelle_classe;
	
	public String getLibelle_echelon() {
		return libelle_echelon;
	}

	public void setLibelle_echelon(String libelle_echelon) {
		this.libelle_echelon = libelle_echelon;
	}

	public String getLibelle_classe() {
		return libelle_classe;
	}

	public void setLibelle_classe(String libelle_classe) {
		this.libelle_classe = libelle_classe;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	@Type(type = "org.hibernate.type.TextType")
	private String prenom;
	
	private String email;
	
	private String nomPere;
	private  String nomMere;
	
	 public String getNomPere() {
		return nomPere;
	}

	public void setNomPere(String nomPere) {
		this.nomPere = nomPere;
	}

	public String getNomMere() {
		return nomMere;
	}

	public void setNomMere(String nomMere) {
		this.nomMere = nomMere;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	private String photFileName;
  
	private Date   delivre_le;
	private Date   expire_le;
	
	public Date getDelivre_le() {
		return delivre_le;
	}

	public void setDelivre_le(Date delivre_le) {
		this.delivre_le = delivre_le;
	}

	public Date getExpire_le() {
		return expire_le;
	}

	public void setExpire_le(Date expire_le) {
		this.expire_le = expire_le;
	}
	private Date   date_naissance;
	
	private String numCni;
	
	private String nomJeuneFille;
	
	private Long nombreEnf;
	
	public Long getNombreEnf() {
		return nombreEnf;
	}

	public void setNombreEnf(Long nombreEnf) {
		this.nombreEnf = nombreEnf;
	}

	public String getNomJeuneFille() {
		return nomJeuneFille;
	}

	public void setNomJeuneFille(String nomJeuneFille) {
		this.nomJeuneFille = nomJeuneFille;
	}

	public String getNumCni() {
		return numCni;
	}

	public void setNumCni(String numCni) {
		this.numCni = numCni;
	}

	public String getPhotFileName() {
		return photFileName;
	}

	public void setPhotFileName(String photFileName) {
		this.photFileName = photFileName;
	}
	@Type(type = "org.hibernate.type.TextType")
	private String   lieu_naissance;
	
	@Type(type = "org.hibernate.type.TextType")
	private String sexe;
	
	@Type(type = "org.hibernate.type.TextType")
	private String contact;
	
    @Type(type = "org.hibernate.type.TextType")
	private String adresse;
	
	@Type(type = "org.hibernate.type.TextType")
	private String sit_matr;
	
	 @Type(type = "org.hibernate.type.TextType")
	private String fonctionaire;
	
	@Type(type = "blob")
	private Blob photo;
	
	public Blob moncv;
	
	@Type(type = "org.hibernate.type.TextType")
	private String num_note_svce;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_type_agent;
	
	public String getLibelle_type_agent() {
		return libelle_type_agent;
	}

	public void setLibelle_type_agent(String libelle_type_agent) {
		this.libelle_type_agent = libelle_type_agent;
	}
	@Type(type = "org.hibernate.type.TextType")
	private String mecano;
	
	@Type(type = "org.hibernate.type.TextType")
	private String emploi;
    
    @Type(type = "org.hibernate.type.TextType")   
	private String libelle_poste ;
    
    @Type(type = "org.hibernate.type.TextType")  
	private String libelle_grade;

	  @Type(type = "org.hibernate.type.TextType")  
	private String libelle_contrat;
    
    public String getNum_note_svce() {
		return num_note_svce;
	}

	public void setNum_note_svce(String num_note_svce) {
		this.num_note_svce = num_note_svce;
	}

	public String getLibelle_contrat() {
		return libelle_contrat;
	}

	public void setLibelle_contrat(String libelle_contrat) {
		this.libelle_contrat = libelle_contrat;
	}
	private String lieuHbtion;
	
	public String getLieuHbtion() {
		return lieuHbtion;
	}

	public void setLieuHbtion(String lieuHbtion) {
		this.lieuHbtion = lieuHbtion;
	}
	/**
	 * Les clés etrangères
	 */
    
    
    @ManyToOne
	private Poste poste;
    
   
    @ManyToOne
	private Type_agent type_agent;
    
    @ManyToOne
   	private Situation  situation;
   
    
	public Situation getSituation() {
		return situation;
	}

	public Type_agent getType_agent() {
		return type_agent;
	}

	public void setType_agent(Type_agent type_agent) {
		this.type_agent = type_agent;
	}

	public void setSituation(Situation situation) {
		this.situation = situation;
	}
	@ManyToOne
	private Emploi  emplois;
    
   
    @ManyToOne
    private Grade grade;
    
    
    @ManyToOne
    private Mode_recrutement mode_recrutement;
    
   
    @ManyToOne
    private Type_recrutement type_recrutement;
    

    @ManyToOne
    private Echelon echelon;
    
    @ManyToOne
    private Contrat contrat;
    

    public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}
	@ManyToOne
    private Classe classe;
    
    
	 public Echelon getEchelon() {
		return echelon;
	}

	public void setEchelon(Echelon echelon) {
		this.echelon = echelon;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = { "type_recrutement.id", "nom","prenom","date_naissance","lieu_naissance","sexe","contact","adresse","fonctionnaire","libelle_contrat","sit_matr",
 "photo","cvs","moncv","emploi","libelle_poste","libelle_grade","libelle_classe","poste.id","situation.id","libelle_echelon","emplois.id","grade.id","echelon.id","classe.id","contrat.id","type_agent.id","libelle_type_agent" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Agent.sortDirections == null) {
	            Agent.sortDirections = Arrays.asList(Agent.SORT_DIRECTIONS);
	        }
	        return Agent.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Agent.sortFields == null) {
	            Agent.sortFields = Arrays.asList(Agent.SORT_FIELDS);
	        }
	        return Agent.sortFields;
	    }
	    
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	public String getLieu_naissance() {
		return lieu_naissance;
	}
	public void setLieu_naissance(String lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getSit_matr() {
		return sit_matr;
	}
	public void setSit_matr(String sit_matr) {
		this.sit_matr = sit_matr;
	}

	

	public String getFonctionaire() {
		return fonctionaire;
	}

	public void setFonctionaire(String fonctionaire) {
		this.fonctionaire = fonctionaire;
	}

	public Mode_recrutement getMode_recrutement() {
		return mode_recrutement;
	}

	public void setMode_recrutement(Mode_recrutement mode_recrutement) {
		this.mode_recrutement = mode_recrutement;
	}

	public Type_recrutement getType_recrutement() {
		return type_recrutement;
	}

	public void setType_recrutement(Type_recrutement type_recrutement) {
		this.type_recrutement = type_recrutement;
	}

	public Blob getPhoto() {
		return photo;
	}
	public void setPhoto(Blob photo) {
		this.photo = photo;
	}
	public String getMecano() {
		return mecano;
	}
	public void setMecano(String mecano) {
		this.mecano = mecano;
	}
	public String getEmploi() {
		return emploi;
	}
	public void setEmploi(String emploi) {
		this.emploi = emploi;
	}
	
	public String getLibelle_poste() {
		return libelle_poste;
	}
	public void setLibelle_poste(String libelle_poste) {
		this.libelle_poste = libelle_poste;
	}
	public String getLibelle_grade() {
		return libelle_grade;
	}
	public void setLibelle_grade(String libelle_grade) {
		this.libelle_grade = libelle_grade;
	}
	public Poste getPoste() {
		return poste;
	}
	public void setPoste(Poste poste) {
		this.poste = poste;
	}
	
	public Emploi getEmplois() {
		return emplois;
	}
	public void setEmplois(Emploi emplois) {
		this.emplois = emplois;
	}
	public Grade getGrade() {
		return grade;
	}
	public void setGrade(Grade grade) {
		this.grade = grade;
	}  
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "nom";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Agent.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Agent.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Agent> objects = new ArrayList<Agent>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom) like ? or LOWER(prenom) like ? or LOWER(matricule) like ?) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Agent() {
		super();
	}

	public Agent(String nom, String matricule, String libelle_echelon,
			String libelle_classe, String prenom, String email, String nomPere,
			String nomMere, String photFileName, Date delivre_le,
			Date expire_le, Date date_naissance, String numCni,
			String nomJeuneFille, Long nombreEnf, String lieu_naissance,
			String sexe, String contact, String adresse, String sit_matr,
			String fonctionaire, Blob photo, Blob moncv, String num_note_svce,
			String libelle_type_agent, String mecano, String emploi,
			String libelle_poste, String libelle_grade, String libelle_contrat,
			String lieuHbtion, Poste poste, Type_agent type_agent,
			Situation situation, Emploi emplois, Grade grade,
			Mode_recrutement mode_recrutement,
			Type_recrutement type_recrutement, Echelon echelon,
			Contrat contrat, Classe classe) {
		super();
		this.nom = nom;
		this.matricule = matricule;
		this.libelle_echelon = libelle_echelon;
		this.libelle_classe = libelle_classe;
		this.prenom = prenom;
		this.email = email;
		this.nomPere = nomPere;
		this.nomMere = nomMere;
		this.photFileName = photFileName;
		this.delivre_le = delivre_le;
		this.expire_le = expire_le;
		this.date_naissance = date_naissance;
		this.numCni = numCni;
		this.nomJeuneFille = nomJeuneFille;
		this.nombreEnf = nombreEnf;
		this.lieu_naissance = lieu_naissance;
		this.sexe = sexe;
		this.contact = contact;
		this.adresse = adresse;
		this.sit_matr = sit_matr;
		this.fonctionaire = fonctionaire;
		this.photo = photo;
		this.moncv = moncv;
		this.num_note_svce = num_note_svce;
		this.libelle_type_agent = libelle_type_agent;
		this.mecano = mecano;
		this.emploi = emploi;
		this.libelle_poste = libelle_poste;
		this.libelle_grade = libelle_grade;
		this.libelle_contrat = libelle_contrat;
		this.lieuHbtion = lieuHbtion;
		this.poste = poste;
		this.type_agent = type_agent;
		this.situation = situation;
		this.emplois = emplois;
		this.grade = grade;
		this.mode_recrutement = mode_recrutement;
		this.type_recrutement = type_recrutement;
		this.echelon = echelon;
		this.contrat = contrat;
		this.classe = classe;
	}

	
	

}
