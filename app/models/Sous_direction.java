package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Entity
public class Sous_direction extends BaseModel {

	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sous_direction;
	
	//@Type(type = "org.hibernate.type.TextType")
	@ManyToOne
	private Departement departement;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_sous_direction","departement.id" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Sous_direction.sortDirections == null) {
	            Sous_direction.sortDirections = Arrays.asList(Sous_direction.SORT_DIRECTIONS);
	        }
	        return Sous_direction.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Sous_direction.sortFields == null) {
	            Sous_direction.sortFields = Arrays.asList(Sous_direction.SORT_FIELDS);
	        }
	        return Sous_direction.sortFields;
	    }
	    

	public  String getLibelle_sous_direction() {
		return libelle_sous_direction;
	}

	public  void setLibelle_sous_direction(String libelle_sous_direction) {
		this.libelle_sous_direction = libelle_sous_direction;
	}

	public  Departement getDepartement() {
		return departement;
	}

	public  void setDepartement(Departement departement) {
		this.departement = departement;
	}

	
	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_sous_direction";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Sous_direction.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Sous_direction.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Sous_direction> objects = new ArrayList<Sous_direction>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_sous_direction) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	      
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Sous_direction(String libelle_sous_direction, Departement departement) {
		super();
		this.libelle_sous_direction = libelle_sous_direction;
		this.departement = departement;
	}



	
}
