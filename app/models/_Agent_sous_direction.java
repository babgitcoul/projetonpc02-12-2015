package models;

//import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_sous_direction extends BaseModel {
	
	
	private  Date date_affectation;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String affectant;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String poste_agent;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String numero_note_service;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String   note_service;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String poste_anterieur;
	

	private Date date_service ;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String status;
		
	@ManyToOne	
	private Sous_direction sous_direction;

	
	@ManyToOne
	private Agent agent;
	
  final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_affectation","affectant","poste_agent","numero_note_service",
		"poste_anterieur","date_service" ,"status","sous_direction","agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_sous_direction.sortDirections == null) {
            _Agent_sous_direction.sortDirections = Arrays.asList(_Agent_sous_direction.SORT_DIRECTIONS);
        }
        return _Agent_sous_direction.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_sous_direction.sortFields == null) {
            _Agent_sous_direction.sortFields = Arrays.asList(_Agent_sous_direction.SORT_FIELDS);
        }
        return _Agent_sous_direction.sortFields;
    }

	public Date getDate_affectation() {
		return date_affectation;
	}
	public void setDate_affectation(Date date_affectation) {
		this.date_affectation = date_affectation;
	}
	public String getAffectant() {
		return affectant;
	}
	public void setAffectant(String affectant) {
		this.affectant = affectant;
	}
	public String getPoste_agent() {
		return poste_agent;
	}
	public void setPoste_agent(String poste_agent) {
		this.poste_agent = poste_agent;
	}
	public String getNumero_note_service() {
		return numero_note_service;
	}
	public void setNumero_note_service(String numero_note_service) {
		this.numero_note_service = numero_note_service;
	}
	
	public String getPoste_anterieur() {
		return poste_anterieur;
	}
	public void setPoste_anterieur(String poste_anterieur) {
		this.poste_anterieur = poste_anterieur;
	}
	public Date getDate_service() {
		return date_service;
	}
	public void setDate_service(Date date_service) {
		this.date_service = date_service;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote_service() {
		return note_service;
	}

	public void setNote_service(String note_service) {
		this.note_service = note_service;
	}

	public Sous_direction getSous_direction() {
		return sous_direction;
	}

	public void setSous_direction(Sous_direction sous_direction) {
		this.sous_direction = sous_direction;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

public _Agent_sous_direction(Date date_affectation, String affectant,
			String poste_agent, String numero_note_service,
			String note_service, String poste_anterieur, Date date_service,
			String status, Sous_direction sous_direction, Agent agent) {
		super();
		this.date_affectation = date_affectation;
		this.affectant = affectant;
		this.poste_agent = poste_agent;
		this.numero_note_service = numero_note_service;
		this.note_service = note_service;
		this.poste_anterieur = poste_anterieur;
		this.date_service = date_service;
		this.status = status;
		this.sous_direction = sous_direction;
		this.agent = agent;
	}
	
	

}
