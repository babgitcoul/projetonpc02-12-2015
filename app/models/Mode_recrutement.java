package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;
 @Entity
public class Mode_recrutement  extends BaseModel{
	
//	private long id_type_agent;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_mode_recrutement;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_mode_recrutement" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Mode_recrutement.sortDirections == null) {
	            Mode_recrutement.sortDirections = Arrays.asList(Mode_recrutement.SORT_DIRECTIONS);
	        }
	        return Mode_recrutement.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Mode_recrutement.sortFields == null) {
	            Mode_recrutement.sortFields = Arrays.asList(Mode_recrutement.SORT_FIELDS);
	        }
	        return Mode_recrutement.sortFields;
	    }

		public String getLibelle_mode_recrutement() {
			return libelle_mode_recrutement;
		}

		public void setLibelle_mode_recrutement(String libelle_mode_recrutement) {
			this.libelle_mode_recrutement = libelle_mode_recrutement;
		}
	    

		public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_mode_recrutement";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Mode_recrutement.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Mode_recrutement.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Mode_recrutement> objects = new ArrayList<Mode_recrutement>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_mode_recrutement) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	            
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

		public Mode_recrutement(String libelle_mode_recrutement) {
			super();
			this.libelle_mode_recrutement = libelle_mode_recrutement;
		}
}
