package models;

//import java.sql.Date;
import java.sql.Blob;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.*;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Mission  extends BaseModel{
	
	//private long id_mission;
	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_mission;

	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String lieu_mission;

	@JsonProperty
	private Date date_de_depart;

	@JsonProperty
	private Date date_de_retour;
  
	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String objet_mission;
	
	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String frais;
	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String imputation;
	@JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String status;
	
	@Type(type = "blob")
	private Blob   rapport_mission ;
	
	@Type(type = "long")
	private long chef_mission;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = { "status","rapport_mission","imputation","frais","libelle_mission","lieu_mission","date_de_depart","date_de_retour","objet_mission","chef_mission" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Mission.sortDirections == null) {
	            Mission.sortDirections = Arrays.asList(Mission.SORT_DIRECTIONS);
	        }
	        return Mission.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Mission.sortFields == null) {
	            Mission.sortFields = Arrays.asList(Mission.SORT_FIELDS);
	        }
	        return Mission.sortFields;
	    }
	    

	public  String getLibelle_mission() {
		return libelle_mission;
	}

	public  void setLibelle_mission(String libelle_mission) {
		this.libelle_mission = libelle_mission;
	}

	public  String getLieu_mission() {
		return lieu_mission;
	}

	public  void setLieu_mission(String lieu_mission) {
		this.lieu_mission = lieu_mission;
	}


	public Date getDate_de_depart() {
		return date_de_depart;
	}

	public void setDate_de_depart(Date date_de_depart) {
		this.date_de_depart = date_de_depart;
	}

	public Date getDate_de_retour() {
		return date_de_retour;
	}

	public void setDate_de_retour(Date date_de_retour) {
		this.date_de_retour = date_de_retour;
	}

	public  String getObjet_mission() {
		return objet_mission;
	}

	public  void setObjet_mission(String objet_mission) {
		this.objet_mission = objet_mission;
	}

	public  long getChef_mission() {
		return chef_mission;
	}

	public  void setChef_mission(long chef_mission) {
		this.chef_mission = chef_mission;
	}
	
	public String getFrais() {
		return frais;
	}

	public void setFrais(String frais) {
		this.frais = frais;
	}

	

	public String getImputation() {
		return imputation;
	}

	public void setImputation(String imputation) {
		this.imputation = imputation;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Blob getRapport_mission() {
		return rapport_mission;
	}

	public void setRapport_mission(Blob rapport_mission) {
		this.rapport_mission = rapport_mission;
	}


	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_mission";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Mission.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Mission.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Mission> objects = new ArrayList<Mission>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_mission) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Mission(String libelle_mission, String lieu_mission,
			Date date_de_depart, Date date_de_retour, String objet_mission,
			Long chef_mission) {
		super();
		this.libelle_mission = libelle_mission;
		this.lieu_mission = lieu_mission;
		this.date_de_depart = date_de_depart;
		this.date_de_retour = date_de_retour;
		this.objet_mission = objet_mission;
		this.chef_mission = chef_mission;
	}
	
}
