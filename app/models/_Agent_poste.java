package models;

//import java.sql.Date;
import java.util.*;
  
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_poste extends BaseModel {
	
	
	private  Date date_poste;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_poste;
	
	@Type(type = "org.hibernate.type.TextType")
	private String type_affectation;
	

	private Date Date_prise_svce;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@ManyToOne
	private Poste poste;
	
	@ManyToOne
	private Sous_direction sous_dir;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_sous_dir;
	
	@ManyToOne
	private Csu csu;
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_csu;

	@Type(type = "org.hibernate.type.TextType")
	private String note_svce;
	
	@ManyToOne
	private Agent agent;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_poste,libelle_poste,nom_agent,prenom_agent,matricule_agent,poste,agent","libelle_csu"};
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_poste.sortDirections == null) {
            _Agent_poste.sortDirections = Arrays.asList(_Agent_poste.SORT_DIRECTIONS);
        }
        return _Agent_poste.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_poste.sortFields == null) {
            _Agent_poste.sortFields = Arrays.asList(_Agent_poste.SORT_FIELDS);
        }
        return _Agent_poste.sortFields;
    }

	public Date getDate_poste() {
		return date_poste;
	}
	public void setDate_poste(Date date_poste) {
		this.date_poste = date_poste;
	}
	public String getLibelle_poste() {
		return libelle_poste;
	}
	public void setLibelle_poste(String libelle_poste) {
		this.libelle_poste = libelle_poste;
	}
	public String getNom_agent() {
		return nom_agent;
	}
	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}
	public String getPrenom_agent() {
		return prenom_agent;
	}
	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}
	public String getMatricule_agent() {
		return matricule_agent;
	}
	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}

	public Poste getPoste() {
		return poste;
	}

	public void setPoste(Poste poste) {
		this.poste = poste;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_poste(Date date_poste, String libelle_poste,
			String nom_agent, String prenom_agent, String matricule_agent,
			Poste poste, Agent agent) {
		super();
		this.date_poste = date_poste;
		this.libelle_poste = libelle_poste;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.poste = poste;
		this.agent = agent;
	}

	public _Agent_poste(Date date_poste, String libelle_poste,
			String type_affectation, Date date_prise_svce, String nom_agent,
			String prenom_agent, String matricule_agent, Poste poste,
			Sous_direction sous_dir, String libelle_sous_dir, Csu csu,
			String libelle_csu,
			String note_svce, Agent agent) {
		super();
		this.date_poste = date_poste;
		this.libelle_poste = libelle_poste;
		this.type_affectation = type_affectation;
		Date_prise_svce = date_prise_svce;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.poste = poste;
		this.sous_dir = sous_dir;
		this.libelle_sous_dir = libelle_sous_dir;
		this.csu = csu;
		this.libelle_csu = libelle_csu;
		
		this.note_svce = note_svce;
		this.agent = agent;
	}

	public String getType_affectation() {
		return type_affectation;
	}

	public void setType_affectation(String type_affectation) {
		this.type_affectation = type_affectation;
	}

	public Date getDate_prise_svce() {
		return Date_prise_svce;
	}

	public void setDate_prise_svce(Date date_prise_svce) {
		Date_prise_svce = date_prise_svce;
	}

	public Sous_direction getSous_dir() {
		return sous_dir;
	}

	public void setSous_dir(Sous_direction sous_dir) {
		this.sous_dir = sous_dir;
	}

	public String getLibelle_sous_dir() {
		return libelle_sous_dir;
	}

	public void setLibelle_sous_dir(String libelle_sous_dir) {
		this.libelle_sous_dir = libelle_sous_dir;
	}

	public Csu getCsu() {
		return csu;
	}

	public void setCsu(Csu csu) {
		this.csu = csu;
	}

	public String getLibelle_csu() {
		return libelle_csu;
	}

	public void setLibelle_csu(String libelle_csu) {
		this.libelle_csu = libelle_csu;
	}

	public String getNote_svce() {
		return note_svce;
	}

	public void setNote_svce(String note_svce) {
		this.note_svce = note_svce;
	}

	public _Agent_poste() {
		super();
	}
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {
		
        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "nom_agent";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_decoration.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_decoration.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_poste> objects = new ArrayList<_Agent_poste>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(nom_agent) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	
	

}
