package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import play.db.jpa.Blob;
import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class _Agent_piece_conge extends BaseModel {
	
	/*
	 * les information consernant la piece liée a la demande
	 * */

	private Date date_piece;
	private String numero_piece;
	private Date date_delivrance ;
	private String auteur;
	private String lieu_delivrance;
	private String poste_auteur;
	private String libelle_piece;
	private Date date_fin_validite;
	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public String getLibelle_conge() {
		return libelle_conge;
	}

	public void setLibelle_conge(String libelle_conge) {
		this.libelle_conge = libelle_conge;
	}

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	private String filename;
	private Blob piece;
	/*
	 * les informations liées a la demande de congés
	 *  */
	    private Date date_debut;
		private Date date_fin;
		
		@Type(type = "org.hibernate.type.TextType")
		private String libelle_conge;
		@Type(type = "org.hibernate.type.TextType")
		private String motif;
		
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Blob getPiece() {
		return piece;
	}

	public void setPiece(Blob piece) {
		this.piece = piece;
	}

	@ManyToOne
	private _Agent_conge agent_demande;
	@ManyToOne
	private Agent agent;
	
	
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = { "filename","piece", "date_piece","numero_piece","date_delivrance","auteur","lieu_delivrance","poste_auteur","libelle_piece","date_fin_valide","conge,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_piece_conge.sortDirections == null) {
            _Agent_piece_conge.sortDirections = Arrays.asList(_Agent_piece_conge.SORT_DIRECTIONS);
        }
        return _Agent_piece_conge.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_piece_conge.sortFields == null) {
            _Agent_piece_conge.sortFields = Arrays.asList(_Agent_piece_conge.SORT_FIELDS);
        }
        return _Agent_piece_conge.sortFields;
    }

	public Date getDate_piece() {
		return date_piece;
	}
	public void setDate_piece(Date date_piece) {
		this.date_piece = date_piece;
	}
	public String getNumero_piece() {
		return numero_piece;
	}
	public void setNumero_piece(String numero_piece) {
		this.numero_piece = numero_piece;
	}
	public Date getDate_delivrance() {
		return date_delivrance;
	}
	public void setDate_delivrance(Date date_delivrance) {
		this.date_delivrance = date_delivrance;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getLieu_delivrance() {
		return lieu_delivrance;
	}
	public void setLieu_delivrance(String lieu_delivrance) {
		this.lieu_delivrance = lieu_delivrance;
	}
	public String getPoste_auteur() {
		return poste_auteur;
	}
	public void setPoste_auteur(String poste_auteur) {
		this.poste_auteur = poste_auteur;
	}
	public String getLibelle_piece() {
		return libelle_piece;
	}
	public void setLibelle_piece(String libelle_piece) {
		this.libelle_piece = libelle_piece;
	}
	public Date getDate_fin_validite() {
		return date_fin_validite;
	}
	public void setDate_fin_validite(Date date_fin_validite) {
		this.date_fin_validite = date_fin_validite;
	}
	

	

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_piece_conge(Date date_piece, String numero_piece,
			Date date_delivrance, String auteur, String lieu_delivrance,
			String poste_auteur, String libelle_piece, Date date_fin_validite,
			String filename, Blob piece, _Agent_conge agent_demande, Agent agent) {
		 super();
		 this.date_piece = date_piece;
		 this.numero_piece = numero_piece;
		 this.date_delivrance = date_delivrance;
		 this.auteur = auteur;
		 this.lieu_delivrance = lieu_delivrance;
		 this.poste_auteur = poste_auteur;
		 this.libelle_piece = libelle_piece;
		 this.date_fin_validite = date_fin_validite;
		 this.filename = filename;
		 this.piece = piece;
		 this.agent_demande = agent_demande;
		 this.agent = agent;
	}

	public _Agent_conge getAgent_demande() {
		return agent_demande;
	}

	public void setAgent_demande(_Agent_conge agent_demande) {
		this.agent_demande = agent_demande;
	}

	public _Agent_piece_conge() {
		super();
	}

	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_piece";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_piece_conge.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_piece_conge.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_piece_conge> objects = new ArrayList<_Agent_piece_conge>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_piece) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
         
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public _Agent_piece_conge(String libelle_piece) {
		super();
		this.libelle_piece = libelle_piece;
	}

	
	

}
