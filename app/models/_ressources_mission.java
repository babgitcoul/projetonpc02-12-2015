package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Type;

@Entity
public class _ressources_mission extends BaseModel {

	@ManyToOne
	private Ressource ressource;
	
	@ManyToOne
	private Mission mission;
	
	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "mission","ressource" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	
	public Ressource getRessource() {
		return ressource;
	}

	public void setRessource(Ressource ressource) {
		this.ressource = ressource;
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	public static List<String> getSortDirections() {
        if (_ressources_mission.sortDirections == null) {
            _ressources_mission.sortDirections = Arrays.asList(_ressources_mission.SORT_DIRECTIONS);
        }
        return _ressources_mission.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_ressources_mission.sortFields == null) {
            _ressources_mission.sortFields = Arrays.asList(_ressources_mission.SORT_FIELDS);
        }
        return _ressources_mission.sortFields;
    }



	public _ressources_mission( Ressource ressource,Mission mission) {
		super();
		this.mission = mission;
		this.ressource = ressource;
		
	}
	
	
	

}
