package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Formation extends BaseModel {
	
//	private long id_formation;
	
	@Type(type = "org.hibernate.type.TextType")	
	private String diplome;
	
	@Type(type = "org.hibernate.type.TextType")
	private String carte;	
	
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_formation;

	@Type(type = "org.hibernate.type.TextType")
	private String description;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_formation","diplome","carte","description" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Formation.sortDirections == null) {
	            Formation.sortDirections = Arrays.asList(Formation.SORT_DIRECTIONS);
	        }
	        return Formation.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Formation.sortFields == null) {
	            Formation.sortFields = Arrays.asList(Formation.SORT_FIELDS);
	        }
	        return Formation.sortFields;
	    }
	    

	public String getDiplome() {
		return diplome;
	}


	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCarte() {
		return carte;
	}


	public void setCarte(String carte) {
		this.carte = carte;
	}


	public String getLibelle_formation() {
		return libelle_formation;
	}


	public void setLibelle_formation(String libelle_formation) {
		this.libelle_formation = libelle_formation;
	}
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_formation";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Formation.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Formation.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Formation> objects = new ArrayList<Formation>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_formation) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Formation(String diplome, String carte, String libelle_formation) {
		super();
		this.diplome = diplome;
		this.carte = carte;
		this.libelle_formation = libelle_formation;
	}

}
