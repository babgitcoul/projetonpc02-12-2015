package models;

import java.sql.Blob;
//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;
@Entity
public class _Agent_mission  extends BaseModel{
	
    @JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private  String libelle_mission;
	
    @JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String poste_agent;
	
    @JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String lieu;
	
    @JsonProperty
	@Type(type = "org.hibernate.type.TextType")
	private String objet;	
	
    @JsonProperty
	@Type(type = "date")
	private Date   date_depart;
    @JsonProperty
	@Type(type = "date")
	private Date   date_reour;
	
    @JsonProperty
	@ManyToOne
	private Mission mission;
    @JsonProperty
	@ManyToOne
	private Agent agent;
	
	
	public _Agent_mission() {
		super();
	}

	final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "libelle_mission","poste_agent","lieu,objet","date_depart","date_retour",
													"mission,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_mission.sortDirections == null) {
            _Agent_mission.sortDirections = Arrays.asList(_Agent_mission.SORT_DIRECTIONS);
        }
        return _Agent_mission.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_mission.sortFields == null) {
            _Agent_mission.sortFields = Arrays.asList(_Agent_mission.SORT_FIELDS);
        }
        return _Agent_mission.sortFields;
    }


	public String getLibelle_mission() {
		return libelle_mission;
	}
	public void setLibelle_mission(String libelle_mission) {
		this.libelle_mission = libelle_mission;
	}
	public String getPoste_agent() {
		return poste_agent;
	}
	public void setPoste_agent(String poste_agent) {
		this.poste_agent = poste_agent;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLeiu(String lieu) {
		this.lieu = lieu;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public Date getDate_depart() {
		return date_depart;
	}
	public void setDate_depart(Date date_depart) {
		this.date_depart = date_depart;
	}
	public Date getDate_reour() {
		return date_reour;
	}
	public void setDate_reour(Date date_reour) {
		this.date_reour = date_reour;
	}

	public Mission getMission() {
		return mission;
	}
	public void setMission(Mission mission) {
		this.mission = mission;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Agent_mission(String libelle_mission, String poste_agent,
			String lieu, String objet, Date date_depart, Date date_reour,
			
			 Mission mission, Agent agent) {
		super();
		this.libelle_mission = libelle_mission;
		this.poste_agent = poste_agent;
		this.lieu = lieu;
		this.objet = objet;
		this.date_depart = date_depart;
		this.date_reour = date_reour;
		
		this.mission = mission;
		this.agent = agent;
	}
	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_mission";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!_Agent_mission.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!_Agent_mission.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<_Agent_mission> objects = new ArrayList<_Agent_mission>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_mission) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
           
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

}
