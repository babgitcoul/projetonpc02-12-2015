package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.Type;

@Entity
public class Traitement extends BaseModel{
	
	//private long id_traitement;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_traitement;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_traitement" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		
		public static List<String> getSortDirections() {
	        if (Traitement.sortDirections == null) {
	            Traitement.sortDirections = Arrays.asList(Traitement.SORT_DIRECTIONS);
	        }
	        return Traitement.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Traitement.sortFields == null) {
	            Traitement.sortFields = Arrays.asList(Traitement.SORT_FIELDS);
	        }
	        return Traitement.sortFields;
	    }
	    

	public  String getLibelle_traitement() {
		return libelle_traitement;
	}

	public  void setLibelle_traitement(String libelle_traitement) {
		this.libelle_traitement = libelle_traitement;
	}
	
	 public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

	        if ((sortField == null) || (sortField.length() < 1)) {
	            sortField = "libelle_traitement";
	        }

	        if ((sortDirection == null) || (sortDirection.length() < 1)) {
	            sortDirection = "desc";
	        }

	        if (!Traitement.getSortFields().contains(sortField)) {
	            throw new IllegalArgumentException("sortField is not valid.");
	        }

	        if (!Traitement.getSortDirections().contains(sortDirection)) {
	            throw new IllegalArgumentException("sortDirection is not valid");
	        }

	        List<Traitement> objects = new ArrayList<Traitement>();
	        List<Object> paramList = new ArrayList<Object>();
	        StringBuilder sb = new StringBuilder();
	        HashMap<String, Object> result = new HashMap();
	        Long count;

	        sb.append("id > 0");

	        if ((keyword != null) && (keyword.trim().length() > 0)) {
	            sb.append(" and (LOWER(libelle_traitement) like ? ) ");
	            paramList.add("%" + keyword.toLowerCase() + "%");
	     
	        }

	        Object[] params = paramList.toArray(new Object[paramList.size()]);
	        count = count(sb.toString(), params);

	        sb.append(" order by " + sortField + " " + sortDirection);
	        JPAQuery query = find(sb.toString(), params);
	        if (page < 1) {
	            page = 1;
	        }
	        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

	        result.put("objects", objects);
	        result.put("count", count);
	        return result;
	    }

	public Traitement(String libelle_traitement) {
		super();
		this.libelle_traitement = libelle_traitement;
	}
	

}
