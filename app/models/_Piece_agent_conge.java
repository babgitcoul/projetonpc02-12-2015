package models;

//import java.sql.Date;
import java.util.*;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
@Entity
public class _Piece_agent_conge extends BaseModel {
	
	

	private Date date_piece;
	private String numero_piece;
	private Date date_delivrance ;
	private String auteur;
	private String lieu_delivrance;
	private String poste_auteur;
	private String piece;
	private Date date_fin_validite;
	
	@ManyToOne
	private Conge conge;
	@ManyToOne
	private Agent agent;
	
	
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "date_piece","numero_piece","date_delivrance","auteur","lieu_delivrance","poste_auteur","peice","date_fin_valide","conge,agent" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Piece_agent_conge.sortDirections == null) {
            _Piece_agent_conge.sortDirections = Arrays.asList(_Piece_agent_conge.SORT_DIRECTIONS);
        }
        return _Piece_agent_conge.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Piece_agent_conge.sortFields == null) {
            _Piece_agent_conge.sortFields = Arrays.asList(_Piece_agent_conge.SORT_FIELDS);
        }
        return _Piece_agent_conge.sortFields;
    }

	public Date getDate_piece() {
		return date_piece;
	}
	public void setDate_piece(Date date_piece) {
		this.date_piece = date_piece;
	}
	public String getNumero_piece() {
		return numero_piece;
	}
	public void setNumero_piece(String numero_piece) {
		this.numero_piece = numero_piece;
	}
	public Date getDate_delivrance() {
		return date_delivrance;
	}
	public void setDate_delivrance(Date date_delivrance) {
		this.date_delivrance = date_delivrance;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getLieu_delivrance() {
		return lieu_delivrance;
	}
	public void setLieu_delivrance(String lieu_delivrance) {
		this.lieu_delivrance = lieu_delivrance;
	}
	public String getPoste_auteur() {
		return poste_auteur;
	}
	public void setPoste_auteur(String poste_auteur) {
		this.poste_auteur = poste_auteur;
	}
	public String getPiece() {
		return piece;
	}
	public void setPiece(String piece) {
		this.piece = piece;
	}
	public Date getDate_fin_validite() {
		return date_fin_validite;
	}
	public void setDate_fin_validite(Date date_fin_validite) {
		this.date_fin_validite = date_fin_validite;
	}
	public Conge getConge() {
		return conge;
	}

	public void setConge(Conge conge) {
		this.conge = conge;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public _Piece_agent_conge(Date date_piece, String numero_piece,
			Date date_delivrance, String auteur, String lieu_delivrance,
			String poste_auteur, String piece, Date date_fin_validite,
			Conge conge, Agent agent) {
		super();
		this.date_piece = date_piece;
		this.numero_piece = numero_piece;
		this.date_delivrance = date_delivrance;
		this.auteur = auteur;
		this.lieu_delivrance = lieu_delivrance;
		this.poste_auteur = poste_auteur;
		this.piece = piece;
		this.date_fin_validite = date_fin_validite;
		this.conge = conge;
		this.agent = agent;
	}
	
	

}
