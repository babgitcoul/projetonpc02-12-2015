package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Csu  extends BaseModel{
	
//	private long id_csu;
	@Type(type = "org.hibernate.type.TextType")
	private String libelle_csu;
	
	@Type(type = "org.hibernate.type.TextType")
	@ManyToOne
	private Ville ville;
	
	@Type(type = "org.hibernate.type.TextType")
	@ManyToOne
	private Arpc arpc;
	
	 final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
		
		final private static String[] SORT_FIELDS = {  "libelle_csu","ville.id,Arpc.id" };
		
		private static List<String> sortDirections;

		private static List<String> sortFields;
		    
	    public static List<String> getSortDirections() {
	        if (Csu.sortDirections == null) {
	            Csu.sortDirections = Arrays.asList(Csu.SORT_DIRECTIONS);
	        }
	        return Csu.sortDirections;
	    }

	    public static List<String> getSortFields() {
	        if (Csu.sortFields == null) {
	            Csu.sortFields = Arrays.asList(Csu.SORT_FIELDS);
	        }
	        return Csu.sortFields;
	    }

	public String getLibelle_csu() {
		return libelle_csu;
	}

	public void setLibelle_csu(String libelle_csu) {
		this.libelle_csu = libelle_csu;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public Arpc getArpc() {
		return arpc;
	}

	public void setArpc(Arpc arpc) {
		this.arpc = arpc;
	}

	
	public static HashMap<String, Object> search(String keyword, String sortField, String sortDirection, int page) {

        if ((sortField == null) || (sortField.length() < 1)) {
            sortField = "libelle_csu";
        }

        if ((sortDirection == null) || (sortDirection.length() < 1)) {
            sortDirection = "desc";
        }

        if (!Csu.getSortFields().contains(sortField)) {
            throw new IllegalArgumentException("sortField is not valid.");
        }

        if (!Csu.getSortDirections().contains(sortDirection)) {
            throw new IllegalArgumentException("sortDirection is not valid");
        }

        List<Csu> objects = new ArrayList<Csu>();
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        HashMap<String, Object> result = new HashMap();
        Long count;

        sb.append("id > 0");

        if ((keyword != null) && (keyword.trim().length() > 0)) {
            sb.append(" and (LOWER(libelle_csu) like ? ) ");
            paramList.add("%" + keyword.toLowerCase() + "%");
          
        }

        Object[] params = paramList.toArray(new Object[paramList.size()]);
        count = count(sb.toString(), params);

        sb.append(" order by " + sortField + " " + sortDirection);
        JPAQuery query = find(sb.toString(), params);
        if (page < 1) {
            page = 1;
        }
        objects = query.fetch(page, BaseModel.DEFAULT_PAGINATE_COUNT);

        result.put("objects", objects);
        result.put("count", count);
        return result;
    }

	public Csu(String libelle_csu, Ville ville, Arpc arpc) {
		super();
		this.libelle_csu = libelle_csu;
		this.ville = ville;
		this.arpc = arpc;
	}
}
