package models;

//import java.sql.Date;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class _Agent_responsable extends BaseModel {
	

	private Date date_enregistrement;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent;
	
	@Type(type = "org.hibernate.type.TextType")
	private String nom_agent1;
	
	@Type(type = "org.hibernate.type.TextType")
	private String prenom_agent1;
	
	@Type(type = "org.hibernate.type.TextType")
	private String matricule_agent1;
	
	
	private Agent agent;
	
	@ManyToOne
	private Agent agent1;
	
		
	
	
final private static String[] SORT_DIRECTIONS = { "asc", "desc" };
	
	final private static String[] SORT_FIELDS = {  "agent","agent1","nom_agent1","prenom_agent1","matricule_agent1",
		"nom_agent1","prenom_agent1","matricule_agent1","date_enregistrement" };
	
	private static List<String> sortDirections;

	private static List<String> sortFields;
	
	public static List<String> getSortDirections() {
        if (_Agent_responsable.sortDirections == null) {
            _Agent_responsable.sortDirections = Arrays.asList(_Agent_responsable.SORT_DIRECTIONS);
        }
        return _Agent_responsable.sortDirections;
    }

    public static List<String> getSortFields() {
        if (_Agent_responsable.sortFields == null) {
            _Agent_responsable.sortFields = Arrays.asList(_Agent_responsable.SORT_FIELDS);
        }
        return _Agent_responsable.sortFields;
    }

	public Date getDate_enregistrement() {
		return date_enregistrement;
	}

	public void setDate_enregistrement(Date date_enregistrement) {
		this.date_enregistrement = date_enregistrement;
	}

	public String getNom_agent() {
		return nom_agent;
	}

	public void setNom_agent(String nom_agent) {
		this.nom_agent = nom_agent;
	}

	public String getPrenom_agent() {
		return prenom_agent;
	}

	public void setPrenom_agent(String prenom_agent) {
		this.prenom_agent = prenom_agent;
	}

	public String getMatricule_agent() {
		return matricule_agent;
	}

	public void setMatricule_agent(String matricule_agent) {
		this.matricule_agent = matricule_agent;
	}

	public String getNom_agent1() {
		return nom_agent1;
	}

	public void setNom_agent1(String nom_agent1) {
		this.nom_agent1 = nom_agent1;
	}

	public String getPrenom_agent1() {
		return prenom_agent1;
	}

	public void setPrenom_agent1(String prenom_agent1) {
		this.prenom_agent1 = prenom_agent1;
	}

	public String getMatricule_agent1() {
		return matricule_agent1;
	}

	public void setMatricule_agent1(String matricule_agent1) {
		this.matricule_agent1 = matricule_agent1;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Agent getAgent1() {
		return agent1;
	}

	public void setAgent1(Agent agent1) {
		this.agent1 = agent1;
	}

	public _Agent_responsable(Date date_enregistrement, String nom_agent,
			String prenom_agent, String matricule_agent, String nom_agent1,
			String prenom_agent1, String matricule_agent1, Agent agent,
			Agent agent1) {
		super();
		this.date_enregistrement = date_enregistrement;
		this.nom_agent = nom_agent;
		this.prenom_agent = prenom_agent;
		this.matricule_agent = matricule_agent;
		this.nom_agent1 = nom_agent1;
		this.prenom_agent1 = prenom_agent1;
		this.matricule_agent1 = matricule_agent1;
		this.agent = agent;
		this.agent1 = agent1;
	}


	
	

}
