package notifiers;
 
import models.*;

import org.apache.commons.mail.*; 

import play.*;
import play.mvc.*;

import java.util.*;
 
public class Mails extends Mailer {
 
   public static void welcome(Agent agent) {
      setSubject("Félicitations %s", agent.getNom());
      addRecipient(agent.getEmail());
      setFrom("ONPC");
      send(agent);
   }
 
//   public static void lostPassword(Participant parti) {
//      String newpassword = user.password;
//      setFrom("Robot <robot@thecompany.com>");
//      setSubject("Your password has been reset");
//      addRecipient(user.email);
//      send(user, newpassword);
//   }
 
}
