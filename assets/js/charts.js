var Charts = function() {
	"use strict";
	var runChart4 = function() {
		nv.addGraph(function() {
			var chart = nv.models.discreteBarChart().x(function(d) {
				return d.label;
			})//Specify the data accessors.
			.y(function(d) {
				return d.value;
			}).staggerLabels(true)//Too many bars and not enough room? Try staggering labels.
			.tooltips(false)//Don't show tooltips
			.showValues(true)//...instead, show the bar value right on top of each bar.
			.transitionDuration(350);

			d3.select('#demo-chart-4 svg').datum(exampleData()).call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});

		//Each bar represents a single discrete quantity.
		function exampleData() {
			return [{
				key : "Cumulative Return",
				values : [{
					"label" : "DALOA",
					"value" : -29.765957771107
				}, {
					"label" : "BOUAKE",
					"value" : 0
				}, {
					"label" : "SAN PEDRO",
					"value" : 32.807804682612
				}, {
					"label" : "Abidjan",
					"value" : 196.45946739256
				}, {
					"label" : "MAN",
					"value" : 0.19434030906893
				}, {
					"label" : "GAGNOA",
					"value" : -98.079782601442
				}, {
					"label" : "YAKRO",
					"value" : -13.925743130903
				}, {
					"label" : "AUTRE",
					"value" : -5.1387322875705
				}]
			}];

		}

	};
	var runChart5 = function() {
		d3.json('assets/plugins/nvd3/json/multiBarHorizontalData.txt', function(data) {
			nv.addGraph(function() {
				var chart = nv.models.multiBarHorizontalChart().x(function(d) {
					return d.label;
				}).y(function(d) {
					return d.value;
				}).margin({
					top : 30,
					right : 20,
					bottom : 50,
					left : 175
				}).showValues(true)//Show bar value next to each bar.
				.tooltips(true)//Show tooltips on hover.
				.transitionDuration(350).showControls(true);
				//Allow user to switch between "Grouped" and "Stacked" mode.

				chart.yAxis.tickFormat(d3.format(',.2f'));

				d3.select('#demo-chart-5 svg').datum(data).call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});
		});

	};
	var runChart9_10 = function() {
		//Regular pie chart example
		nv.addGraph(function() {
			var chart = nv.models.pieChart().x(function(d) {
				return d.label;
			}).y(function(d) {
				return d.value;
			}).showLabels(true);

			d3.select("#demo-chart-9 svg").datum(exampleData()).transition().duration(350).call(chart);

			return chart;
		});

		//Donut chart example
		nv.addGraph(function() {
			var chart = nv.models.pieChart().x(function(d) {
				return d.label;
			}).y(function(d) {
				return d.value;
			}).showLabels(true)//Display pie labels
			.labelThreshold(.05)//Configure the minimum slice size for labels to show up
			.labelType("percent")//Configure what type of data to show in the label. Can be "key", "value" or "percent"
			.donut(true)//Turn on Donut mode. Makes pie chart look tasty!
			.donutRatio(0.35)//Configure how big you want the donut hole size to be.
			;

			d3.select("#demo-chart-10 svg").datum(exampleData()).transition().duration(350).call(chart);

			return chart;
		});

		//Pie chart example data. Note how there is only a single array of key-value pairs.
		function exampleData() {
			return [{
				"label" : "DALOA",
				"value" : 29.765957771107
			}, {
				"label" : "FERKE",
				"value" : 0
			}, {
				"label" : "Man",
				"value" : 32.807804682612
			}, {
				"label" : "Bouake",
				"value" : 196.45946739256
			}, {
				"label" : "Vavoua",
				"value" : 0.19434030906893
			}, {
				"label" : "Korhogo",
				"value" : 98.079782601442
			}, {
				"label" : "Abidjan",
				"value" : 13.925743130903
			}, {
				"label" : "Yakro",
				"value" : 5.1387322875705
			}];
		}

	};
	function stream_layers(n, m, o) {
		if (arguments.length < 3)
			o = 0;
		function bump(a) {
			var x = 1 / (.1 + Math.random()), y = 2 * Math.random() - .5, z = 10 / (.1 + Math.random());
			for (var i = 0; i < m; i++) {
				var w = (i / m - y) * z;
				a[i] += x * Math.exp(-w * w);
			}
		}

		return d3.range(n).map(function() {
			var a = [], i;
			for ( i = 0; i < m; i++)
				a[i] = o + o * Math.random();
			for ( i = 0; i < 5; i++)
				bump(a);
			return a.map(stream_index);
		});
	}

	function stream_index(d, i) {
		return {
			x : i,
			y : Math.max(0, d)
		};
	}

	return {
		//main function to initiate template pages
		init : function() {
			runChart4();
			runChart5();
			runChart6();
			runChart7();
			runChart9_10();
		}
	};
}();
